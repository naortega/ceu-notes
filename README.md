# CEU Notes (Recursos CEU)

Este documento contiene todos los recursos con los que yo he trabajado durante mi tiempo estudiando la titulación profesional de ASIR en CEU San Pablo.

Todos los recursos están disponible bajo la licencia Creative Commons Attribution No-Derivatives 4.0 International, salvo en casos donde se diga explícitamente lo contrario.

La mayoría de los documentos están escritos en texto normal (lo cual se pueden abrir en NotePad), terminando en la extensión `.txt`. Los documentos que terminan en `.tex` son aquellos usados para la creación de documentos PDF, y están escritos en el lenguaje de programación [LaTeX](https://www.latex-project.org/). Generalmente todos han de tener su PDF correspondiente (ya compilado). Yo personalmente he usado la herramienta XeLaTeX para la compilación, pero lo mismo seguramente funciona con `pdflatex` normal y corriente.

Espero que este repositorio sirva para ayudar a mis _hermanos en armas_ de los cursos siguientes con algunas de las cosas más raras que nos piden a veces los profesores.

**AVISO:** Los profesores saben que existe este repositorio, y más de uno comparará tu trabajo con el mío, así que cuidado con copiar.
