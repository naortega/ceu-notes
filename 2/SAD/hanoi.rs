use std::env;

fn print_hanoi(n:i32)
{
    if n == 2
    {
        println!("1");
        println!("2");
        println!("1");
    }
    else
    {
        print_hanoi(n-1);
        println!("{}", n);
        print_hanoi(n-1);
    }
}

fn main()
{
    let args:Vec<String> = env::args().collect();
    if args.len() != 2
    {
        println!("Need a number!");
        std::process::exit(1);
    }
    let n:i32 = args[1].parse().unwrap();
    if n < 2
    {
        println!("Need a number greater than 1!");
        std::process::exit(1);
    }

    print_hanoi(n);
}
