\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Introducción}{2}{section.1}%
\contentsline {section}{\numberline {2}Posibles Riesgos}{2}{section.2}%
\contentsline {section}{\numberline {3}Almacenamiento Ordinario de Datos}{2}{section.3}%
\contentsline {section}{\numberline {4}Las Copias de Seguridad}{2}{section.4}%
\contentsline {section}{\numberline {5}Recuperación}{3}{section.5}%
\contentsline {section}{\numberline {6}Vulnerabilidades}{3}{section.6}%
\contentsline {section}{\numberline {7}Derechos de Autor y Licencia}{4}{section.7}%
