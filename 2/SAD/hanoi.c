#include <stdlib.h>
#include <stdio.h>

void print_hanoi(int n)
{
	if(n == 2)
	{
		printf("1\n2\n1\n");
	}
	else
	{
		print_hanoi(n-1);
		printf("%d\n", n);
		print_hanoi(n-1);
	}
}

int main(int argc, char *argv[])
{
	if (argc != 2)
	{
		fprintf(stderr, "Give me a number!!!\n");
		return EXIT_FAILURE;
	}
	int n = atoi(argv[1]);
	if (n < 2)
	{
		fprintf(stderr, "Number must be greater than 1!\n");
		return EXIT_FAILURE;
	}
	print_hanoi(n);
	return EXIT_SUCCESS;
}
