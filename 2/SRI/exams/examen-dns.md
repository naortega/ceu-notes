# Instalación DNS

Nicolás A. Ortega Froysa

Lo primero sería configurar el archivo `/etc/network/interfaces`
apropiadamente para usar una dirección IP estática.

Luego instalar bind:

```
apt update
apt install bind9
```

Asegurarnos de que el servicio está corriendo

```
systemctl status bind9
```

Para configurar reenviador DNS, editamos el archivo
/etc/bind/named.conf.options, descomentando y modificando el bloque de
forwarders para que se parezca a lo siguiente:

```
forwarders {
	4.4.4.4;
};
```

Para verificar que nuestro archivo no tiene errores podemos correr lo
siguiente:

```
named-checkconf named.conf.options
```

Para configurar nuestro servidor DNS para examenasir.com debemos de editar el
archivo named.conf para que sea de la forma siguiente:

```
include "/etc/bind/named.conf.options";
include "/etc/bind/named.conf.local";
//include "/etc/bind/named.conf.default-zones";
```

Ahora, para crear los archivos que necesitamos, vamos a hacer copias de el
archivo `db.local` a ser `examenasir.local` respectivamente, y
`named.conf.local` lo modificamos para que contenga lo siguiente:

```
zone "examenasir.com" {
        type master;
        file "/etc/bind/examenasir.local";
};
```

Luego, el archivo `examenasir.local` lo modificamos para que sea de la manera
siguiente:

```
$TTL    604800
@       IN      SOA     examenasir.com. root.examenasir.com. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@       IN      NS      examenasir.com.
@       IN      A       10.0.2.15
mail    IN      MX      10      examenasir.com.
webmail IN      A       10.0.2.15
```

Para verificar nuestra configuración:

```
named-checkzone examenasir.com examenasir.local
```

Finalmente, editamos el archiov `/etc/resolv.conf` para resolver este nombre
de dominio con nuestro servidor de DNS:

```
domain examenasir.com
search examenasir.com
nameserver 10.0.2.15
```

Al hacer esto, ya si hacemos un `nslookup` encontraremos la salida
siguiente:

```
Server:         10.0.2.15
Address:        10.0.2.15#53

Name:   examenasir.com
Address: 10.0.2.15
```
