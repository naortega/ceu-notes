apt update

# Install MariaDB
apt install mariadb-server
mysql_secure_installation
# root passwd: toor
mysql -u root -p
> create database openfiredb;
> create user 'openfireus'@'localhost' IDENTIFIED BY 'openfirepass';
> grant all on openfiredb.* to 'openfireus'@'localhost';
> flush privileges;
> quit;

# Openfire
wget https://github.com/igniterealtime/Openfire/releases/download/v4.7.0/openfire_4.7.0_all.deb
apt install ./openfire_4.7.0_all.deb

# Admin console:
English
--
XMPP Domain Name: my-openfire
XMPP Domain Name: my-openfire
--
Standard Database Connection
--
MySQL
Database URL:
# Cambiar HOSTNAME por localhost
# Cambiar DATABASE por openfiredb
Username: openfireus
Password: openfirepass
--
Current Password: admin
New Password: admin
Re-Enter Password: admin


Se añaden los usuarios y esas cosas, que no puedo porque OpenFire no quiere
funcionar. Lo demás lo explico por este problema técnico.

# Spark
wget https://www.igniterealtime.org/downloadServlet?filename=spark/spark_3_0_1.deb
apt install ./spark_3_0_1.deb

Como es obvio, no puede continuar desde aquí por culpa de que OpenFire no
quiere funcionar. Pero básicamente, desde el panel de administrador de OpenFire crearía los usuarios para luego conectarme con Spark al servidor, conectándome a my-openfire, que lo había configurado en mi /etc/hosts.
