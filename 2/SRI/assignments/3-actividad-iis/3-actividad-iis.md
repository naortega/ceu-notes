# 3. Actividad IIS

_Nicolás A. Ortega Froysa_

## Configuración Hostname

Para configurar un nuevo dominio lo primero que hemos de hacer es abrir el IIS
de Microsoft.

![IIS](imgs/00-iis.png)

Luego tenemos que crear un sitio nuevo, y darle el nombre de dominio
`prueba.es`.

![IIS](imgs/01-crear-sitio.png)
![IIS](imgs/02-formulario-sitio-nuevo.png)

Luego tenemos que cambiar el archivo de `hosts` que se encuentra en
`C:\Windows\System32\drivers\etc\`. Ahí editamos el documento para redireccionar
las peticiones para `prueba.es` a nuestro `localhost` (`127.0.0.1`).

![IIS](imgs/03-hosts-path.png)
![IIS](imgs/04-entrada-hosts.png)

Después cambiamos el archivo predeterminado de directorio añadiendo una entrada
nueva a la lista. Este archivo se debe de denominarse `inicio.html`.

![IIS](imgs/05-menu-cambiar-inicio.png)
![IIS](imgs/06-agregar-entrada.png)
![IIS](imgs/07-entrada.png)

Finalmente, reiniciamos el servidor y creamos el archivo de `inicio.html`. Al
navegar a `http://prueba.es` nos debe de salir el contenido de este archivo.

![IIS](imgs/08-reiniciar.png)
![IIS](imgs/09-archivo-inicio.png)
![IIS](imgs/10-resultado.png)

## Conclusión

IIS es muy complicado para lo que debería de ser un solo archivo de
configuración.

## Derechos de Autor

Copyright © 2022 Nicolás A. Ortega <nicolas@ortegas.org>

Este documento está licenciado bajo las condiciones del CC-BY-ND 4.0
Internacional.
