# Configuración Apache

Por Nicolás A. Ortega Froysa

## Actividad I

**Configura el dominio markdown.es en tu servidor de DNS local (máquina
virtual).**

Se añade en el archivo `/etc/hosts` el siguiente código:

```
127.0.0.1     markdown.es
```

Esto se puede hacer por medio del comando siguiente como root:

```bash
echo "127.0.0.1  markdown.es" >> /etc/hosts
```

## Actividad II

**Configura el dominio markdown.es en tu equipo local para que apunte a la
dirección IP de tu máquina virtual**

_[Omitido por falta de datos de administrador]_

## Actividad III

**Conéctate a tu máquina virtual a través de un cliente SSH.**

Usando Putty me conecto a `localhost:8022` (el puerto que he asignado para
acceder al puerto 22 de la máquina virtual) utilizando mi usuario (`nicolas`) y
mi contraseña (`nicolas`).

## Actividad IV

**Crea un fichero de configuración en Apache (vurtalhost) para markdown.es.**

Como ya tenemos archivos de configuración disponibles, en vez de crear un
archivo de cero, vamos a copiar un archivo existente (de prácticas anteriores) y
modificarlo. Lo hacemos de la manera siguiente:

```bash
cd /etc/apache2/sites-available/
cp semanaverde.com.conf markdown.es.conf
sed -i 's/semanaverde\.com/markdown\.es/g' markdown.es.conf
```

## Actividad V

**Activar el sitio markdown.es.**

Para actualizar tan sólo es necesario hacer un enlace simbólico:

```bash
cd /etc/apache2/sites-enabled/
ln -s ../sites-available/markdown.es.conf ./markdown.es.conf
```

## Actividad VI

**Modificaciones**

En primer lugar, modificamos cual es el directorio raíz de nuestro sitio web
para que sea `/home/markdown.es`. Para hacer esto hemos de cambiar la directiva
`DocumentRoot` a tener `/home/markdown.es`. Además de esto, hemos de añadir las
líneas siguientes al final de nuestro archivo de configuración fuera del bloque
de `VirtualHost`:

```
<Directory /home/markdown.es/>
    #Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>
```

Comentamos la segunda línea ya que no queremos que se haga índice de los
archivos existentes.

También queremos cambiar el fichero por defecto, que se hace con la directiva
`DirectoryIndex inicio.html`. Con esto podemos crear los directorios y ficheros
que nos hacen falta:

```bash
mkdir /home/markdown.es/
echo "Hola, Markdown!" > /home/markdown.es/inicio.html
```

Finalmente, añadimos un archivo para informar sobre un error 404:

```bash
echo "Recurso no encontrado" >> /home/markdown.es/error404.html
```

Y editando el archivo de configuración de nuestro servidor añadimos la directiva
siguiente:

```
ErrorDocument 404 "/error404.html"
```
