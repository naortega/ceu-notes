# Esquema Mail

## Origen con Servidor Local

```
MUA origen --*SMTP*--> MTA local --> MDA local --> Buzón de enviados
                                 \--> MTA receptor --> MDA receptor --> Buzón recibidos

MUA receptor --*IMAP/POP*--> MTA receptor --> MDA receptor --> Buzón recibidos --> MDA receptor --> MTA receptor --*IMAP/POP*--> MUA receptor
```

```
   Origen        Servidor Recipiente    Recipiente
-------------       -------------     -------------
| --------- |       | --------- |     | --------- |
| |  MTA  | |       | |  MTA  | |     | |  MUA  | |
| --------- |       | --------- |     | --------- |
| |  MUA  | |       | |  MDA  | |     -------------
| --------- |       | --------- |
| |  MDA  | |       | | Buzón | |
| --------- |       | --------- |
| | Buzón | |       -------------
| --------- |
-------------
```

## Receptor con Servidor Local

```
Origen --*SMTP*--> MTA origen --> MDA origen --> Buzón de enviados
                              \--> MTA receptor local --> MDA receptor local --> Buzón recibidos

MUA receptor local --*IMAP/POP*--> MTA receptor local --> MDA receptor local --> Buzón recibidos --> MDA receptor local --> MTA receptor local --*IMAP/POP*--> MUA receptor local
```

```
   Origen          Servidor Origen          Receptor
-------------       -------------        -------------
| --------- |       | --------- |        | --------- |
| |  MUA  | |       | |  MTA  | |        | |  MTA  | |
| --------- |       | --------- |        | --------- |
-------------       | |  MDA  | |        | |  MUA  | |
                    | --------- |        | --------- |
                    | | Buzón | |        | |  MDA  | |
                    | --------- |        | --------- |
                    -------------        | | Buzón | |
                                         | --------- |
                                         -------------
```

## Recibir Correos por POP/IMAP

```
MUA cliente --*POP/IMAP*--> MTA --> MDA --> Buzón --> MDA --> MTA -->MUA cliente
```


```
   Cliente         Servidor Cliente
-------------       -------------
| --------- |       | --------- |
| |  MUA  | |       | |  MTA  | |
| --------- |       | --------- |
-------------       | |  MDA  | |
                    | --------- |
                    | | Buzón | |
                    | --------- |
                    -------------
```
