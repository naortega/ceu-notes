# 4. Actividad IIS

_Nicolás A. Ortega Froysa_

## Configuración del Dominio

Para poder configurar el dominio, tenemos que añadir nuestro dominio,
`asir.com`, al archivo `C:\Windows\System32\drivers\etc\hosts`.

![IIS](imgs/00-hosts.png)

Al hacer esto, se puede hacer un ping a `asir.com` en el CMD y nos dará
respuesta `127.0.0.1`.

![IIS](imgs/01-ping.png)

## Configuración del Sitio Web

Configuramos lo básico de nuestro servidor. Primero creamos nuestra carpeta que
servirá de nuestro directorio raíz de nuestro sitio.

![IIS](imgs/02-new-folder.png)
![IIS](imgs/03-new-site.png)

También crearemos una página de inicio denominada `lapaginadeinicio.html`, y
apuntaremos a ella para cargar como página predeterminada.

![IIS](imgs/04-start-page.png)
![IIS](imgs/05-set-as-index.png)
![IIS](imgs/06-start-page-demo.png)

## Directorio Virtual (Alias/Enlace)

Para configurar nuestro enlace, primero creamos el directorio `C:\enlacesim`. A
partir de ahí configuramos nuestro directorio virtual en el servidor a partir de
nuestro sitio web.

![IIS](imgs/07-virtual-directory.png)
![IIS](imgs/08-configure-virtual-directory.png)
![IIS](imgs/09-link-demo.png)

## Derechos de Autor

Copyright © 2022 Nicolás A. Ortega <nicolas@ortegas.org>

Este documento está licenciado bajo las condiciones del CC-BY-ND 4.0
Internacional.
