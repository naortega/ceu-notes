# Actividad Apache en clase (Actividad 1)

Nicolás Andrés Ortega Froysa

## Configuración DNS

En primer lugar, será necesario configurar nuestro DNS local (de nuestra
máquina) para redireccionar la dirección `asir2.es` a nuestra máquina (i.e.
`127.0.0.1`). Esto se puede hacer con el comando siguiente:

```bash
$ echo "127.0.0.1   asir2.es" >> /etc/hosts

```

Con esto, si le hacemos un `ping` a `asir2.es` nos debería de salir respuestas
de nuestra propia máquina:

```
root@debian-redes:~# ping asir2.es
PING asir2.es (127.0.0.1) 56(84) bytes of data.
64 bytes from localhost (127.0.0.1): icmp_seq=1 ttl=64 time=0.016 ms
64 bytes from localhost (127.0.0.1): icmp_seq=2 ttl=64 time=0.041 ms
64 bytes from localhost (127.0.0.1): icmp_seq=3 ttl=64 time=0.046 ms
^C
--- asir2.es ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2053ms
rtt min/avg/max/mdev = 0.016/0.034/0.046/0.013 ms
```

## Configuración Inicial de Apache

La raíz de nuestro sitio web se encontrará en `/var/www/asir2.es`, luego creamos
este directorio con el comando `mkdir`. Luego, para configurar el Apache como
tal, nos vamos al directorio `/etc/apache2/sites-available/` y hacemos una copia
del archivo `000-default.conf` que se denominará `asir2.es.conf`, y nos interesa
hacer un par de cambios al nuevo archivo:

1. Descomentar la línea que pone `ServerName` y cambiar el dominio a `asir2.es`.
2. Cambiar la directiva `DocumentRoot` a `/var/www/asir2.es` (la raíz de nuestra
   web).
3. Por motivos de seguridad, deshabilitamos la opción de `Indexes` para
   `/var/www/` en nuestro `/etc/apache2/apache2.conf` para que sea de la forma
   siguiente:

   ```
   <Directory /var/www/>
   	Options FollowSymLinks
	AllowOverride None
	Require all granted
   </Directory>
   ```

Una vez hecho estos cambios, lo aplicamos primero cambiando al directorio
`/etc/apache2/sites-enabled/` y haciendo un enlace simbólico a nuestra
configuración nueva y recargando la configuración del servicio de apache:

```bash
$ ln -s ../sites-available/asir2.es.conf ./asir2.es.conf
$ systemctl reload apache2.service
```

## Archivos de la Web

En primer lugar, hemos de configurar nuestro servidor para reconocer el archivo
de índice `esinicio.html`. Esto lo hacemos añadiendo la directiva
`DirectoryIndex esinicio.html` a nuestro _VirtualHost_. Luego el archivo lo
creamos con el comando siguiente:

```bash
$ echo "Empezamos" > /var/www/asir2.es/esinicio.html
```

También configuraremos un documento de error personalizado para los errores 403.
Para hacer esto añadimos primero la directiva en nuestra configuración
`ErrorDocument 403 /error403.html`. Una vez guardado, creamos el archivo como
tal con el comando siguiente:

```bash
$ echo "No se puede acceder" > /var/www/asir2.es/error403.html
```

Cuando recargamos nuestra configuración, veremos que ya están disponibles estos
archivos.

## Permisos de Directorios

Vamos a crear ahora un directorio en la que no tiene acceso el usuario que se
denomina `demo/`. Esta carpeta la creamos en primer lugar con `mkdir`. No
queremos que nadie tenga acceso a este directorio, así que ponemos lo siguiente
en nuestra configuración:

```
<Directory /var/www/asir2.es/demo/>
        Options FollowSymLinks
        AllowOverride All
        Require all denied
</Directory>
```

Al recargar la configuración, podemos probar que ha funcionado correctamente
creando un archivo `esinicio.html` en `demo/` y viendo si podemos cargar
`http://asir2.es/demo/`. Si nos da nuestra página de error 403, entonces está
bien configurado.

Luego creamos otra carpeta inferior que se denomina `otro/` dentro de `demo/` al
que los usuarios sí pueden acceder. Como antes, creamos el directorio con
`mkdir`, y luego añadimos lo siguiente a nuestra configuración:

```
<Directory /var/www/asir2.es/demo/otro/>
        Options FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>
```

Después de recargar la configuración, podemos asegurarnos de que funciona
añadiendo un archivo `esinicio.html` al directorio y reiniciando.

## Cambio de Puerto

Para cambiar de puerto simplemente hemos de cambiar el puerto de nuestro
_VirtualHost_ de 80 a 8031, de tal modo que se parece a lo siguiente:

```
<VirtualHost *:8031>
```

Y también hay que añadir el puerto a nuestro archivo `/etc/apache2/ports.conf`:

```
Listen 8031
```

## Alias

Creamos primero nuestros ficheros para el alias:

```bash
$ mkdir /home/asir2.es/
$ touch enlace.html
```

Una vez creado, configuramos el directorio con la directiva siguiente:

```
<Directory /home/asir2.es/>
        Options FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>
```

Y luego, en nuestro _VirtualHost_, añadimos la directiva de alias:

```
Alias /enlace/ "/home/asir2.es/enlace.html"
```

## Autenticación

Para crear un directorio protegido por autenticación, primero creamos el
directorio `admin` con `mkdir`. Luego creamos una directiva para este
directorio:

```
<Directory /var/www/asir2.es/admin/>
        Options FollowSymLinks
        AllowOverride All
        Require all denied
        <RequireAll>
                AuthName "Acceso restringido"
                AuthType Basic
                AuthUserFile /etc/apache2/htpass
                Require user usuario
        </RequireAll>
</Directory>
```

Luego creamos el archivo `htpass` con el comando siguiente, introduciendo la
contraseña que queremos (e.g. `laClave-2020`), y moviendo el archivo a
`/etc/apache2/`:

```bash
$ htpasswd -c htpass admin
$ mv htpass /etc/apache2/
```

## Derechos de Autor

Copyright © 2022 Nicolás A. Ortega Froysa <nicolas@ortegas.org>
License: CC-BY-ND 4.0 International
