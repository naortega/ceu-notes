# Configuración de un Servidor Fossil

Nicolás A. Ortega Froysa

## Introducción

Fossil es un sistema de gestión de versiones, considerado generalmente como
alternativa a Git, pero que contiene además soporte para muchos elementos que
suelen estar asociados a los proyectos de código, como serían Wiki y una
herramienta de gestión de tarea. Es una herramienta poco usada en ámbitos
generales, pero sí se usa para el proyecto conocido SQLite. También tiene la
ventaja de que todo está contenido dentro del mismo repositorio, y por tanto se
puede compartir fácilmente, y descentralizar el control del código.

## Instalación de Fossil

Antes de configurar nuestro servicio hemos de instalar Fossil en nuestro
servidor. Como estamos usando un sistema Debian, lo instalamos utilizando el
comando siguiente:

```
# apt install fossil
```

## Creación del Repositorio

Una vez instalado, vamos a crear el repositorio como tal, que consta de un
archivo que guardaremos en el servidor. Para hacer esto primero hemos de crear
el usuario y grupo que se encargará de gestionar este repositorio.

```
# groupadd fossil
# useradd -d /srv/fossil -m -g fossil -s /usr/sbin/nologin fossil
```

Esto creará el directorio `/srv/fossil` que lo usaremos para guardar el archivo
del repositorio. Para crear el repositorio como tal hemos de correr el comando
siguiente:

```
# cd /srv/fossil
# fossil init semanaverde.fossil
project-id: 88b14f646618a2bbef27386242a857e56c55ced3
server-id:  23476a9b53e66b1866c04de1d59a4947bb068869
admin-user: root (initial password is "qbHCunmSj8")
```

Esto nos mostrará el usuario y la contraseña del usuario administrador, que
deberíamos guardarlo para luego.

Finalmente, para que el usuario `fossil` pueda acceder al archivo (y más
importante, modificarlo) cambiamos el dueño del archivo:

```
# chown fossil:fossil semanaverde.fossil
```

## Configuración del Servidor

El servidor de Fossil (web) se accede mejor por medio de un SCGI. Para esto, lo
que más nos conviene (en un sistema Debian) es crear un servicio de Systemd para
que se inicialice al iniciar el sistema. Para eso crearemos
`/usr/lib/systemd/system/fossil-server.service` con el contenido siguiente:

```
[Unit]
Description=Fossil user server
After=network-online.target

[Service]
WorkingDirectory=/srv/fossil
ExecStart=/usr/bin/fossil server --scgi --localhost --port 8080 /srv/fossil/semanaverde.fossil
User=fossil
Group=fossil
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```

Ahora ya podremos gestionar nuestro servicio por medio de `systemctl` como
cualquier otro servicio. Como queremos habilitar y encender este servicio,
corremos los comandos siguientes:

```
# systemctl enable fossil-server.service
# systemctl start fossil-server.service
```

## Configuración del Proxy con Nginx

Una vez que tengamos el servicio puesto, es necesario crear un proxy que apunta
(y traduce) a nuestro servidor Fossil. En nuestro caso usaremos el servidor HTTP
Nginx. Crearemos un archivo de configuración Nginx con el __path__
`/etc/nginx/sites-available/repo.semanaverde.com.es.conf` con el contenido
siguiente:

![Configuración Nginx Fossil](nginx-fossil.jpg)

Ahora creamos un enlace simbólico para que Nginx vea esta configuración,
habilitándolo:

```
# cd /etc/nginx/sites-enabled
# ln -s ../sites-available/repo.semanaverde.com.es.conf ./
```

Comprobamos que está escrito correctamente el código:

```
# nginx -t
```

Si todo sale bien, reiniciamos Nginx para poder tener acceso a nuestro servicio
de Fossil:

```
# systemctl reload nginx.service
```

Ya con esto podremos acceder a nuestro servicio Fossil desde
`repo.semanaverde.com.es`.
