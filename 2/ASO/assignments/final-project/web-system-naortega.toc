\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Resumen de Topología}{2}{section.1}%
\contentsline {section}{\numberline {2}Configuración Inicial Windows Server}{3}{section.2}%
\contentsline {section}{\numberline {3}Configuración Inicial Linux}{3}{section.3}%
\contentsline {subsection}{\numberline {3.1}Instalación}{4}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Post-Instalación}{4}{subsection.3.2}%
\contentsline {section}{\numberline {4}Active Directory}{7}{section.4}%
\contentsline {subsection}{\numberline {4.1}Instalación}{7}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Configuración en Linux}{10}{subsection.4.2}%
\contentsline {section}{\numberline {5}Terminal de Administración}{11}{section.5}%
\contentsline {section}{\numberline {6}Base de Datos}{11}{section.6}%
\contentsline {section}{\numberline {7}Servidor Web}{13}{section.7}%
\contentsline {subsection}{\numberline {7.1}Nginx}{13}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Certificado SSL}{15}{subsection.7.2}%
\contentsline {subsection}{\numberline {7.3}MediaWiki}{17}{subsection.7.3}%
\contentsline {section}{\numberline {8}Derechos de Autor y Licencia}{19}{section.8}%
