\documentclass[12pt,a4paper,titlepage]{article}
\usepackage[spanish]{babel}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}

\title{Gestión de un Servidor OpenBSD}
\author{Nicolás A. Ortega Froysa}

\begin{document}
\maketitle

\tableofcontents
\pagebreak

\section{Introducción}

En esta práctica vamos a ver cómo es la administración de sistema de un servidor
de {\it OpenBSD}. Los objetivos son los siguientes:

\begin{itemize}
	\item Aprender los comandos básicos del sistema.
	\item Ver en qué se diferencia OpenBSD de un sistema Linux.
	\item Ver cómo cambia la carga del sistema mientras más servicios vayamos
		proveyendo.
\end{itemize}

La instalación en sí no forma parte de este documento, pero generalmente el
instalador guiado es bastante intuitivo.

Lo que caracteriza a OpenBSD especialmente, respecto a otros sistemas UNIX (y
otros sistemas que hemos visto en clase) son dos cosas. Primero, que no es una
distribución de Linux, sino de BSD. Son sistemas completamente distintos, aunque
siguen (generalmente) una filosofía/arquitectura similar, basándose los dos en
UNIX. Segundo, que OpenBSD, entre los otros sistemas BSD, es el más
especializado en seguridad, hasta el punto de sacrificar cualquier otro aspecto
de la computación a favor siempre de la seguridad. Es por este motivo que
deshabilitaron en su momento el {\it hyper-threading} de los procesadores Intel
por miedo a {\it Spectre}.\footnotemark

\footnotetext{\url{https://www.theregister.com/2018/06/20/openbsd\_disables\_intels\_hyperthreading/}}

\section{Configuración Inicial}

Generalmente la configuración de OpenBSD ha sido bastante simple, y la mayoría
de las opciones están en su valor por defecto.  El teclado -- i.e.\ keymap --
usado es el estadounidense, simplemente porque para la informática es la más
cómoda. La máquina lleva el {\it hostname} de {\tt puffy-nortega}.

En cuanto a los usuarios, existe un usuario {\it root} con contraseña {\tt
toor}, y otro usuario {\it nicolas} con contraseña {\tt nicolas}. Durante la
instalación se ha configurado que no sea posible acceder a {\it root} por SSH,
ya que esto hace que el sistema sea más vulnerable -- más todavía con este tipo
de contraseña.

El disco se ha particionado con una sola partición para simplificar este proceso
de instalación. Se podría instalar con más particiones, repartiendo así los
datos y haciendo más fácil el proceso de hacer copias de respaldo (que sean de
una sola partición), pero eso no entra dentro de los objetivos de esta práctica.

En cuestión de configuración de la máquina (virtual), tiene a su disposición
2GiB de RAM, y un {\it core} para procesar.

Finalmente, en OpenBSD se dividen los archivos de sistema en diferentes {\it
sets}. Éstos se pueden usar para instalar diferentes componentes del sistema,
desde el {\it kernel} hasta algunos juegos de consola. Éstos están los {\it
sets} instalados:

\begin{itemize}
	\item {\tt bsd}: {\it kernel} de BSD ({\bf obligatorio}).
	\item {\tt bsd.mp}: {\it kernel} multi-procesador.
	\item {\tt bsd.rd}: {\it kernel} de uso en memoria.
	\item {\tt base72.tgz}: sistema base o {\it userland} ({\bf obligatorio}).
	\item {\tt comp72.tgz}: colección de compiladores.
	\item {\tt man72.tgz}: páginas de manual.
\end{itemize}

\begin{figure}
	\center
	\includegraphics[width=0.75\textwidth]{imgs/00-initial-load.png}
	\caption{Primer arranque de OpenBSD.}
	\label{fig:initial-load}
\end{figure}

Al inicializar la máquina lleva una carga de sistema extremadamente baja, ya que
está corriendo tan sólo lo mínimo (figura \ref{fig:initial-load}).

\section{Instalación de Paquetes}

Para administrar paquetes en OpenBSD se hace uso de las herramientas de paquete
de OpenBSD. Los comandos se pueden reconocer porque llevan la patrón {\tt
pkg\_*}. En particular, las que nos interesan son tres:

\begin{itemize}
	\item {\tt pkg\_add}: instalar y actualizar.
	\item {\tt pkg\_delete}: eliminar/desinstalar.
	\item {\tt pkg\_info}: buscar información.
\end{itemize}

Para instalar los paquetes que nos interesan vamos a correr el comando siguiente:

\begin{verbatim}
root# pkg_add nginx mariadb-server mariadb-client \
 rsync
\end{verbatim}

\section{Configuración de Servicios}

El control de los servicios en OpenBSD se hace con el comando {\tt rcctl}, ya
que OpenBSD no usa Systemd. Generalmente se usa de una forma muy similar a
Systemd, usando los subcomandos siguientes:

\begin{itemize}
	\item {\tt start}: iniciar un servicio.
	\item {\tt stop}: parar un servicio.
	\item {\tt restart}: reiniciar el servicio.
	\item {\tt enable}: habilitar un servicio para iniciarse al arrancar la
		máquina.
	\item {\tt disable}: deshabilitar un servicio para que no se inicie cuando
		arranque la máquina.
	\item {\tt check}: mostrar el estado de un servicio.
	\item {\tt ls}: mostrar una lista de los servicios.
\end{itemize}

\subsection{Nginx}

\begin{table}[!ht]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		{\bf Nombre} & Nginx \\ \hline
		{\bf Descripción} & Un servidor web moderno. \\ \hline
		{\bf Dir. Config.} & {\tt /etc/nginx/} \\ \hline
		{\bf Dir. Logs} & {\tt /etc/nginx/logs/} \\ \hline
	\end{tabular}
	\label{tbl:nginx-descriptor}
	\caption{Datos sobre Nginx.}
\end{table}

En nuestra configuración de Nginx vamos a crear una página simple que se pueda
acceder desde fuera en el puerto 80. Por defecto, Nginx en OpenBSD usa el
directorio {\tt /var/www/htdocs/} como raíz del servidor. Aquí añadiremos un
archivo {\tt index.html} para que responda con un código {\tt 200 OK}. Iniciamos
el servicio (y lo habilitamos) con el comando siguiente:

\begin{verbatim}
root# rcctl enable nginx
root# rcctl start nginx
nginx(ok)
\end{verbatim}

\begin{figure}
	\centering
	\includegraphics[width=0.75\textwidth]{imgs/01-nginx-test.png}
	\caption{Prueba de Nginx.}
	\label{fig:nginx-test}
\end{figure}

Ahora, si probamos acceder a nuestro servidor, veremos el archivo que hemos
creado (figura \ref{fig:nginx-test}).

A este servidor le podemos hacer un {\it stress test} utilizando la herramienta
{\it wrk}, que sirve para hacer {\it benchmarking} de servicios HTTP. Desde un
cliente corremos el comando siguiente, que nos hará una prueba de 50 conexiones
paralelas, sobre 10 hilos, durante 20 segundos. Vemos que el {\it load} aumenta
de forma considerable (figura \ref{fig:load-wrk-test}).

\begin{verbatim}
$ wrk -c 50 -t 10 -d 20 http://172.16.40.3/
\end{verbatim}

\begin{figure}
	\centering
	\includegraphics[width=0.75\textwidth]{imgs/02-load-wrk-test.png}
	\caption{{\it Load} resultante de una prueba con {\it wrk}.}
	\label{fig:load-wrk-test}
\end{figure}

\subsection{MariaDB}

\begin{table}[!ht]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		{\bf Nombre} & MariaDB \\ \hline
		{\bf Descripción} & Sistema de gestión de bases de datos. \\ \hline
		{\bf Archivo Conf.} & {\tt /etc/my.cnf} \\ \hline
		{\bf Dir. Logs} & {\tt /var/db/mysql/} \\ \hline
	\end{tabular}
	\label{tbl:mariadb-descriptor}
	\caption{Datos sobre MariaDB.}
\end{table}

MariaDB requiere de una configuración inicial más manual, así que en primer
lugar se tendrá que inicializar (y habilitar) el servicio, que se denomina {\tt
mysqld} -- esto es porque MariaDB tiene como objetivo ser un reemplazo {\it
in-place} de MySQL.

\begin{verbatim}
root# rcctl enable mysqld
root# rcctl start mysqld
mysqld(ok)
\end{verbatim}

Lo configuramos usando el comando {\tt mysql\_secure\_install}.  Esto instalará
las tablas dentro del directorio {\tt /var/mysql/} y podremos definir la
contraseña de {\it root}, que será {\tt Rooty} (y algunas opciones de MariaDB).
Podemos acceder a este base de datos usando el comando siguiente desde el
usuario {\it root}, e introduciendo la contraseña que hemos asignado:

\begin{verbatim}
puffy-nortega# mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 13
Server version: 10.9.3-MariaDB OpenBSD port:
mariadb-server-10.9.3v1

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and
others.

Type 'help;' or '\h' for help. Type '\c' to clear the current
input statement.

MariaDB [(none)]>
\end{verbatim}

\subsection{Servicio RSyncd}

\begin{table}[!ht]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		{\bf Nombre} & RSyncd \\ \hline
		{\bf Descripción} & Servicio  \\ \hline
		{\bf Archivo Conf.} & {\tt /etc/rsyncd.conf} \\ \hline
		{\bf Archivo Log} & {\tt /var/log/rsyncd.log} \\ \hline
	\end{tabular}
	\label{tbl:rsyncd-descriptor}
	\caption{Datos sobre RSyncd.}
\end{table}

{\it RSync} es un servicio/herramienta de transferencia de archivos. Cuando se
usa en modo {\it demonio}, puede crear un servidor de ficheros disponible en el
puerto 873 por defecto. Puede ser útil para sincronizar archivos y repositorios.
Se configura creando un archivo de configuración en {\tt /etc/rsyncd.conf}, y lo
configuramos con lo siguiente:

\begin{verbatim}
uid = nobody
gid = nobody
use chroot = no
max connections = 4
pid file = /var/run/rsyncd.pid
lock file = /var/run/rsyncd.lock
log file = /var/log/rsyncd.log

[files]
path = /srv/rsync
comment = Mis archivos.
read only = true
list = true
\end{verbatim}

Esto creará un servicio que proveerá los ficheros que se encuentran en el
directorio {\tt /srv/rsync}. Para nuestra prueba vamos a poner un par de
archivos pesados -- unos ISOs -- para descargarlos desde nuestro cliente.

Habilitamos e iniciamos el servicio de RSyncd usando los comandos siguientes:

\begin{verbatim}
root# rcctl enable rsyncd
root# rcctl start rsyncd
rsyncd(ok)
\end{verbatim}

Una vez habilitado el servicio, y creado los archivos, podemos poner a prueba
nuestro servicio corriendo el comando siguiente desde nuestro cliente:

\begin{verbatim}
$ rsync -avz 172.16.40.3::files ./
\end{verbatim}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.75\textwidth]{imgs/03-load-rsyncd.png}
	\caption{Prueba de carga de RSyncd.}
	\label{fig:load-rsyncd}
\end{figure}

A partir de aquí, veremos que aumenta considerablemente la carga en nuestro
servidor (figura \ref{fig:load-rsyncd}), mientras el cliente va descargando los
ficheros. Además, conllevará una carga adicional, ya que con el {\it flag} {\tt
-z} comprimirá los datos antes de enviarlos para ser descomprimidos en nuestro
cliente.

\section{Conclusión}

OpenBSD, aunque tiene sus diferencias respecto a Linux, es bastante similar. Lo
más diferente sería su sistema de servicios, que se controla a través de {\tt
rcctl} en vez de {\tt systemctl}. También su sistema de administración de
paquetes es distinto a lo normal que se encuentra en Ubuntu o Debian, y quizá se
podría decir que es un poco más lento.

La forma de administrar el sistema es más conforme a la tradición de UNIX, que
también lo seguía más Linux en sus inicios. Hay una dependencia más fuerte en la
configuración de ficheros.

Es más, como OpenBSD está optimizado para la seguridad, sería muy buena opción
para un servidor, ya que estaría expuesto al público.

Un negativo que podría tener es que como no es un Linux, sino un BSD, tiene
menos soporte, hay menos recursos en la web que podemos consultar, y suele tener
menos servicios disponibles -- aunque los principales siempre suelen estar.

\pagebreak

\section{Derechos de Autor y Licencia}

\noindent
Copyright \copyright\ \the\year\ Nicolás A. Ortega Froysa
<nicolas@ortegas.org> \\
\\
Este documento se distribuye bajo los términos y condiciones de la licencia
Creative Commons Attribution No Derivatives 4.0 International.

\end{document}
