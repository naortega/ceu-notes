\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Introducción}{2}{section.1}%
\contentsline {section}{\numberline {2}Configuración Inicial}{2}{section.2}%
\contentsline {section}{\numberline {3}Instalación de Paquetes}{3}{section.3}%
\contentsline {section}{\numberline {4}Configuración de Servicios}{4}{section.4}%
\contentsline {subsection}{\numberline {4.1}Nginx}{4}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}MariaDB}{6}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Servicio RSyncd}{6}{subsection.4.3}%
\contentsline {section}{\numberline {5}Conclusión}{8}{section.5}%
\contentsline {section}{\numberline {6}Derechos de Autor y Licencia}{9}{section.6}%
