\babel@toc {spanish}{}
\beamer@sectionintoc {1}{Introducción}{3}{0}{1}
\beamer@sectionintoc {2}{Instalación}{4}{0}{2}
\beamer@sectionintoc {3}{Gestión de Paquetes}{7}{0}{3}
\beamer@sectionintoc {4}{Gestión y Configuración de Servicios}{9}{0}{4}
\beamer@sectionintoc {5}{Prueba de {\it Stress}}{14}{0}{5}
\beamer@sectionintoc {6}{Fin}{16}{0}{6}
