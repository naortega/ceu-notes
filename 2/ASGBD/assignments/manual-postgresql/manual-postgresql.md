# Manual de Instalación y Configuración PostgreSQL

_Nicolás A. Ortega Froysa_

## Instalación de PostgreSQL

Desde una máquina de GNU & Linux basada en Debian, podemos instalar el
PostgreSQL desde la línea de comando con el siguiente comando como `root`:

```bash
apt install postgresql
```

Generalmente en los sistemas basados en Debian los servicios se inicializan al
instalarse, pero siempre conviene asegurarnos. Para esto veremos la salida del
estado actual de este servicio:

![PostgresSQL status](imgs/00-postgresql-status.png)

## Configuración de PostgreSQL

Una vez instalado el PostgreSQL, hay que configurarlo para poder accederlo
remotamente. Primero hemos de configurar la contraseña del usuario `postgres`.
Esto se hace entrando en el usuario de sistema `postgres`, y entrando en la base
de datos usando el comando `psql`. Una vez conectado a la base de datos
configuramos la contraseña usando el comando siguiente:

```
alter user postgres with password 'postgres';
```

![Change postgres password](imgs/01-change-postgres-passwd.png)

Con esto aún es necesario cambiar la configuración de PostgreSQL como tal para
permitir acceso remoto (desde fuera de la misma máquina). Para esto, primero
editamos el archivo de configuración `/etc/postgresql/13/main/postgresql.conf`
para cambiar la directiva de `listen_addresses` a lo siguiente:

```
listen_addresses = '*'
```

Luego editamos el archivo de `/etc/postgresql/13/main/pg_hba.conf` y lo
cambiamos de la forma siguiente:

```
#host    all             all             127.0.0.1/32            md5
host    all             all             0.0.0.0/0            md5
```

Al hacer estos cambios podemos reiniciar el servicio para efectuarlos:

```
systemctl restart postgresql.service
```

## Firewalld

Para tener un sistema seguro será necesario tener un _firewall_ instalado. Para
esto vamos a instalar a `firewalld`, una herramienta de RedHat. Esto se hace con
el comando `apt install firewalld`. Al instalarlo, para configurar que deje
abierto el puerto de PostgreSQL corremos los siguientes comandos como root:

```
$ firewall-cmd --permanent --zone=public --add-port=5432/tcp
$ firewall-cmd --reload
```

Esto abrirá el puerto 5432 al público.

## Prueba

Finalmente, para hacer una prueba, podemos conectarnos desde otra máquina a
nuestro servidor corriendo el comando siguiente e introduciendo la contraseña
que hemos configurado antes:

```
psql -h <address> -p <port> -U postgres postgres
```

![PostgreSQL connect](imgs/02-postgresql-connect.png)

## Derechos de Autor

Copyright © 2022 Nicolás A. Ortega Froysa <nicolas@ortegas.org>

Este documento se distribuye bajo los términos y condiciones de la licencia
Creative Commons Attribution No Derivatives 4.0 International.
