# Instalación de Oracle XE en un Docker CentOS

Nicolás A. Ortega Froysa

## Instalación de Docker en Ubuntu

En primer lugar, se debería de preparar el repositorio, para que Ubuntu pueda
acceder y actualizar el software conforme vayan saliendo nuevas versiones. Esto
se puede hacer con los siguientes comandos:

```bash
apt update
apt install ca-certificates curl gnupg lsb-release
mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb \[arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg\] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Pasado esto, queremos instalar Docker como tal. Esto lo podemos hacer ya con el
comando siguiente:

```bash
apt install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

Una vez instalado, debemos de inicializar Docker con el sistema de init de
Systemd, corriendo el siguiente comando:

```bash
systemctl start docker
# O si queremos que se inicialice automáticamente
systemctl enable --now docker
```

Ya tenemos Docker instalado y configurado en nuestro sistema para poder usarlo.

## Instalación de la Imagen de Docker

Ya que tenemos a Docker instalado, debemos de bajar la imagen que nos interesa.
Como queremos instalar una imagen de CentOS, corremos el siguiente comando:

```bash
docker pull centos
```

Una vez descargada, creamos un container con esta imagen. Con el mismo comando,
también vamos a abrir un shell interactivo con el que vamos a configurar nuestro
container (`-it`). Vamos a nombrar nuestro container `centos-oracle` (`--name
centos-oracle`). También vamos a redireccionar los puertos para tener acceso
externo a MariaDB (`-p 1521:1521`).

```bash
docker run -it --name centos-oracle -p 1521:1521 centos
```

Una vez dentro, debemos de instalar Oracle XE. para esto tendremos que descargar
e instalar el paquete de preinstalado de Oracle XE de su página.

```bash
yum update
curl -o oracle-database-preinstall-21c-1.0-1.el8.x86_64.rpm https://yum.oracle.com/repo/OracleLinux/OL8/latest/x86_64/getPackage/oracle-database-preinstall-21c-1.0-1.el8.x86_64.rpm
dnf -y localinstall oracle-database-preinstall-21c-1.0-1.el8.x86_64.rpm
```

Luego debemos instalar el paquete en sí. Esto se puede hacer con el comando
siguiente:

```bash
curl -LO https://download.oracle.com/otn-pub/otn_software/db-express/oracle-database-xe-21c-1.0-1.ol8.x86_64.rpm
yum install -y localinstall oracle-database-xe-21c-1.0-1.ol8.x86_64.rpm
```

Una vez instalado ya podemos configurar nuestro base de datos Oracle ejecutando
el comando siguiente:

```bash
/etc/init.d/oracle-xe-21c configure
```

Este __script__ nos preguntará acerca de las contraseñas de los usuarios
administrativos SYS, SYSTEM, y PDBADMIN.

Finalmente configuramos los variables de entorno Oracle en nuestro __shell__ de
bash:

```bash
export ORACLE_SID=XE
export ORAENV_ASK=NO
. /opt/oracle/product/21c/dbhomeXE/bin/oraenv
```

Una vez hecho esto, ya podemos conectarnos por medio del puerto 1521 a nuestro
`localhost` con una herramienta como SQLDeveloper.
