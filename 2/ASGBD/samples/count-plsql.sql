DECLARE
    office_id oficina.codigo_oficina%type;
    cnt INT;
    CURSOR my_cursor IS
        SELECT codigo_oficina FROM oficina;
BEGIN
    cnt := 0;
    OPEN my_cursor;
    LOOP
        FETCH my_cursor INTO office_id;
        EXIT WHEN my_cursor%NOTFOUND;
        cnt := cnt + 1;
    END LOOP;
    dbms_output.put_line(cnt);
    CLOSE my_cursor;
END;