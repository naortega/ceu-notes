DECLARE
    tbl_name USER_TABLES.TABLE_NAME%type;
    CURSOR table_cursor IS SELECT TABLE_NAME FROM USER_TABLES;
BEGIN
    OPEN table_cursor;
    LOOP
        FETCH table_cursor INTO tbl_name;
        EXIT WHEN table_cursor%NOTFOUND;
        dbms_output.put_line('ALTER TABLE ' || tbl_name || ' ADD fecha_creacion DATE;');
        dbms_output.put_line('ALTER TABLE ' || tbl_name || ' ADD usuario_creacion VARCHAR2(32);');
    END LOOP;
    CLOSE table_cursor;
END;
/