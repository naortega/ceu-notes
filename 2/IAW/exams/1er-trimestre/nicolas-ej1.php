<!DOCTYPE html>
<html>
	<head>
		<title>Dice Game</title>
	</head>
	<body>
		<h1>Ejercicio 1: El Juego del Dado Más Alto</h1>
		<p>Actualice la página para una nueva tirada.</p>
<?php
$player1 = rand(1,6);
$player2 = rand(1,6);
?>
		<table>
			<tr>
				<th>Jugador 1</th>
				<th>Jugador 2</th>
				<th>Resultado</th>
			</tr>
			<tr>
				<td bgcolor="red" ><img src="imgs/<?= $player1 ?>.svg" /></td>
				<td bgcolor="blue" ><img src="imgs/<?= $player2 ?>.svg" /></td>
				<td>
<?php
if($player1 > $player2)
	echo "Ha ganado el jugador 1";
else if($player2 > $player1)
	echo "Ha ganado el jugador 2";
else
	echo "Han empatado";
?>
				</td>
			</tr>
		</table>
		<p>Última modificación de esta página: <?= date('Y-m-d', filemtime(__FILE__)) ?></p>
	</body>
</html>
