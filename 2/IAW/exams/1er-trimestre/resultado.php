<!DOCTYPE html>
<html>
	<head>
		<title>Resultado</title>
	</head>
	<body>
		<h1>Encuesta (Resultado)</h1>
<?php
if(isset($_POST["favorite"])) {
?>
		<p>Su opción es:</p>
		<img src="imgs/<?= $_POST["favorite"] ?>.svg" width="100px" />
<?php
} else {
?>
		<p><b>ERROR:</b> No ha seleccionado ninguno.</p>
<?php
}
?>
		<p><a href="nicolas-ej2.php" >Volver al formulario</a></p>
		<p>Última modificación de esta página: <?= date('Y-m-d', filemtime(__FILE__)) ?>
	</body>
</html>
