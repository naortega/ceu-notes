\documentclass[12pt,a4paper,titlepage]{article}
\usepackage[spanish]{babel}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{minted}

\title{Tema V Ejercicio II: MediaWiki}
\author{Nicolás A. Ortega Froysa}

\begin{document}
\maketitle

\tableofcontents
\pagebreak

\section{Introducción}

En este trabajo queremos explorar el proceso de instalación y gestión de {\it
MediaWiki}, el software de enciclopedias online más utilizado en la web. Quizá
su uso más conocido sería en la página de {\it Wikipedia}.

Queremos saber cómo instalarlo en una máquina GNU/Linux, y cómo se gestionaría.
Respecto a esto último, veremos gestión de contenidos y sintáxis del editor de
MediaWiki.

\section{Instalación}

Para la instalación de MediaWiki, deberemos de seguir varios pasos, y antes
incluso de instalarlo en sí, habría que instalar y configurar sus dependencias.

\subsection{Requisitos}

MediaWiki no se instala como una aplicación normal, ya que se accede por medio
de la web. En su lugar, se basa sobre un servidor web y una base de datos. Para
un servidor web, podemos usar casi cualquier opción que permite usar CGIs de
PHP. En nuestro caso vamos a usar Nginx para esto. Es necesario también instalar
PHP con una versión mayor a 7.2, que tiene problemas con MediaWiki.

También será necesario tener una base de datos, por lo que también tenemos
varias opciones. De acuerdo a la página de instalación de MediaWiki podemos usar
SQLite, MariaDB/MySQL, o incluso PostgreSQL. Nosotros usaremos MariaDB.

\subsubsection{Nginx}

Para instalar Nginx sólo hemos de instalarlo junto a PHP. Instalamos los
paquetes necesarios con el comando siguiente:

\begin{minted}{bash}
apt install nginx php php-fpm php-xml php-mbstring php-intl
\end{minted}

Ahora en el directorio {\tt /etc/nginx/sites-available} creamos un archivo con
el nombre {\tt my-wiki.conf} con el contenido siguiente:

\begin{minted}{text}
# Default server configuration
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    root /var/www/my-wiki;

    # Add index.php to the list if you are using PHP
    index index.html index.htm index.nginx-debian.html index.php;

    server_name lotr.wiki;

    location / {
        # First attempt to serve request as file, then
        # as directory, then fall back to displaying a 404.
        try_files $uri $uri/ =404;
    }

    # pass PHP scripts to FastCGI server
    location ~ \.php$ {
        include snippets/fastcgi-php.conf;

        # With php-fpm (or other unix sockets):
        fastcgi_pass unix:/run/php/php-fpm.sock;
        # With php-cgi (or other tcp sockets):
    }
}
\end{minted}

Luego se crea un enlace simbólico del mismo en la carpeta de {\tt
sites-enabled}. A partir de ahí podemos inicializar el servicio:

\begin{minted}{bash}
systemctl enable nginx
systemctl start nginx
\end{minted}

\subsubsection{MariaDB}

Como en nuestro caso vamos a instalar MariaDB, hemos de correr el comando
siguiente para instalarlo:

\begin{minted}{bash}
apt install mariadb-server
\end{minted}

Ahora lo tenemos que configurar utilizando el comando siguiente, siguiendo los
pasos. Sólo hemos de fijar la contraseña de root (el administrador) y luego
seguir seleccionando las opciones por defecto.

\begin{minted}{bash}
mysql_secure_installation
\end{minted}

Cuando ya lo tenemos configurado empezamos el servicio:

\begin{minted}{bash}
systemctl start mariadb
systemctl enable mariadb
\end{minted}

Ahora tenemos que crear la base de datos:

\begin{minted}{mysql}
CREATE DATABASE my_wiki;
CREATE USER 'mediawiki'@'localhost' IDENTIFIED BY 'mediawiki';
GRANT ALL PRIVILEGES ON mediawiki TO 'mediawiki'@'localhost';
FLUSH PRIVILEGES;
QUIT;
\end{minted}

\subsection{Descarga}

Podemos descargar MediaWiki de la página principal (figura \ref{fig:download}).

\begin{figure}[ht!]
    \includegraphics[width=0.75\textwidth]{imgs/00-download.png}
    \caption{Download}
    \label{fig:download}
\end{figure}

Una vez descargado lo descomprimimos y lo metemos dentro de nuestro directorio
de root, que lo hemos configurado como {\tt /var/www/my-wiki}.

\subsection{Configuración}

\begin{figure}[ht!]
    \includegraphics[width=0.75\textwidth]{imgs/01-config-page.png}
    \caption{Configuration front page.}
    \label{fig:config-page}
\end{figure}

Para la configuración hemos de entrar por el navegador a la dirección de web
{\tt http://lotr.wiki:80/mw-config} (el domonio lo hemos configurado antes en el
archivo {\tt /etc/hosts}). Encontraremos una página de configuración (figura
\ref{fig:config-page}). Elegimos inglés porque es el idioma más universal y más
documentado.

Al pasar a la página siguiente encontraremos primero una página que encuentre si
faltan algunos módulos. Pero como los hemos instalado todos no tendremos
problemas.

En la página siguiente configuramos la base de datos. Para el {\it host}
definimos {\tt localhost}, ya que lo tenemos alojado localmente. Luego, para el
nombre de la base de datos metemos el nombre que hemos definido enteriormente
(i.e.\ {\tt my\_wiki}), y luego como usuario y contraseña lo que definimos
anteriormente. Preguntará si queremos usar la misma cuenta para el acceso web
(post-instalación). A esto le damos que sí (para ahorrar pasos).

\begin{figure}[ht!]
    \includegraphics[width=0.75\textwidth]{imgs/05-wiki-conf.png}
    \caption{Wiki configuration}
    \label{fig:wiki-conf}
\end{figure}

Ahora viene la configuración de la Wiki en sí. Definimos el nombre, el {\it
namespace} (que lo mantendremos igual), y sobre todo lo que nos importa es
configurar el usuario administrador. Nos pedirá si queremos configurar más
opciones en cuyo caso nos mostrará varias opciones que podremos modificar luego
acerca de licencia de la Wiki, quién tiene acceso para editar páginas, etc.

\begin{figure}[ht!]
    \includegraphics[width=0.75\textwidth]{imgs/06-advanced-wiki-settings.png}
    \caption{Advanced Wiki settings.}
    \label{fig:advanced-wiki-settings}
\end{figure}

Una vez instalado, debemos de descargar un archivo {\tt LocalSettings.php}. Este
archivo lo tenemos que mover el archivo a {\tt /var/www/my-wiki}. Ahora si
visitamos a la página principal nos debería de salir la nuestra página principal
(figura \ref{fig:front-page-logged}).

\begin{figure}
    \includegraphics[width=0.75\textwidth]{imgs/08-front-page-logged.png}
    \caption{Front page logged in.}
    \label{fig:front-page-logged}
\end{figure}

\section{Gestión de Contenidos}

Para gestionar contenidos tenemos dos opciones. La primera y más fácil es
utilizar el editor visual. Este usa un modelo WYSIWYG. Parece en ese sentido un
procesador de texto como Word o Google Drive (figura \ref{fig:visual-editor}).

\begin{figure}[ht!]
    \includegraphics[width=0.75\textwidth]{imgs/09-visual-editor.png}
    \caption{Visual editor (WYSIWYG).}
    \label{fig:visual-editor}
\end{figure}

También tenemos la opción de escribir el código con el sintáxis especial de
MediaWiki. Se parece en algo al markdown, pero tiene sus diferencias (figura
\ref{fig:code-editor}).

\begin{figure}[ht!]
    \includegraphics[width=0.75\textwidth]{imgs/10-code-editor.png}
    \caption{Code editor.}
    \label{fig:code-editor}
\end{figure}

Todo esto nos permite crear páginas dinámicas y extensas, interlazadas en un
sistema complejo. Nos proporciona una página simple e informativa.

\begin{figure}[ht!]
    \includegraphics[width=0.75\textwidth]{imgs/11-frodo-page.png}
    \caption{Frodo page.}
    \label{fig:frodo-page}
\end{figure}

\section{Conclusión}

MediaWiki es una herramienta útil y bastante utilizado. Permite crear una
enciclopedia virtual robusta y eficaz para guardar información de forma
sistemática.

\pagebreak

\section{Derechos de Autor y Licencia}

\noindent
Copyright \copyright\ \the\year\ Nicolás A. Ortega Froysa
<nicolas@ortegas.org> \\
\\
Este documento se distribuye bajo los términos y condiciones de la licencia
Creative Commons Attribution No Derivatives 4.0 International.

\end{document}
