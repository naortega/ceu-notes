\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Introducción}{2}{section.1}%
\contentsline {section}{\numberline {2}Instalación}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Requisitos}{2}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Nginx}{2}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}MariaDB}{3}{subsubsection.2.1.2}%
\contentsline {subsection}{\numberline {2.2}Descarga}{4}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Configuración}{4}{subsection.2.3}%
\contentsline {section}{\numberline {3}Gestión de Contenidos}{5}{section.3}%
\contentsline {section}{\numberline {4}Conclusión}{7}{section.4}%
\contentsline {section}{\numberline {5}Derechos de Autor y Licencia}{8}{section.5}%
