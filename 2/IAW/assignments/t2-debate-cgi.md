# Debate CGI

_Autor: Nicolás A. Ortega Froysa_

La CGI es una herramienta muy útil para poder hacer procesamiento de páginas en
el lado del servidor, y así hacer que las páginas sean dinámicas.

Un servidor web, como tal, no puede procesar páginas, sino que simplemente toma
el archivo que pide el cliente y se lo devuelve si existe. Lo que hace la CGI es
habilitar un _gateway_ para que el servidor web pueda pasar la página primero
por otro programa para que lo procese dependiendo de ciertos parámetros. Los más
populares de estas CGI son las de PHP y Perl. Al terminar de procesar la página,
la devuelven ya formateada al servidor para enviarla al cliente.
