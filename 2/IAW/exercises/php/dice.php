<!DOCTYPE html>
<html>
	<head>
		<title>Dice</title>
	</head>
	<body>
<?php
$num_dice = rand(1,10);

if ($num_dice == 1)
	echo "<h1>$num_dice dado</h1>";
else
	echo "<h1>$num_dice dados</h1>";

$max = -1;
for($i = 0; $i < $num_dice; $i++) {
	$roll = rand(1,6);
	if($roll > $max)
		$max = $roll;
?>
		<img src="imgs/<?= $roll ?>.svg" />
<?php
}
?>
		<p>El valor más grande obtenido es <b><?= $max ?></b>.</p>
		<p>Última modificación de esta página: <?= date('Y-m-d', filemtime(__FILE__)) ?>
	</body>
</html>
