\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Introducción}{2}{section.1}%
\contentsline {section}{\numberline {2}Objetivos}{2}{section.2}%
\contentsline {section}{\numberline {3}Metodología}{3}{section.3}%
\contentsline {subsection}{\numberline {3.1}Actividad Empresarial}{3}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Instalaciones y Localización}{3}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Materiales de Producción}{4}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Recursos Humanos}{4}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Análisis del Macroentorno}{4}{subsection.3.5}%
\contentsline {subsubsection}{\numberline {3.5.1}Factores Políticos y Legales}{4}{subsubsection.3.5.1}%
\contentsline {subsubsection}{\numberline {3.5.2}Factores Económicos}{4}{subsubsection.3.5.2}%
\contentsline {subsubsection}{\numberline {3.5.3}Factores Socioculturales}{5}{subsubsection.3.5.3}%
\contentsline {subsubsection}{\numberline {3.5.4}Factores Demográficos}{5}{subsubsection.3.5.4}%
\contentsline {subsubsection}{\numberline {3.5.5}Factores Medioambientales}{5}{subsubsection.3.5.5}%
\contentsline {subsubsection}{\numberline {3.5.6}Factores Tecnológicos}{5}{subsubsection.3.5.6}%
\contentsline {subsection}{\numberline {3.6}Análisis de Microentorno}{6}{subsection.3.6}%
\contentsline {subsection}{\numberline {3.7}Estrategia Comercial}{6}{subsection.3.7}%
