# Spotify:
----------
Spotify fue emprendedor en el sector de la distribución de música. Generalmente
en este sector la distribución digital de la música era por tiendas virtuales,
lo cual el usuario tenía que comprar la música que quería, y estaba limitado a
consumir tan sólo aquello que ha comprado. Spotify cambió de estrategia, optando
por un modelo de streaming, igual que ya se estaba viendo en otras plataformas
como YouTube. De esta forma el usuario podría pagar una subscripción periódica y
tener acceso a todo el repertorio de Spotify. También atraía usuarios por tener
acceso gratuito con anuncios, de tal manera que todos pueden acceder al
repertorio de Spotify, aunque algunos con la inconveniencia de anuncios.

# Uber:
-------
La mayoría de nuestras pertenencias tienen un uso bastante pequeño. La mayor
parte del tiempo están sin usar. Esto incluye a nuestros coches, que realmente
sólo los usamos para llegar de un lado a otro, y pasado eso están sin hacer
nada. Uber se aprovechó de esto para que las personas pudiesen ganar un dinero
en su tiempo libre conduciendo. Al final servía de taxi más barato, y durante
bastante tiempo de problema fiscal.
