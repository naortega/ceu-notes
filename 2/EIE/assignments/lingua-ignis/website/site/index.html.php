<?php
$title = "Inicio";

include("templates/header.html.php");
?>

<h1><?= $title ?></h1>

<p>
A menudo nuestras organizaciones e instituciones no proveen traducciones
necesarias a otros idiomas de documentos estandarizados, por el simple hecho de
que contratar un traductor les puede incurrir un gasto demasiado alto para algo
tan recurrente. A causa de esto, vemos muchos casos en los que hay personas que
no entienden qué están firmando cuando reciben permisos y notificaciones.
Podemos pensar en los padres inmigrantes, que aún les cuesta la lengua nativa
del país en el que están, cuyos hijos tienen que hacer de intérprete en
cualquier ocasión de interacción entre la escuela y los padres.

<p>
<i>Lingua Ignis</i> es una empresa que busca hacer que este tipo de traducciones
sea más fácil, y a menos coste que contratar un traductor para las traducciones.
Notamos que la mayoría de estos documentos suelen ser del mismo tipo, cambiando
tan sólo algunos datos. Por lo tanto, queremos ofrecer una plataforma que provee
plantillas de documentos en varios idiomas, que al rellenar unos pocos de datos
no-traducibles (e.g. nombres propios) y fácilmente formateados (e.g. fechas),
el servicio que proveemos generará los documentos de manera instantánea en las
lenguas solicitadas. Esto facilitaría que nuestros clientes puedan ofrecer
notificaciones de una manera más cómoda a sus propios clientes, en la lengua que
ellos entienden mejor.

<?php
include("templates/footer.html.php");
?>
