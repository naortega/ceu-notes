<?php
$title = "Servicios";

include("templates/header.html.php");
?>

<h1><?= $title ?></h1>

<p>
Aquí puede usted encontrar la lista de todos los servicios que proveemos en
<i>Lingua Ignis</i>, y los precios asociados.

<h2>Colección de Plantillas</h2>

<p>
Este es nuestro servicio principal y distintivo. Consta de colecciones de
plantillas, que se agrupan por tema/sector. Estas plantillas suelen ser de
carácter general, y tan sólo tendrá que introducir los datos pertinentes y
específicos para el caso concreto. Tendrá acceso a todas las plantillas de la
colección que elige para traducir a las lenguas que elija, y podrá usarlas las
veces que quiera.

<p>
Elige la colección que le interesa, junto con las lenguas a las que quiere
traducir los documentos, y dependiendo de su selección, su precio será en
función de eso.

<h3>Colecciones</h3>

<table>
	<tr>
		<th>Colección</th>
		<th>Inglés</th>
		<th>Francés</th>
	</tr>
	<tr>
		<td>Escuela</td>
		<td>25€/mes</td>
		<td>28€/mes</td>
	</tr>
	<tr>
		<td>Salud</td>
		<td>35€/mes</td>
		<td>40€/mes</td>
	</tr>
</table>

<h2>Traducción de Nuevas Plantillas</h2>

<p>
Como es de esperar, puede ser que tenga otros documentos que quiera convertir en
plantilla, o simplemente que no tengamos la plantilla que desea. Para esto
proveemos el servicio de <i>traducción de nuevas plantillas</i>. Nos envía el
documento que quiera convertir en plantilla, y nosotros lo traducimos y lo
añadimos a la plataforma en una colección especial (particular) suya. Mantenemos
el derecho de usar estas plantillas en nuestras colecciones oficiales si vemos
que se pueden aprovechar.

<h2>Traducción Tradicional</h2>

<p>
Hay muchas traducciones que no se pueden hacer con plantillas. Para este tipo de
casos proveemos un servicio de traducción independiente. Este servicio cuesta
distinto dependiendo de si tiene una suscripción o no.

<?php
include("templates/footer.html.php");
?>
