<?php
$title = "Misión";

include("templates/header.html.php");
?>

<h1><?= $title ?></h1>

<p>
Nuestra empresa busca crear una alternativa a las vías convencionales del sector
de la traducción. Sobre todo, busca proveer una forma de acercar nuestras
instituciones a algunas de las personas más marginadas de nuestra sociedad, que
ya sufren de por sí el <i>shock</i> cultural de estar fuera de su país de
origen, normalmente por motivos de necesidad. El servicio de <i>Lingua Ignis</i>
serviría para facilitar la comunicación regular entre éstos dos, y así fomentar
el diálogo y la convivencia. Esperamos que, mediante nuestros servicios, podemos
contribuir a un mundo mejor.

<?php
include("templates/footer.html.php");
?>
