<?php
$title = "El Equipo";

include("templates/header.html.php");
?>

<h1><?= $title ?></h1>

<p>
En nuestro equipo tenemos dos especialistas: la traductora y el informático.
Ester López Barea trabaja de traductora, que traduce todos los documentos. Ella
además se encarga de relaciones públicas. Nicolás Ortega Froysa trabaja de informático, manteniendo nuestra infraestructura y desarrollando nuestra web.

<?php
include("templates/footer.html.php");
?>
