<!DOCTYPE html>
<html lang="es-ES" >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Language" content="es-ES" />
		<meta name="viewport" content="width=device-width" />
		<meta name="keywords" content="languages, translation, templates" />
		<meta name="description" content="El servicio para facilitar la traducción de plantillas." />
		<meta name="author" content="<?= $site_author ?>" />
		<meta name="generator" content="Isidore" />
		<title><?= $title ?> - <?= $site_name ?></title>
		<link rel="stylesheet" href="/style.css" type="text/css" />
		<link rel="icon" href="/favicon.png" type="image/png" />
	</head>
	<body>
		<header>
			<center>
			<a href="/" ><img src="/website-banner.png" alt="Website banner" width="40%" /></a>
			</center>
		</header>
		<nav>
			<a href="/" >Inicio</a>
			<a href="/services.html" >Servicios</a>
			<a href="/mission.html" >Misión</a>
			<a href="/team.html" >El Equipo</a>
		</nav>
		<main>
