\documentclass[12pt,a4paper,titlepage]{article}
\usepackage[spanish]{babel}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage[toc,page]{appendix}
\usepackage{fancyhdr}
\pagestyle{fancy}

\fancyfoot[RO]{\includegraphics[width=0.2\linewidth]{imgs/ceu-cep.png}}
\setlength{\headheight}{15.21004pt}
\setlength{\footskip}{33.02553pt}
\newcommand{\tabitem}{~~\llap{\textbullet}~}

\title{\includegraphics[width=0.75\linewidth]{imgs/lingua-ignis-logo.png}}
\author{Integrante: Nicolás A. Ortega Froysa, \\
	Ester López Barea
	\\
	Profesor: Alejandro Gómez Simón}

\begin{document}

\maketitle
\tableofcontents
\pagebreak

\section{Introducción}

A menudo nuestras organizaciones e instituciones no proveen traducciones
necesarias a otros idiomas de documentos estandarizados, por el simple hecho de
que contratar un traductor les puede incurrir un gasto demasiado alto para algo
tan recurrente. A causa de esto, vemos muchos casos en los que hay personas que
no entienden qué están firmando cuando reciben permisos y notificaciones.
Podemos pensar en los padres inmigrantes, que aún les cuesta la lengua nativa
del país en el que están, cuyos hijos tienen que hacer de intérprete en
cualquier ocasión de interacción entre la escuela y los padres.

{\it Lingua Ignis} es una empresa que busca hacer que este tipo de traducciones
sea más fácil, y a menos coste que contratar un traductor para las traducciones.
Notamos que la mayoría de estos documentos suelen ser del mismo tipo, cambiando
tan sólo algunos datos. Por lo tanto, queremos ofrecer una plataforma que provee
plantillas de documentos en varios idiomas, que al rellenar unos pocos de datos
no-traducibles (e.g.\ nombres propios) y fácilmente formateados (e.g.\ fechas),
el servicio que proveemos generará los documentos de manera instantánea en las
lenguas solicitadas. Esto facilitaría que nuestros clientes puedan ofrecer
notificaciones de una manera más cómoda a sus propios clientes, en la lengua que
ellos entienden mejor.

\section{Objetivos}

Nuestra empresa busca crear una alternativa a las vías convencionales del sector
de la traducción. Sobre todo, busca proveer una forma de acercar nuestras
instituciones a algunas de las personas más marginadas de nuestra sociedad, que
ya sufren de por sí el {\it shock} cultural de estar fuera de su país de origen,
normalmente por motivos de necesidad. El servicio de {\it Lingua Ignis} serviría
para facilitar la comunicación regular entre éstos dos, y así fomentar el
diálogo y la convivencia. Esperamos que, mediante nuestros servicios, podemos
contribuir a un mundo mejor.

La forma en la que vamos a abordar esta tarea también ayudará a cambiar la
dirección del sector para que adopte mejor las nuevas tecnologías y las
optimizaciones que pueden conllevar. Ya este sector ha visto revoluciones con la
traducción asistida por máquinas (i.e.\ {\it Computer Assisted Translation}),
pero también queremos que la tecnología ayude no sólo en el rendimiento, sino
también en el alcance de nuestro trabajo.

\section{Metodología}

\subsection{Nuestros Servicios}

El servicio principal, y que más nos distingue, será la traducción por
plantillas. Proveeremos una selección amplia de colecciones de plantillas
(organizadas por temática) que se pueden traducir a varias lenguas. De este
modo, el cliente sólo pagará por aquellas colecciones que le interesan, y para
traducir sólo a aquellas lenguas que le interesan. Por ejemplo, si un cliente es
una institución educativa, y tiene muchos alumnos cuyos padres sean de Angola,
podría contratar el servicio de la colección educativa para traducirlo sólo a
francés y árabe. El servicio, siendo de suscripción, tendría acceso a él durante
todo un año para generar traducciones cuando vea necesidad.

Conociendo que es posible (e incluso probable) que el cliente quiera otra clase
de plantillas, o una plantilla específica, ofreceremos también otro servicio de
traducción de plantillas nuevas, que luego las incorporaríamos a la plataforma
para que se puedan usar. Dependiendo de la clase de plantilla, reservaríamos el
derecho a usarla (o algún derivado) en nuestras públicas para los demás
clientes, y así mejorar nuestro servicio a la vez.

Finalmente, como servicio más clásico, ofreceremos un servicio de traducción de
documentos. Aunque muchos formularios y notificaciones se pueden traducir en
plantillas que facilitan la generación de nuevos documentos, esto no es siempre
posible. Por lo tanto, este servicio sirve para aquellos casos donde se requiere
de una traducción de un documento completo. El servicio estaría abierto a
clientes que no estén suscritos a nuestra plataforma, pero los que están
suscritos tendrán un descuento por su contribución regular.

\subsection{Definición Jurídica}

Al menos en un principio, pensamos formar nuestra empresa como autónomo. No es
necesario complicar la forma jurídica de la empresa, causando mayor pérdidas en
gestiones burocráticas. Ya si la empresa llega a tener cierto nivel de éxito y
vemos que estamos creciendo, buscaremos la forma de ampliar. Pero hasta entonces
no vemos necesidad de complicar las cosas.

De esta forma, podemos también aprovechar de que, con estado de autónomo, sólo
tendremos que pagar la IRPF, que nos permite aprovechar mejor los beneficios de
nuestro trabajo.

\subsection{Requisitos Materiales y Técnicos}

Como nuestra empresa se trata de un servicio en red, y tampoco precisamos de
ninguna localización donde trabajar, ya que se puede trabajar en remoto y no
precisamos de muchos trabajadores de todos modos. Por esto, no será necesario
ningún local. Se trabajará el 100\% remoto, y así ahorrando costes.

En cuanto a materiales para llevar a cabo nuestro trabajo, sí que tenemos varias
necesidades tecnológicas. En primer lugar, será necesario un sistema de servidor
para proveer este servicio. Será necesario que este servidor tenga capacidad de
computación suficiente para generar los documentos, y espacio de disco también
para almacenarlos (aunque sea de forma temporal), además de las plantillas. Será
necesario montar un servidor de copia de respaldo, por si hay cualquier problema
con el servidor principal, poder restablecer nuestra actividad lo antes posible.
Esto figuraría como un coste inicial de cerca de 880€.

El servicio se proveerá desde la casa de uno de los integrantes, lo cual la
empresa pagará el servicio de internet de esa persona junto con los gastos
eléctricos del servidor -- aunque éstos sean muy pequeños. Además sería
necesario pagar un nombre de dominio, <<{\tt lingua-ignis.com}>>, para que se
pueda acceder fácilmente a nuestro servicio. Esto nos podría llegar a costar
unos 61€ mensuales.

Para los trabajadores, compraremos dos ordenadores -- uno para el técnico y otro
para el traductor -- para poder realizar su trabajo. Aunque puede ser posible
efectuar estas tareas en los ordenadores que ya tengan, lo suyo es tener un
dispositivo aparte para hacer tareas del trabajo, tanto para no distraerse a la
hora de trabajar, como también para tener una discriminación más clara entre el
trabajo y la vida personal. Los dos ordenadores juntos llegarían a costar 1.300€
(650€ cada uno, aproximadamente).

\subsection{Recursos Humanos}

Esta empresa, al menos al empezar, sería un autónomo que consta tan sólo de la
pareja emprendedora. Uno se encargaría de las tareas tecnológicas, y la otra se
encargaría de las traducciones. Entre los dos llevarían los asuntos burocráticos
del negocio.

\subsection{Análisis de Macroentorno}

\subsubsection{Factores Políticos}

Por lo general, no parece haber limitaciones políticas a nuestra empresa. Es
verdad que para ciertos sectores y documentos es posible que fuese necesario
algún tipo de licenciatura (e.g.\ documentos legales). Pero en principio, para
las plantillas más generales sería posible emprender nuestro negocio sin ningún
tipo dificultad política o legal.

\subsubsection{Factores Económicos}

Es verdad que muchas instituciones no contratan traductores para este tipo de
documentos porque no consideran importante este tipo de servicio, de proveer
notificaciones y formularios de permisos en una lengua más comprensible para los
que lo reciben. Por eso nosotros buscamos facilitar esta posibilidad, haciendo
que el servicio sea más económico y accesible.

\subsubsection{Factores Socioculturales}

En España, en muchos sitios, es posible que tengamos dificultades convenciendo a
estas instituciones a que contraten nuestro servicio, ya que muchas veces no se
ve la necesidad. Sería una tarea nuestra demonstrarles lo importante que es
facilitar el acceso lingüístico en sus instituciones a las personas que tienen
más dificultades con nuestra lengua.

\subsubsection{Factores Demográficos}

En nuestro país hemos visto un aumento en inmigración y de niños extranjeros en
nuestro sistema educativo (figura \ref{fig:proporcion-inmigrantes}). Es
razonable pensar que, a causa de esto, veremos también un aumento en la demanda
para traducciones de documentos. Nosotros podríamos aprovechar esta tendencia
para reforzar nuestra posición de lo imprescindible que es poder comunicar bien
con estas personas para convencer a nuestros clientes de su necesidad.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.75\textwidth]{imgs/proporcion-inmigrantes.png}
	\caption{Proporción de alumnos inmigrantes en enseñanzas de régimen general
	no universitarias (fuente: Fundación Europea Sociedad y Educación)}
	\label{fig:proporcion-inmigrantes}
\end{figure}

\subsubsection{Factores Medioambientales y Tecnológicos}

Como nuestro servicio se accedería por la web, sería más accesible en nuestro
país, donde casi todas las instituciones (y nuestros posibles clientes) tienen
acceso a {\it internet}. También, la mayoría de nuestra población parece tener
un alfabetismo tecnológico (digital) básico como para navegar nuestro servicio.
Como todo el poder de procesamiento ocurriría en nuestros servidores, no sería
necesario que tuviesen un ordenador potente. Simplemente con poder visualizar la
página {\it web} podrán acceder y gestionar sus plantillas.

\subsection{Análisis de Microentorno}

\subsubsection{Relación con Clientes}

Al principio de nuestra existencia empresarial será necesario adquirir nuevos
clientes de manera rápida. Se puede hacer uso de nuestra red de contactos en la
comunidad católica, en la que hay muchas personas que forman parte de
instituciones educativas y caritativas que estarían interesados en este tipo de
servicio. También podríamos hacer uso de la publicidad por {\it internet} para
dar a conocer a nuestra empresa en un ámbito más amplio.

Una vez que nuestros clientes hayan registrado su cuenta, la relación con los
clientes será de forma automática por medio de nuestra interfaz web, donde
podrán renovar sus suscripciones, anularlas, pedir traducciones, etc.

\subsubsection{Relación con Competidores}

Proveemos un servicio dentro de un mercado ya existente con otros competidores.
Y aunque nuestra solución sea nueva, nos veremos en real competencia con ellos.
Podemos prever lo siguiente en cuanto a una comparación de nuestra solución con
la tradicional de traducir cada documento:

\begin{table}[ht!]
	\centering
	\begin{tabular}{|p{6cm}|p{6cm}|}
		\hline
		{\bf Debilidades} & {\bf Amenazas} \\ \hline \hline
		\tabitem No tenemos (aún) muchas lenguas & \tabitem Menos lenguas
		soportadas \\
		\tabitem Infraestructura completa propia & \tabitem Más flexibilidad en
		traducción \\
		\hline
		{\bf Fortalezas} & {\bf Oportunidades} \\ \hline \hline
		\tabitem Más control de nuestras infraestructuras & \tabitem Añadir más
		lenguas a nuestro repertorio \\
		\tabitem Capaz de producir más cantidad & \tabitem Proveer también
		servicios tradicionales \\
		\tabitem Automatización de traducciones & \\
		\hline
	\end{tabular}
	\caption{Análisis DAFO de {\it Lingua Ignis}.}
	\label{tbl:dafo}
\end{table}

\subsubsection{Relación con Proveedores}

Por lo general no vamos a tener proveedores, salvo el proveedor de {\it
internet} y el nombre de dominio. Los demás materiales sólo será necesario
comprarlos al empezar nuestra empresa. Mas en el caso de que quisiésemos mudar
nuestra infraestructura a un servicio externo (y así no tener que mantener el
{\it hardware} nosotros mismos) no sería necesario tampoco mantener una relación
con ellos más allá de la interacción por su interfaz {\it web} para gestionar
nuestro uso de sus servicios.

\subsection{Estrategia Comercial}

Nuestro logotipo es una lengua de fuego, como indica el nombre de nuestra
organización (figura \ref{fig:official-logo}). Esto es en referencia al evento
de Pentecostés, cuando el Espíritu Santo descendió sobre la Virgen María y los
Apóstoles, y <<se les aparecieron unas lenguas como de fuego que se repartieron
y se posaron sobre cada uno de ellos>>, que les inducía a <<hablar en diversas
lenguas, según el Espíritu les concedía expresarse>> de tal modo que <<cada uno
les oía hablar en su propia lengua>> (Hch.\ 2,3-6). Por lo tanto, en honor a
esta inspiración, el eslogan de nuestra empresa será <<{\it Quoniam audiebat
unusquisque lingua sua}>>, (<<Porque cada uno les oía hablar en su propia
lengua>>).

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.3\textwidth]{imgs/lingua-ignis-logo.png}
	\caption{Logotipo oficial de {\it Lingua Ignis}.}
	\label{fig:official-logo}
\end{figure}

Como se ve en el logotipo, adoptaremos los colores naranja y blanco. Naranja que
representa la lengua de fuego que apareció en Pentecostés, y blanco que
representa al Espíritu Santo, a quien pedimos su auxilio para dar fruto a
nuestra labor.

Aunque no tendremos un lugar físico donde interactuar con nuestros clientes, sí
que tendremos un lugar virtual. Aquí queremos comunicar nuestra misión
caritativa y universal. Para eso, podremos usar imágenes que muestran a personas
de varios países que se podrían beneficiar de un servicio como el nuestro.
Habría que usar también colores más cálidas para transmitir calor, intimidad; un
sentimiento de hogar.

\subsection{Análisis de Viabilidad}

\subsubsection{Presupuesto a Corto-Medio Plazo}

\begin{align}
	2.180 \times 1,1 + 61 \times 12 = 3.203,2\\
	2.180 \times 0,1 + 61 \times 12 = 950
\end{align}

Considerando los costes iniciales y mensuales de la empresa (apéndice
\ref{appendix:costes}) podemos calcular que tan sólo para el primer año nos
haría falta una inversión de 2.912€, y asumiendo la necesidad de un 10\% más por
cualquier imprevisto, en total nos llega la cantidad precisada a 3.203,20€. Mas
a partir del primer año ya los costes anuales bajarían a ser de unos 61€
mensuales, añadiendo un dinero de reserva por cualquier imprevisto (e.g.\ algún
ordenador se estropea) que debería de ser de un 10\% de los costes fijos, que
nos llevaría a tener un presupuesto anual (después del primer año) de 950€.

Nuestros ingresos dependerán de la cantidad de clientes que podamos obtener, el
precio de mercado de traducciones -- un precio variable que no se puede
publicar, y que puede cambiar a lo largo del año --, y el precio de
suscripciones que establecemos -- que dependerá del sector y la lengua objetiva.
Dicho lo cual, hay muchas instituciones con misiones caritativas que estarían
abiertos a usar este tipo de solución, y así ahorrar dinero en traducciones, y
sobre todo traductores asalariados. Tan sólo en España existen más de 7.000 ONGs
que podrían estar interesados en nuestro servicio.\footnotemark Incluso tan sólo
con suscripciones de unos 150€/año, y un 5\% de éstos como clientes (sin contar
con empresas que podrían estar interesados) tenemos un ingreso posible de
52.500€/año sólo en suscripciones (sin contar los demás servicios). Tardaríamos
algo en adquirir nuestro clientes, luego es posible que no tengamos ingresos
tales durante el primer año, pero luego ya tendremos más posibilidades de
expandir.

\footnotetext{\url{https://www.guiaongs.org/directorio/}}

En total, el beneficio neto (antes de impuestos) del primer año podría ser de
más de 45.000€.

\subsubsection{Ratios de Viabilidad}

\begin{align}
	RL = 52.500 \div 61 = 860,66 \\
	RE = 2.241 \div 54.680 = 0,04 \\
	ROA = 45.000 \div 54.680 = 0,82
\end{align}

Haciendo los cálculos anteriores, podemos analizar la viabilidad de nuestra
empresa en función de su ratio de liquidez. Como la mayoría de nuestros gastos
son de carácter no corriente (i.e.\ son fijos) vemos que tenemos un ratio de
liquidez de 860,66, muy superior al óptimo. Esto se podría amortiguar con más
gastos.

En cuestión del ratio de endeudamiento, sería a un nivel de 0,04. Sería muy por
debajo de lo recomendado, dicho lo cual podríamos revisar nuestro plan e incluir
más gastos que podrían ayudar a mejorar la productividad de nuestra empresa con
más inversiones.

Dicho lo cual, vemos una rentabilidad económica bastante más alta de lo
necesario, a 0,82.

\subsubsection{Financiación}

Por lo general, este proyecto no tiene muchos gastos. Incluso para el primer
año, que es el de mayor gasto con todo el equipaje, los servidores, los
materiales, etc.\ vemos que no hay necesidad de muchos gastos. Por esto, todos
los gastos lo podemos subvencionar nosotros con nuestros fondos personales. Aún
en el caso de que fuera necesario conseguir más fondos, tenemos contactos
(familiares) que estarían dispuestos a invertir una pequeña cantidad de dinero
en esta empresa, viendo posibilidad de recuperar sus inversiones.

\section{Resultados}

Como se ve en la tabla DAFO mostrado anteriormente (tabla \ref{tbl:dafo}) las
amenazas claras que tenemos son principalmente aquellas empresas que hacen
traducciones tradicionales. Éstas suelen tener más flexibilidad y, al tener más
inversión inicial, pueden tener acceso a más empleados que pueden traducir a más
lenguas. En nuestro caso, como queremos empezar pequeño primero, no tenemos
aquel lujo. Por el otro lado, nuestro producto facilitará mucho la traducción de
documentos triviales y recurrentes (salvo algunos datos), y además proveerá una
opción más barata en comparación con los competidores.

A la vez, en la empresa tenemos mucho lugar para avanzar y expandir nuestro
lugar en el mercado. Tenemos también más control sobre nuestra infraestructura,
lo cual ayuda a que lo podamos manejar y configurar a nuestras necesidades.

\section{Conclusión}

Por lo general es una idea de empresa bastante buena con mucha potencial. Viendo
la potencial que tiene, quizá lo suyo sea montar una empresa en forma de
sociedad y entonces buscar la inversión de terceros, y así poder explotar más
posibilidades. Pero como los afiliados no creen que merece la pena tanta
burocracia y negociación, tendrá que esperar.

\pagebreak

\begin{appendices}

\section{Costes}
\label{appendix:costes}

\begin{table}[ht!]
	\centering
	\begin{tabular}{|l|r|}
		\hline
		\multicolumn{2}{|c|}{\bf Costes Fijos} \\ \hline \hline
		{\bf Recurso} & {\bf Coste} \\ \hline
		Servidor (SolidRun ClearFog CN9130) & 400€ \\
		Raspberry Pi & 80€ \\
		NVMe 2TB & 200€ \\
		Disco externo USB 4TB & 150€ \\
		Cables & 50€ \\
		Thinkpad E14 (x2) & 1.300€ \\ \hline
		{\bf Total} & 2.180€ \\ \hline
		\hline
		\multicolumn{2}{|c|}{\bf Costes Variables (mensuales)} \\ \hline \hline
		{\bf Recurso} & {\bf Coste} \\ \hline
		Internet & 60€ \\
		Dominio {\tt lingua-ignis.com} & 1€ \\
		Uso eléctrico & --\footnotemark \\ \hline
		{\bf Total} & 61€ \\ \hline
	\end{tabular}
	\label{tbl:costes}
	\caption{Detalle de costes fijos y variables.}
\end{table}

\footnotetext{El uso eléctrico de los servidores, al menos al principio, tan
minúsculo que no merece consideración en esta tabla.}

\end{appendices}

\end{document}
