\babel@toc {spanish}{}\relax 
\contentsline {section}{\numberline {1}Introducción}{2}{section.1}%
\contentsline {section}{\numberline {2}Objetivos}{2}{section.2}%
\contentsline {section}{\numberline {3}Metodología}{3}{section.3}%
\contentsline {subsection}{\numberline {3.1}Nuestros Servicios}{3}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Definición Jurídica}{3}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Requisitos Materiales y Técnicos}{4}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Recursos Humanos}{4}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Análisis de Macroentorno}{5}{subsection.3.5}%
\contentsline {subsubsection}{\numberline {3.5.1}Factores Políticos}{5}{subsubsection.3.5.1}%
\contentsline {subsubsection}{\numberline {3.5.2}Factores Económicos}{5}{subsubsection.3.5.2}%
\contentsline {subsubsection}{\numberline {3.5.3}Factores Socioculturales}{5}{subsubsection.3.5.3}%
\contentsline {subsubsection}{\numberline {3.5.4}Factores Demográficos}{5}{subsubsection.3.5.4}%
\contentsline {subsubsection}{\numberline {3.5.5}Factores Medioambientales y Tecnológicos}{5}{subsubsection.3.5.5}%
\contentsline {subsection}{\numberline {3.6}Análisis de Microentorno}{6}{subsection.3.6}%
\contentsline {subsubsection}{\numberline {3.6.1}Relación con Clientes}{6}{subsubsection.3.6.1}%
\contentsline {subsubsection}{\numberline {3.6.2}Relación con Competidores}{6}{subsubsection.3.6.2}%
\contentsline {subsubsection}{\numberline {3.6.3}Relación con Proveedores}{7}{subsubsection.3.6.3}%
\contentsline {subsection}{\numberline {3.7}Estrategia Comercial}{7}{subsection.3.7}%
\contentsline {subsection}{\numberline {3.8}Análisis de Viabilidad}{8}{subsection.3.8}%
\contentsline {subsubsection}{\numberline {3.8.1}Presupuesto a Corto-Medio Plazo}{8}{subsubsection.3.8.1}%
\contentsline {subsubsection}{\numberline {3.8.2}Ratios de Viabilidad}{9}{subsubsection.3.8.2}%
\contentsline {subsubsection}{\numberline {3.8.3}Financiación}{9}{subsubsection.3.8.3}%
\contentsline {section}{\numberline {4}Resultados}{10}{section.4}%
\contentsline {section}{\numberline {5}Conclusión}{10}{section.5}%
\contentsline {section}{Appendices}{11}{section*.5}%
\contentsline {section}{\numberline {A}Costes}{11}{appendix.1.Alph1}%
