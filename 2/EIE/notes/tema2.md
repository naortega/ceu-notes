# Tema 2

## Investigación Comercial

- ¿Cómo es el entorno?
  - Social: género y edad
  - Económico: el dinero de los clientes
  - Legislativo y político: legalidad, subvenciones, y amigabilidad política
  - Tecnológico: desarrollo tecnológico de la zona
- ¿Qué Ocurre en el mercado?
  - ¿En qué situación se encuentra nuestro sector?
  - ¿Cómo es la competencia?
  - ¿Qué hacen los competidores?
