Nicolás Burger

- Nicolás ha vendido hamburguesas por valor de 400€, cobrandolas al contado

# ACTIVO                                                    108.100€

## Activo no corriente                                       82.500€

### Inmovilizado Intangible                                   4.000€
- Licencia vitalicia: 4.000€

### Inmovilizado Material                                    78.500€
- Local: 60.000€
- Maquinaria: 15.000€
- Dos ordenadores: 1.000€
- Mac: 2.500€

### Inversiones inmobiliarias

### Inversiones en empresas del grupo y asociadas l/p

### Inversiones Financieras l/p

### Deudores comerciales l/p

## Activo Corriente                                          25.600€

### Existencias                                               1.700€
- Pan, hamburguesas e ingredientes: 2.100€ (400€)

### Deudores comerciales y otras cuentas a cobrar

### Inversiones en empresas del grupo y asociadas c/p

### Inversiones Financieras c/p                              20.000€
- Inversión Apple (no grupo): 20.000€

### Efectivo                                                  3.900€
- Banco: 3.500€
- Caja: 400€

# PASIVO + PATRIMONIO NETO                                  107.700€

## Patrimonio Neto                                           84.600€

### Capital                                                  79.600€
79.200€

### Prima de emisión

### Reservas                                                  5.000€
- Reserva voluntaria: 5.000€

### Resultado de ejercicios anteriores

## Pasivo No Corriente                                       21.000€

### Provisiones l/p

### Deudas l/p                                               21.000€
- Deuda por local (15m): 21.000€

### Deudas con empresas del grupo l/p

## Pasivo Corriente                                           2.500€

### Provisiones c/p

### Deudas a c/p                                              2.500€
- Mac: 2.500€

### Deudas c/p con empresas del grupo
