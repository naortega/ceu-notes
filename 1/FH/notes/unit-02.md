# Unit 2

## Cáculos Con Memoria

### Cálculo de canal

DDRX-YYYY where X is the type of DDR being used, and YYYY the frequency. It can
also be expressed as PC(X)-ZZZZ where ZZZZ is the máximum theoretical
transfer rate (MTTR) of the memory. To calculate the ZZZZ value from YYYY
you must do `YYYY * 64 / 8 = ZZZZ MB/s`. To calculate the external
frequency you must divide the YYYY value by 2, for the two bits, and for
internal you must divide the YYYY value by the prefetch value, which is:

- DDR: 2
- DDR2: 4
- DDR3: 8

### Cálculo de Latencia

Para calcular el tiempo ocioso de un CPU cuando accede a memoria, se
calcula usando la frecuencia externa. Con esto se ha de convertir de
frecuencia en (nano) segundos, de ahí se ha de multiplicar por el número de
ciclos de latencia (CL), y finalmente multiplicar este número por la frecuencia
de reloj de la CPU.

E.g.

```calc
CL=5; CPU Freq=2.2GHz; DDR-800
800/2 = 400 MHz
1/400 = 2.5 ns
2.5 * 5 = 12.5 ns
12.5 * 2.2 GHz = 28 cycles
```
