# Black Friday Computer Setup
Ya que puedo tomar un poco de libertad, voy a montar un ordenador de
arquitectura ARM.

## Raspberry Pi
85€
Raspberry Pi 4B (8GB Model)
[link](https://www.kubii.es/raspberry-pi-3-2-b/2955-raspberry-pi-4-modele-b-8gb-3272496301535.html?src=raspberrypi)

## Tarjeta SD de 128GB con transferencia de 250MB/s
35.08€
Carte Lexar Professional 1667x 128 Go SDXC UHS-II (tarjeta SD)
[link](https://www.amazon.es/Carte-Lexar-Professional-1667x-UHS-II/dp/B07SZ3GCC6/ref=sr_1_8?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=128GB%2BSD%2Bcard&qid=1637931526&qsid=259-0750456-8975462&sr=8-8&sres=B07H9DVLBB%2CB07FCMKK5X%2CB08GYKNCCP%2CB07KXQX3S3%2CB08TJRVWV1%2CB085FGMQS6%2CB07SZ3GCC6%2CB07G3H5RBT%2CB073JYC4XM%2CB08GY99LX8%2CB08PCFQCRG%2CB012PL8BNO%2CB0858MZ6J6%2CB07H48412Q%2CB08GY61ZZN%2CB07GBD3JLR%2CB07YGZ7JD5%2CB083526Z17%2CB07C963SYS%2CB07T9JV7K7&srpt=FLASH_MEMORY&th=1)

**Notas:** Necesitamos mucho espacio ya que en los sistemas U-Boot generalmente el sistema principal se pone en la tarjeta SD.

## Teclado Mecánico
98.62€
Velocifire Blade VM01 Mecánico Teclado con Marrón Interruptores Teclado para Juegos LED Iluminados con Conexión de Dable del Juego Teclado 【Negro】
[link](https://www.amazon.es/Velocifire-Mec%C3%A1nico-Interruptores-Iluminados-Conexi%C3%B3n/dp/B01M0QEYR4/ref=sr_1_2?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=velocifire+keyboard&qid=1637931868&qsid=259-0750456-8975462&sr=8-2&sres=B07RWCD8CJ%2CB01M0QEYR4%2CB07MQD7WNB%2CB00564NI9U%2CB01L6L44NU%2CB00XYTTEAQ%2CB0056C7W1C%2CB08P5WWFBW%2CB086PKSF7F%2CB00YDSSB6A%2CB013SL1ZBK%2CB07RHY7SG2%2CB00779631W%2CB07YT168QJ%2CB0832FHGKD%2CB08641KV2Q%2CB016UPAXBE%2CB01NCF75BC%2CB0856XC7NN%2CB07SDMPZ4V&srpt=KEYBOARDS)

## Ratón Inalámbrico
24.99€
Logitech M705 Marathon Ratón Inalámbrico, Embalaje de Business, 2,4 GHz con Mini Receptor USB, Seguimiento Óptico 1000 DPI, 7 Botones, Batería 3 Años, PC/Mac/Portátil/Chromebook - Gris
[link](https://www.amazon.es/Logitech-Marathon-Inal%C3%A1mbrico-Embalaje-Seguimiento/dp/B07W4DHT47/ref=sr_1_2_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=logitech%2Bmouse&qid=1637932092&sr=8-2-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFUR1kxSUZFRU5VWTMmZW5jcnlwdGVkSWQ9QTA1ODI2NjMzNlNFSllYOUpXQUFDJmVuY3J5cHRlZEFkSWQ9QTA2MjUyNDBVTllSTzZPUTlTNU8md2lkZ2V0TmFtZT1zcF9hdGYmYWN0aW9uPWNsaWNrUmVkaXJlY3QmZG9Ob3RMb2dDbGljaz10cnVl&th=1)

## Disco Duro Externo
42.21€
Toshiba Canvio Basics - Disco duro externo portátil USB 3.0 de 2.5 pulgadas (1 TB) color negro
[link](https://www.amazon.es/Toshiba-Canvio-Basics-port%C3%A1til-Pulgadas/dp/B07997KKSK/ref=sr_1_6?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=1TB%2Busb%2B3.0&qid=1637932232&qsid=259-0750456-8975462&sr=8-6&sres=B06VVS7S94%2CB081VGBGP5%2CB0058VIWTM%2CB07997KKSK%2CB07BCP5L2B%2CB075KPPTTW%2CB015CH1PJU%2CB07SYRW97F%2CB08GR49Z4B%2CB00CMKWL3A%2CB08KHVTS1M%2CB00YFI1A66%2CB087DF1L2J%2CB07855LJ99%2CB07PPNSFBK%2CB08HJ9ZSMV%2CB084XPFQKN%2CB07DQ5ZH1D%2CB09HCT1733%2CB00KWHJY7Q&srpt=FLASH_DRIVE&th=1)

**Notas:** Al usar una tarjeta SD para nuestro sistema, sería buena idea usar un disco externo para guardar nuestros archivos personales con más capacidad de almacenamiento.

## Monitor
99€
HUAWEI Eye Comfort AD80 - Monitor de 23,8" FullHD (1920x1080, IPS anti reflejos, 16:9, 5ms, 250 nits, 60Hz, HDMI, VGA, marcos estrechos), Negro
[link](https://www.amazon.es/HUAWEI-Eye-Comfort-AD80-1920x1080/dp/B08YF9DR2J/ref=sr_1_5?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=HD+monitor&qid=1637932358&qsid=259-0750456-8975462&sr=8-5&sres=B08YF9DR2J%2CB08KJB7JKV%2CB08HSJNN5N%2CB08N131X79%2CB094CRCF6W%2CB08DP58YCT%2CB08FRJ78PR%2CB084BJ9SD6%2CB0957LFHTM%2CB08WM38TNH%2CB07PN63WQ8%2CB08BHW7GBL%2CB08N6FRYW5%2CB083X18CW4%2CB08D8R4VLX%2CB08DYB1QF6%2CB08CKNH2ZM%2CB0845NMZ6K%2CB07W9LRB2J%2CB06XDY3SJF&srpt=MONITOR)

## Cable HDMI
10.99€
LinkinPerk - Cable micro-HDMI a HDMI, alta velocidad, Compatible con Ethernet, 3D, 4K y retorno de audio
[link](https://www.amazon.es/LinkinPerk-Velocidad-Compatible-Microsoft-Port%C3%A1tiles/dp/B072FPT4RW/ref=sr_1_4_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=cable+micro+HDMI&qid=1637932561&sr=8-4-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzQzlSSk5WSTJCVlJFJmVuY3J5cHRlZElkPUEwMjEzNjIyM0FTMEQ5SkFBT0FTViZlbmNyeXB0ZWRBZElkPUEwMzMxMDk4MkhMM1E3M1dHTTJNOCZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU%3D)

## Cargador USB-C
26.55€
NEUE DAWN 65W Cargador Portátil USB C para Lenovo Yoga 920 910 720 730 ThinkPad X1 E15 E14 Flex 5, DELL XPS, HP, Acer,ASUS Chromebook, Huawei Matebook, Xiaomi Air Tipo Type C PD Adaptador de Corriente
[link](https://www.amazon.es/NEUE-DAWN-Chromebook-Adaptador-Corriente/dp/B08XB87999/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=USB+C+power+supply&qid=1637932662&qsid=259-0750456-8975462&sr=8-1&sres=B08XB87999%2CB08LR5JBK7%2CB07QRQTKCZ%2CB01M3NMEZO%2CB07WKD71DX%2CB07QL6GF92%2CB094QQVL41%2CB09B3P84LH%2CB09534T734%2CB08BFHYTWZ%2CB09DNY3GCB%2CB08DKXJS5F%2CB077H2CQDF%2CB092MWBBSS%2CB096LH6VCM%2CB088CX4BX3%2CB085HTV69S%2CB07CSS2MPC%2CB096LHWCJ4%2CB07RLL59DW&srpt=CHARGING_ADAPTER)

## Altavoces
19.99€
Amazon Basics – Altavoces para ordenador de sobremesa o portátil, con alimentación por CA, versión para la UE
[link](https://www.amazon.es/AmazonBasics-Computer-Speakers-Desktop-AC-Powered/dp/B07D924NF5/ref=sr_1_9?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=altavoces+jack&qid=1637932778&qsid=259-0750456-8975462&sr=8-9&sres=B00KDA4HYW%2CB08VYD4SJH%2CB07GPWY8S6%2CB01LZWX5X0%2CB07D924NF5%2CB08R727HCP%2CB00II0QHX8%2CB07G4SVXTC%2CB08P32QNX7%2CB078T13XS2%2CB07Z47T6Q7%2CB076B92Y5L%2CB0791H74NT%2CB00544XKK4%2CB00YR92T90%2CB000S8FNTM%2CB0995N4QT8%2CB005ZD6TB6%2CB08L4XPXLR%2CB08D79RFLB&srpt=SPEAKERS)

## Cable Ethernet
6.79€
UGREEN Cable de Red Cat 7, Cable Ethernet Network LAN 10000Mbit/s con Conector RJ45 (10 Gigabit, 600MHz, Cable FTP) para PS5, Xbox X/S, PC, Compatible con Cat 6, Cat 5e, Cat 5, Cable Plano(1 Metro)
[link](https://www.amazon.es/UGREEN-Ethernet-10000Mbit-conector-Compatible/dp/B00QV1F1C4/ref=sr_1_4_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=ethernet+cable&qid=1637932850&sr=8-4-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUExTzZJTk1EQU9VVzZCJmVuY3J5cHRlZElkPUEwMjc3NTk1MUczRElVSFo1TFEzQiZlbmNyeXB0ZWRBZElkPUEwOTY1NDU1MVMwWjVDVU5KOVc0RCZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU%3D)

## USB Hub
12.99€
atolla Hub USB 3.0, 4 Puertos USB Hub de Datos de 5Gbps, Multi USB Hub con Diseno de Cable Plegable para PC, Portátiles, admite Windows 10, 8, 7, Vista, XP, Mac OS, Linux
[link](https://www.amazon.es/atolla-Puertos-Plegable-Port%C3%A1tiles-Windows/dp/B08RCYDWFT/ref=sr_1_3_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=USB+hub&qid=1637932918&sr=8-3-spons&psc=1&smid=A3ASPSSXGPYE5&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUExWDRSRFZaREpNTVYwJmVuY3J5cHRlZElkPUEwMTUzMjA5MTlLQURSSFhUTkozWSZlbmNyeXB0ZWRBZElkPUEwNzUzMjA3SUowNjNUVlk0TU5IJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ%3D%3D)

## Caja
13.99€
Case para Raspberry Pi 4, Raspberry Pi 4 Caja de aluminio, refrigeración pasiva con placa de disipación térmica, carcasa compatible con RPi4 Modelo B 8 GB/4 GB/2 GB
[link](https://www.amazon.es/Raspberry-aluminio-refrigeraci%C3%B3n-disipaci%C3%B3n-compatible/dp/B08SVX2XW4/ref=sr_1_4_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=Raspberry+pi+4+case&qid=1637932997&sr=8-4-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUExMUNOMEw0NDAwVkhOJmVuY3J5cHRlZElkPUEwNjUzNjUzM0JENVkyOVVQS05IUiZlbmNyeXB0ZWRBZElkPUEwMjkyMTU1SlkyWFMwWE1HOE1ZJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ%3D%3D)

## Total
85 + 35.08 + 98.62 + 24.99 + 42.21 + 99 + 10.99 + 26.55 + 19.99 + 6.79 + 12.99 + 13.99 = 476.2€
