Calcula en Mb, MB y MiB lo que ocupan las siguientes imágenes: (a)
800x600 escala de grises de 8 bits; (b) 800x600 color indexado con 16
colores; (c) 1024x768 CMYK

a)
800 * 600 * 8 = 3.84 Mb
3840000 / 8 = 480000 B = 0.48 MB
480000 / 2^20 = 0.458 MiB

b)
800 * 600 * 4 = 1920000 b = 1.92 Mb
1920000 b / 8 = 240000 B = 0.24 MB
240000 B / 2^20 = 0.229 MiB

c)
1024 * 768 * 32 = 25165824 b = 25.165 Mb
25165824 b / 8 = 3145728 B = 3.146 MB
3145728 B / 2^20 = 3 MiB
