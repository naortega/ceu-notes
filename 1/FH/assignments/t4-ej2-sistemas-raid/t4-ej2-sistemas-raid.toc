\babel@toc {spanish}{}\relax 
\contentsline {section}{\numberline {1}Introducción: ¿Qué es RAID?}{3}{section.1}%
\contentsline {section}{\numberline {2}Niveles RAID}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}RAID 0}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}RAID 1}{4}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}RAID 2}{4}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}RAID 3}{5}{subsection.2.4}%
\contentsline {subsection}{\numberline {2.5}RAID 4}{5}{subsection.2.5}%
\contentsline {subsection}{\numberline {2.6}RAID 5}{6}{subsection.2.6}%
\contentsline {subsection}{\numberline {2.7}RAID 6}{7}{subsection.2.7}%
\contentsline {subsection}{\numberline {2.8}RAID 5E y 6E}{7}{subsection.2.8}%
\contentsline {section}{\numberline {3}RAID Híbrido (Nested)}{7}{section.3}%
\contentsline {subsection}{\numberline {3.1}RAID 01}{8}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}RAID 10}{8}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}RAID 30}{9}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}RAID 100}{9}{subsection.3.4}%
\contentsline {section}{\numberline {4}Derechos de Autor}{10}{section.4}%
