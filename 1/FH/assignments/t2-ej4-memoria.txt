1) DDR3-800 ->
800 MHz * 64 bits / 8 = 6400 MB/s => PC(3)-6400
Externo: 800/2 = 400 MHz
Interno: 800/8 = 100 MHz

2) DDR2-400 ->
400 MHz * 64 bits / 8 = 3200 MB/s => PC(2)-3200
Externo: 400 / 2 = 200 MHz
Interno: 400 / 4 = 100 MHz

3) PC(1)-1600 ->
1600 * 8 / 64 = 6400 MHz => DDR-200
Externo: 200 / 2 = 100 MHz
Interno: 200 / 2 = 100 MHz
