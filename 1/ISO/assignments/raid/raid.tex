\documentclass[12pt,a4paper]{article}
\usepackage[spanish]{babel}
\usepackage{graphicx}

\title{Ejercicio II: Almacenamiento RAID}
\author{Nicolás A. Ortega Froysa}

\begin{document}
\maketitle

\section{Introducción: ¿Qué es RAID?}
En la informática, aumentar las capacidades de una unidad suele ser más difícil
que simplemente usar varias unidades. Por ejemplo, es más fácil que un ordenador
use varios procesadores, que no construir un procesador más rápido. Esto es lo
que lleva a que en el almacenamiento de información a largo plazo (i.e.\ en
discos duros), para almacenar mucha información se suelen tener muchos discos.
La dificultad con este modelo está en que se complica la organización de datos,
ya que cada unidad se trata de forma separada. En esto, se han inventado varios
sistemas lógicos de almacenamiento para gestionar el espacio en varios discos,
como LVM. Pero la más conocida para almacenamiento masivo es {\bf Redundant
Array of Independent Disks}, o RAID.

El objetivo del almacenamiento con RAID es por seguridad, rendimiento, y
capacidad de almacenamiento.

\begin{itemize}
	\item {\bf Seguridad}: se pueden hacer escrituras paralelas a dos discos de
		la misma información para poder tener copia de seguridad.
	\item {\bf Rendimiento}: al distribuir información entre varios discos se
		puede paralelizar la transferencia de datos.
	\item {\bf Capacidad de almacenamiento}: permite tratar a varios discos como
		si fueran un solo disco.
\end{itemize}

Aunque éstos son algunas de las ventajas de usar RAID, realmente depende del
tipo de {\em nivel} que tenga.

\section{Niveles RAID}
Los sistemas configurados con RAID no funcionan todos igual, sino que se pueden
configurar de forma distinta, usando distintos niveles, e incluso combinando
éstos dependiendo de las necesidades. Dependiendo del nivel que sea, se explota
una o más de las ventajas expuestas anteriormente.

\subsection{RAID 0}
\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.5\linewidth]{imgs/striping.png}
		\caption{Striping con tres discos.}
		\label{fig:striping}
	\end{center}
\end{figure}

Para poder aprovechar tanto la mejora en rendimiento de tener varios discos,
como su aumentada capacidad de almacenamiento, este nivel implementa una técnica
que se denomina {\em striping}, tal que los segmentos consecutivos de un archivo
se distribuyen entre los discos configurados (ver figura \ref{fig:striping}).
Al usar varios discos, esto significa que la capacidad máxima de esta
configuración RAID sería la suma del espacio de almacenamiento de todos sus
discos. También mejora la transferencia de datos, al poder paralelizar su
transferencia entre varios discos.

\subsection{RAID 1}
Para poder asegurarse de la seguridad de los datos guardados, este nivel
automáticamente copia la información a los demás discos haciendo {\em
mirroring}. Lo que hace es que mientras se escribe a un disco, se va escribiendo
los mismos datos en el disco de copia. Esto no causa demasiado retraso, ya que
las transmisiones son a dos discos distintos, lo cual se puede hacer en
paralelo.

\subsection{RAID 2}
\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.5\linewidth]{imgs/raid2.png}
		\caption{Diagram of RAID 2 setup.}
		\label{fig:raid2}
	\end{center}
\end{figure}

Al igual que el RAID 0, hace {\em striping}, mas en vez de hacerlo a nivel de
bloques, lo hace a nivel de bits. También se suma un método de corrección de
errores con un {\em hamming code}. Mas debida a su complejidad comparada con un
sistema de paridad, apenas se usa este nivel en la práctica. Esto hace que sea
el único nivel original de RAID que no se usa actualmente. También es imposible
de hacer varias peticiones a la vez por la distribución a nivel de bit en vez de
ser por bloque, lo cual significa generalmente que para leer tan sólo un bloque
ya debe de usar todos los discos.

\subsection{RAID 3}
\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.5\linewidth]{imgs/raid3.png}
		\caption{Diagram of RAID 3 setup.}
		\label{fig:raid3}
	\end{center}
\end{figure}

Muy similar al nivel anterior, RAID 3 implemente {\em striping} a nivel de
bytes, y un disco dedicado a guardar la paridad para la detección de errores. El
hecho de que su {\em striping} sea a nivel de byte significa que, al igual que
en el nivel 2, no se pueden hacer varias peticiones a la vez. Esto significa que
puede ser muy útil si se precisa una alta velocidad de transferencia de datos
consecutivos de un mismo bloque. Mas este nivel ya casi no se usa en la
práctica.

\subsection{RAID 4}
\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.5\linewidth]{imgs/raid4.png}
		\caption{Diagram of RAID 4 setup.}
		\label{fig:raid4}
	\end{center}
\end{figure}

Al igual que RAID 3, este nivel implementa {\em striping} y un disco dedicado a
la paridad, pero a este nivel el {\em striping} sí que es a nivel de bloque, lo
cual permite un acceso a datos más veloz, aunque la escritura pueda ser más
lenta debido a la escritura al disco de paridad. Su ventaja es que puede ser
fácilmente extendida siempre y cuando los nuevos discos estén completamente
limpios (sólo bytes de ceros).

\subsection{RAID 5}
\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.5\linewidth]{imgs/raid5.png}
		\caption{Diagram of RAID 5 setup.}
		\label{fig:raid5}
	\end{center}
\end{figure}

Funciona de manera igual que RAID 4, mas distribuye la información de paridad
entre todos los discos, lo cual para funcionar tan sólo hace falta que funcionen
todos los discos menos uno. Al faltar un solo disco, se pueden calcular los
errores a partir de la información de paridad. Dicho lo cual, este nivel precisa
de un mínimo de tres discos. Esto también mejora la velocidad de escritura
respecto al RAID 4, ya que la información de paridad se encuentra también
distribuida.

\subsection{RAID 6}
\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.5\linewidth]{imgs/raid6.png}
		\caption{Diagram of RAID 6 setup.}
		\label{fig:raid6}
	\end{center}
\end{figure}

Finalmente existe el nivel RAID 6. Se parece al RAID 5, en que también usa {\em
striping} a nivel de bloques, y también usa paridad, pero en vez de ser sólo un
bloque de paridad, son dos. Esto hace que haya más penalización al escribir
debido a las paridades, pero aumenta la seguridad de los datos, ya que pueden
fallar hasta dos discos.

\section{RAID Híbrido (Nested)}
Además de los niveles anteriores, también se pueden hacer combinaciones, que
normalmente se denominan por {\em RAID X+Y}. Lo que significa esto es que a
nivel más bajo está usando el nivel X, y a nivel más alto usa Y. De los más
comunes, podríamos decir que existen:

\begin{itemize}
	\item RAID 0+1 (01)
	\item RAID 0+3 (03)
	\item RAID 1+0 (10)
	\item RAID 5+0 (50)
	\item RAID 6+0 (60)
	\item RAID 10+0 (100)
\end{itemize}

\section{Derechos de Autor}
Copyright \textcopyright\ 2021 Ortega Froysa, Nicolás <nicolas@ortegas.org> \\
Este documento está licenciado con la licencia Creative Commons Attribution No
Derivatives 4.0 International.\\
Creado con \LaTeX.

\end{document}
