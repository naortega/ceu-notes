\documentclass[12pt,a4paper]{article}
\usepackage[spanish]{babel}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}

\title{Tema IX Ejercicio IV: Mantis Bug Tracker}
\author{Nicolás A. Ortega Froysa}

\begin{document}
\maketitle
\pagebreak
\tableofcontents
\pagebreak

\section{Introducción}

Muchas veces cuando gestionamos cualquier tipo de proyecto, es necesario tener
alguna herramienta que nos ayude a manejar las tareas (o incidencias) que vayan
surgiendo al respecto. Esto facilita mucho tanto mantener de forma organizada
nuestras incidencias, como también poderlas asignar y actualizar conforme a sus
necesidades.

Una de estas herramientas es {\em Mantis Bug Tracker} (o {\em MantisBT}). Es una
herramienta que se puede usar desde la {\em web}. Es {\em software libre}, y nos
permite montar nuestra propia instancia de la misma en nuestro propio servidor.

En este documento revisaremos cómo instalar y configurar {\em MantisBT} y sus
dependencias en un sistema GNU/Linux. Además revisaremos un uso básico, siendo
cómo crear una tarea nueva y modificar su estado.

\section{Instalación}

\subsection{Dependencias}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/00-install-deps.png}
		\caption{Instalación de dependencias.}
		\label{fig:install-deps}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/01-database-user.png}
		\caption{Configuración de usuario en base de datos.}
		\label{fig:database-user}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/02-secure-install.png}
		\caption{Instalación segura de MariaDB.}
		\label{fig:secure-install}
	\end{subfigure}
	\caption{Instalación y configuración de dependencias.}
\end{figure}

Antes de empezar será necesario instalar y configurar primero las dependencias.
{\em MantisBT} precisa de tres:

\begin{itemize}
	\item PHP (versión 7 o mayor)
	\item MariaDB o MySQL (en nuestro caso usaremos MariaDB)
	\item Apache 2 (o cualquier servidor HTTP, como NGinx; mas en nuestro caso
		nos centraremos en Apache)
\end{itemize}

En nuestro caso, estamos instalando desde un sistema de Linux Mint, que usa el
sistema de gestión de paquetes {\tt apt} (figura \ref{fig:install-deps}), como
en Debian y Ubuntu. Por lo tanto, para instalar nuestras dependencias usaríamos
el siguiente comando:

\begin{verbatim}
sudo apt install php php-cli php-fpm php-mysql \
    php-zip php-gd php-mbstring php-curl php-xml \
    php-pear php-bcmath apache2 mariadb-server \
    mariadb-client
\end{verbatim}

Una vez instaladas estas cosas, los servicios se activarán de forma automática
(generalmente) en sistemas basadas en Debian, pero si este no es tu caso, puedes
activarlas con los siguientes comandos:

\begin{verbatim}
sudo systemctl start apache2
sudo systemctl start mariadb
\end{verbatim}

Una vez que se haya completado la instalación inicial, hay que configurar la
base de datos (figura \ref{fig:database-user}). Para esto entramos en la base de
datos y configuramos el usuario {\em root} de la forma siguiente para actualizar
el {\em plugin} de autenticación:

\begin{verbatim}
mysql -u root
UPDATE mysql.user SET plugin = 'mysql_native_password'
    WHERE User = 'root';
FLUSH PRIVILEGES;
QUIT;
\end{verbatim}

Una vez que hayamos acabado con esto, podemos finalizar la configuración de
nuestra base de datos con el siguiente comando (figura
\ref{fig:secure-install}):

\begin{verbatim}
sudo mysql_secure_installation
\end{verbatim}

Esto es un pequeño programa de configuración e instalación que lo usaremos para
configurar la nueva contraseña de {\em root}. Cuando pregunta por la contraseña
actual, que no tenemos, simplemente introduces una línea en blanco. Luego
preguntará por la nueva contraseña. En todos los demás simplemente introducimos
una línea en blanco para tomar la decisión por defecto.

Después de todo esto, tan sólo hemos de configurar el usuario que usará
MantisBT, y crear la base de datos en sí. Esto se hace con los siguientes
comandos:

\begin{verbatim}
CREATE USER 'mantisbt'@'localhost' IDENTIFIED BY
    'CatholicMantis';
CREATE DATABASE mantisbt;
GRANT ALL PRIVILEGES ON mantisbt.* TO 'mantisbt'@'localhost';
FLUSH PRIVILEGES;
QUIT;
\end{verbatim}

Aquí se puede cambiar {\tt CatholicMantis} por otra contraseña. El valor {\tt
mantibt} es el nombre de nuestra base de datos. Nuestro usuario también se
nombra {\tt mantisbt}. Para comprobar su se ha configurado correctamente este
usuario, se puede hacer entrando en el {\em shell} de MariaDB usando el comando
siguiente:

\begin{verbatim}
mysql -u mantisbt -p
\end{verbatim}

\subsection{Instalación de MantisBT}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/05-descargar.png}
		\caption{Descarga.}
		\label{fig:descargar}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/06-install-mantisbt.png}
		\caption{Instalación de MantisBT}
		\label{fig:install-mantisbt}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/07-mantisbt-start.png}
		\caption{Primera vista de MantisBT.}
		\label{fig:mantisbt-start}
	\end{subfigure}
	\caption{Instalación de MantisBT}
\end{figure}

Una vez configuradas nuestras dependencias, podemos instalar MantisBT en sí.
Esto se hace primero descargando el archivo comprimido que contiene el código de
la herramienta (figura \ref{fig:descargar}). Esto se puede hacer en la siguiente
URL:

\hfill

\url{https://mantisbt.org/download.php}

\hfill

Una vez que hayamos descargado nuestro código hemos de descomprimirlo, moverlo a
una ruta más accesible para el servicio de HTTP (i.e.\ Apache), configurar
nuestro servidor para apuntar a esa ruta, y finalmente reiniciar el servicio
HTTP (figura \ref{fig:install-mantisbt}). Esto se hace corriendo los siguientes
comandos:

\begin{verbatim}
unzip mantisbt-{version}.zip
sudo mkdir -p /srv/
sudo mv mantisbt-{version}/ /srv/
sudo chmod -R www-data:www-data /srv/mantisbt
\end{verbatim}

Con esto ya tendremos los archivos instalados, pero aún tenemos que configurar
Apache para que apunte a la ruta correcta para leer nuestros archivos. Para
esto, creamos un nuevo archivo:

\begin{verbatim}
/etc/apache2/sites-enabled/mantisbt.conf
\end{verbatim}

\noindent
E introducimos dentro de él lo siguiente:

\begin{verbatim}
<VirtualHost *:80>
    ServerAdmin admin@example.com
    DocumentRoot "/srv/mantisbt"
    ServerName bt.example.com
    ServerAlias www.bt.example.com
    ErrorLog "/var/log/apache2/mantisbt-error_log"
    TransferLog "/var/log/apache2/mantisbt-access_log"
    <Directory "/srv/mantisbt/">
        DirectoryIndex index.php index.html
        Options FollowSymLinks
        AllowOverride None
        Require all granted
        Options MultiViews FollowSymlinks
    </Directory>
</VirtualHost>
\end{verbatim}

\noindent
En este código, los variables importantes son los siguientes:

\begin{itemize}
	\item {\tt admin@example.com} es el correo electrónico del administrador de
		nuestro servicio.
	\item {\tt /srv/mantisbt} es la ruta donde se encuentra MantisBT.
	\item {\tt bt.example.com} es el nombre de dominio que tendrá (inclusive su
		{\em alias} en la línea siguiente).
\end{itemize}

Para verificar que el código lo hemos escrito (o copiado) correctamente, podemos
probarlo corriendo el comando:

\begin{verbatim}
sudo apachectl -t
\end{verbatim}

Si éste responde con la línea {\tt Syntax OK}, entonces es que se ha escrito
correctamente y Apache podrá leer sin errores lo que hemos escrito.

En nuestro caso, como hemos puesto un nombre de dominio que no es nuestro (i.e.\
{\tt bt.example.com}), y que no enruta a nuestro servidor, podemos simularlo
modificando el archivo {\tt /etc/hosts} y añadiendo la línea siguiente:

\begin{verbatim}
127.0.0.1    bt.example.com
\end{verbatim}

Al hacer todo esto, podemos ir a nuestro navegador e introducir en la entrada de
URL {\tt http://bt.example.com} y nos debería salir ya MantisBT (figura
\ref{fig:mantisbt-start}).

\section{Configuración}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/08-config-mantisbt.png}
		\caption{Opciones de la base de datos.}
		\label{fig:config-mantisbt}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/09-config-complete.png}
		\caption{Resultado de configuración.}
		\label{fig:config-complete}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/10-admin-panel.png}
		\caption{Panel administrador.}
		\label{fig:admin-panel}
	\end{subfigure}
	\caption{Configuración de MantisBT.}
\end{figure}

Para configurar nuestro servicio de MantisBT, hemos de configurarlo de acuerdo a
las opciones que habíamos configurado antes para nuestro base de datos con
MariaDB (figura \ref{fig:config-mantisbt}). Principalmente nos interesa
modificar los siguientes apartados:

\begin{itemize}
	\item {\bf Username (for Database):} mantisbt
	\item {\bf Password (for Database):} CatholicMantis
	\item {\bf Database name (for Database):} mantisbt
	\item {\bf Default Time Zone:} Madrid
\end{itemize}

Con todo esto configurado, le podemos dar ya al botón que dice {\em
Install/Upgrade Database}. Esto empezará por popular nuestra base de datos con
tablas y entradas necesarias para MantisBT, y por lo tanto es algo que puede
tardar unos minutos, aunque parezca que la conexión esté colgada.

Cuando por fin termina, devolverá una nueva página con los resultados (figura
\ref{fig:config-complete}), donde no debería de haber ningún error. Si hubiese
algún error es preciso buscar cómo resolverlo. Si vamos al cabo de la página
encontraremos un botón para ir al panel del administrador (figura
\ref{fig:admin-panel}). Aquí se nos pedirá entrar en sesión del administrador,
que por defecto tiene usuario {\tt administrator} y de contraseña {\tt root}.
Cuando entremos en sesión por primera vez, se nos pedirá cambiar esta
contraseña.

\section{Creación y Manejo de Tareas}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/11-create-project.png}
		\caption{Crear proyecto.}
		\label{fig:create-project}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/12-project-properties.png}
		\caption{Propiedades del proyecto.}
		\label{fig:project-properties}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/13-create-new-issue.png}
		\caption{Crear una tarea nueva.}
		\label{fig:create-new-issue}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/14-issue-view.png}
		\caption{Vista de tarea.}
		\label{fig:issue-view}
	\end{subfigure}
	\caption{Creación de una tarea.}
\end{figure}

Para empezar a usar nuestra herramienta, lo más útil es empezar creando un
proyecto. Esto lo podemos hacer entrando en {\em Manage $\rightarrow$ Manage
Projects $\rightarrow$ Create New Project} (figura \ref{fig:create-project}).
Esto nos llevará a una nueva pantalla donde podremos editar las propiedades del
proyecto, aunque el único campo preciso es el de {\em Project Name} (figura
\ref{fig:project-properties}).

Para crear una tarea nueva, hemos de entrar en donde dice {\em Report Issue}.
Esto nos llevará a una página que se parece a aquella donde configuramos las
propiedades del proyecto, pero con más opciones (figura
\ref{fig:create-new-issue}). Introducimos la información pertinente que nos hace
falta para nuestra tarea y la mandamos. Al finalizarla veremos un resumen de
nuestra tarea con todos los estados actuales que tiene (figura
\ref{fig:issue-view}).

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/15-set-status.png}
		\caption{Asignar estado.}
		\label{fig:set-status}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/16-close-issue.png}
		\caption{Cerrar caso.}
		\label{fig:close-issue}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.75\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/17-closed.png}
		\caption{Caso cerrado.}
		\label{fig:closed}
	\end{subfigure}
	\caption{Estados de un caso.}
\end{figure}

Los casos en sí tienen muchas opciones, pero la que más nos importa es su
estado (i.e.\ la propiedad {\em Status}). Ésta la podemos modificar en lo bajo
del reportaje donde pone {\em Change Status to:} (figura \ref{fig:set-status}).
Nos provee una variedad de opciones:

\begin{itemize}
	\item {\bf New:} una tarea nueva que se acaba de crear. Se mantiene en este
		estado hasta que sea asignada o reconocida (i.e.\ {\em Acknowledged}).
	\item {\bf Acknowledged:} se reconoce que la tarea es algo en que quieren
		trabajar los desarrolladores.
	\item {\bf Confirmed:} se ha confirmado y reproducido la incidencia
		relacionada con la tarea en cuestión.
	\item {\bf Assigned:} se le ha asignado la tarea a algún miembro para que
		trabaje sobre el caso como miembro dedicado.
	\item {\bf Resolved:} cuando una tarea se resuelve, aunque no necesariamente
		que se haya arreglado la incidencia. Puede tomar varias formas (e.g.\
		{\em fixed}, {\em won't fix}, {\em duplicate}, etc.).
	\item {\bf Closed:} cuando se cierra definitivamente una tarea de tal modo
		que ya no se aceptan más aportaciones sobre el asunto.
\end{itemize}

Si intentamos cerrar el caso cambiando el estado a {\em Closed} nos traerá a una
nueva página donde describimos por qué (figura \ref{fig:close-issue}). Cuando
finalicemos esto pulsando {\em Close Issue}, cerramos el caso y nos devolverá a
la página de propiedades del caso con el caso ya cerrado (figura
\ref{fig:closed}).

\section{Conclusión}

La interfaz de MantisBT no es demasiado intuitiva, y se puede perderse uno
fácilmente. Dicho lo cual, sí que parece una herramienta muy especializada para
su tarea, comparada al menos con otras que vienen integradas dentro de
repositorios públicos, como serían aquellas de GitHub o GitLab. A la misma vez,
tener tantas opciones también puede conducir a una sobrecarga de información,
causando confusión en vez de ayudar a resolver problemas.

\pagebreak

\section{Derechos de Autor y Licencia}

\noindent
Copyright \copyright\ \the\year\ Nicolás A. Ortega Froysa
<nicolas@ortegas.org>\\
\\
Este documento se distribuye bajo los términos y condiciones de la licencia
Creative Commons Attribution No Derivatives 4.0 International.

\end{document}
