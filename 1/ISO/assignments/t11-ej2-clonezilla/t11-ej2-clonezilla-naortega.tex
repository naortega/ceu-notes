\documentclass[12pt,a4paper]{article}
\usepackage[spanish]{babel}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}

\title{Tema XI Ejercicio II: Clonación de un Servidor}
\author{Nicolás A. Ortega Froysa}

\begin{document}
\maketitle
\pagebreak
\tableofcontents
\pagebreak

\section{Introducción}

Un elemento fundamental del mantenimiento de sistemas es hacer copias de
respaldo, por lo que pudiera ocurrir. Hay varias formas de hacer este tipo de
copias, tanto por el formato que toman como por los datos que respaldan. En el
caso de esta práctica nos centraremos en usar la herramienta {\em Clonezilla}
para hacer una imagen de un disco entero.

\section{Descarga}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.55\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/scrot-2022-05-13_09-49-51.png}
		\caption{Versiones disponibles de {\em Clonezilla}.}
		\label{fig:clonezilla-versions}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.4\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/scrot-2022-05-13_09-50-18.png}
		\caption{Selección del tipo de descarga.}
		\label{fig:clonezilla-type}
	\end{subfigure}
	\caption{Descarga de {\em Clonezilla}.}
\end{figure}

Lo primero que hemos de hacer es descargar la imagen de {\em Clonezilla} que
queremos usar. En la página de descargas del sitio web de {\em
Clonezilla}\footnote{https://clonezilla.org/downloads.php} aparecen varias
versiones, principalmente dos tipos: basadas en Debian o en Ubuntu. Para
nuestros propósitos vamos a usar la versión {\em stable} basada en Debian
(figura \ref{fig:clonezilla-versions}).

A continuación nos preguntará por el tipo de descarga que queremos. Esto nos da
tres opciones (figura \ref{fig:clonezilla-type}):

\begin{itemize}
	\item {\em CPU architecture}: la arquitectura de la CPU de la máquina en
		cuestión. En nuestro caso, usaremos {\tt amd64} (también vale para Intel
		y cualquier máquina de arquitectura x86\_64).
	\item {\em File type}: el tipo de formato en el que queremos la imagen, que
		puede ser un ZIP para extraer los archivos sobre un sistema de ficheros
		ya creado, o una ISO para poder grabarla en un dispositivo. Nos interesa
		usar la ISO.
	\item {\em Respository}: seleccionar de dónde descargar. Esto realmente es
		lo de menos. Vamos con {\em auto}.
\end{itemize}

Una vez descargada la imagen (ISO) podemos instalarla en un dispositivo USB
mediante el comando siguiente como {\em root} -- {\bf AVISO:} cambiar {\tt
/dev/sdX} por el fichero de dispositivo que tienes en tu máquina que corresponde
a tu dispositivo USB:

\begin{verbatim}
# dd if=clonezilla-live-2.8.1-12-amd64.iso of=/dev/sdX bs=1M
\end{verbatim}

Con esto, ya deberíamos tener un dispositivo USB con {\em Clonezilla} instalado
para usarlo.

\section{Creando una Imagen de Respaldo}

Antes de empezar, es necesario tener a nuestra disposición no sólo el
dispositivo USB con {\em Clonezilla}, sino también otro dispositivo de
almacenamiento para donde guardar la imagen de copia de respaldo. Es lo suyo que
esté formateado con EXT4, que es el formato más estándar en GNU/Linux (y {\em
Clonezilla} es una distribución de GNU/Linux).

Con estos dos elementos, podemos empezar a hacer la copia de respaldo de nuestra
máquina. Enchufamos ambos dispositivos a la máquina y la inicializamos desde el
dispositivo con {\em Clonezilla}.

\subsection{Configuración Inicial}

Antes de empezar con la tarea, nos preguntará acerca de la lengua que queremos
usar, y el tipo de teclado que tenemos. Vamos a seleccionar lo que es la opción
dominante en el mundo de la informática: el inglés; y el mejor teclado del
mundo: el estadounidense.

A partir de aquí entramos en {\em Clonezilla} en sí para poder hacer la copia de
respaldo.

\subsection{Selección de Dispositivos}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/scrot-2022-05-14_19-53-39.png}
		\caption{Selección del modo de trabajar con {\em Clonezilla}.}
		\label{fig:select-work-mode}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/scrot-2022-05-14_19-53-52.png}
		\caption{Selección del destino de la imagen.}
		\label{fig:select-destination}
	\end{subfigure}
	\caption{}
\end{figure}

Lo primero que nos va a preguntar es de qué forma queremos trabajar, y nos da
una variedad de opciones (figura \ref{fig:select-work-mode}):

\begin{itemize}
	\item {\tt device-image}: trabajar usando dispositivos o particiones de
		éstos junto a imágenes. Esta opción es la que vamos a usar.
	\item {\tt device-device}: trabajar usando dispositivos o particiones de
		éstos y guardar las copias directamente a otro dispositivo -- no como
		imagen, sino como si el otro dispositivo fuese una copia exacta de la
		misma.
	\item {\tt remote-source}: para hacer clonación remota de tal modo que esta
		máquina sea la fuente de la que se está haciendo la clonación.
	\item {\tt remote-dest}: para hacer clonación remota de tal modo que esta
		máquina sea el destino de la clonación.
	\item {\tt lite-server}: crear un servidor {\em lite} para guardar
		clonaciones.
	\item {\tt lite-client}: crear un cliente {\em lite} para hacer clonaciones
		y enviarlas al servidor.
\end{itemize}

Al elegir que vamos a usar la opción {\tt device-image}, tenemos que elegir
dónde vamos a guardar la imagen que resulta de la clonación. Existen varios
métodos de usar servicios remotos (e.g.\ SSH, SAMBA, NFS, etc.), pero como
tenemos un dispositivo de copia de respaldo disponible, vamos a usar la opción
{\tt local\_dev} (figura \ref{fig:select-destination}). Después nos pedirá
insertar el dispositivo USB donde vamos a almacenar la imagen resultante. Como
ya la tenemos enchufada, simplemente pulsamos {\em Enter}. Esto nos mostrará los
dispositivos que tenemos a nuestra disposición y algunos sobre cada uno.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/scrot-2022-05-14_19-56-42.png}
		\caption{Selección de partición de repositorio.}
		\label{fig:repository-partition-selection}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/scrot-2022-05-14_19-58-40.png}
		\caption{Selección de directorio donde guardar las imágenes.}
		\label{fig:repository-directory-selection}
	\end{subfigure}
	\caption{}
\end{figure}

A continuación tenemos que decidir en qué partición vamos a guardar la imagen de
respaldo que hacemos (figura \ref{fig:repository-partition-selection}). En
nuestro caso, tenemos disponible los 16GB de {\tt sdb1} para guardar imágenes.
Después, antes de montarlo, nos preguntará si queremos hacerle un {\em
filesystem check} (i.e.\ {\tt fsck}). Esto sirve para comprobar el estado del
disco y {\em repararlo} si fuese necesario. Como en nuestro caso estamos
tratando con un dispositivo nuevo, vamos a saltar este paso seleccionando la
opción {\tt no-fsck}.

Después nos facilitará un navegador de archivos muy básico para navegar y
seleccionar el directorio en que queremos guardar la imagen (figura
\ref{fig:repository-directory-selection}). Como lo queremos guardar directamente
en el directorio de más alto nivel de nuestro dispositivo de almacenamiento,
simplemente seleccionamos <<Done>>.

Cuando nos pregunte por el modo de uso que queremos usar, como lo que queremos
hacer es bastante simple, vamos a usar el <<Beginner mode>>. Para el nombre de
archivo de la imagen, vamos a aceptar el nombre que viene por defecto, que es la
fecha actual.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/scrot-2022-05-14_19-59-42.png}
		\caption{Selección de fuente de imagen.}
		\label{fig:select-source}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/scrot-2022-05-14_20-00-52.png}
		\caption{Confirmación de opciones.}
		\label{fig:confirm-options}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/scrot-2022-05-14_20-01-07.png}
		\caption{Proceso de clonación.}
		\label{fig:cloning-process}
	\end{subfigure}
	\caption{}
\end{figure}

Ya podemos seleccionar el disco que queremos usar para hacer la clonación. En
nuestro caso, como no podemos hacer copias del dispositivo a la que vamos a
guardar (i.e.\ {\tt sdb}) tan sólo podemos guardar al dispositivo {\tt sda}
(figura \ref{fig:select-source}). Para este paso, también saltaremos el paso de
hacer un {\tt fsck}, seleccionando la opción {\tt -sfsck}. Además, nos
preguntará si queremos asegurarnos de que la imagen sea íntegra y se pueda
restaurar, a la cual respondemos que sí. También nos preguntará si queremos
cifrar la imagen o no (en nuestro caso: no, {\tt -senc}). La última opción es de
seleccionar qué hará {\em Clonezilla} cuando acabe de hacer la copia de
respaldo. Como a partir de entonces ya se habrá acabo nuestra tarea, le decimos
que apague al ordenador (i.e.\ {\tt -p poweroff}). Finalmente nos pedirá
confirmar nuestras selecciones para continuar (figura
\ref{fig:confirm-options}). Cuando le decimos que sí, empezará el proceso de
clonación (figura \ref{fig:cloning-process}), después del cual se apagará el
equipo.

\section{Restauración del Sistema}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/scrot-2022-05-17_10-00-15.png}
		\caption{Destrucción de los datos de nuestro servidor.}
		\label{fig:server-murking}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/scrot-2022-05-17_10-05-25.png}
		\caption{Selección de acción.}
		\label{fig:select-action}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/scrot-2022-05-17_10-06-17.png}
		\caption{Restauración del sistema.}
		\label{fig:restoring-system}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/scrot-2022-05-17_10-09-31.png}
		\caption{Sistema restaurado.}
		\label{fig:restored-system}
	\end{subfigure}
	\caption{}
\end{figure}

Como es evidente, a veces hacemos cosas tontas (figura \ref{fig:server-murking})
y es necesario restablecer la copia de respaldo. Para esto, entramos de nuevo en
{\em Clonezilla} para poder restaurar nuestro sistema desde la imagen que hemos
hecho. Para esto, vamos a seleccionar la opción, en vez de {\tt savedisk}, {\tt
restoredisk} (figura \ref{fig:select-action}). Cuando montemos el dispositivo de
copias de respaldo, nos mostrará las imágenes que tiene disponibles (que sólo
tenemos una). Seleccionamos la que queremos. También seleccionamos el
dispositivo en el que queremos implantar la copia. Nos preguntará, como antes,
si queremos asegurarnos de que se {\em restaurable}, y qué se debería de después
de la restauración (que apagaremos el dispositivo como la última vez). Al acabar
este proceso de selección, empezará a restaurar nuestro sistema a partir de la
imagen (figura \ref{fig:restoring-system}).

Al finalizar todo este proceso, se encontrará el sistema en las mismas
condiciones en las que se ha dejado en esa revisión de imagen que hayamos
guardado (figura \ref{fig:restored-system}).

\section{Conclusión}

El {\em Clonezilla} es una herramienta muy útil y eficaz en hacer copias de
seguridad. Se pueden hacer una variedad de operaciones de clonación, y su menú
(al menos la versión de principiantes) es fácil de navegar, comprender, y usar.
Tiene el inconveniente de que no puede estar inicializado el ordenador en el
sistema normal mientras se hace la copia, y requiere de más interacción humana,
lo cual hacer copias automatizadas quizá no sea tan fácil si hay que tener la
máquina fuente inicializada desde una imagen de {\em Clonezilla}.

\pagebreak

\section{Derechos de Autor y Licencia}

\noindent
Copyright \copyright\ \the\year\ Nicolás A. Ortega Froysa
<nicolas@ortegas.org> \\
\\
Este documento se distribuye bajo los términos y condiciones de la licencia
Creative Commons Attribution No Derivatives 4.0 International.

\end{document}
