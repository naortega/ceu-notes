#!/bin/bash

if [ $# -lt 5 ]
then
	echo "Mínimo de 5 parámetros. Has introducido tan sólo $#."
	exit 1
fi

echo "Se han introducido $# parámetros."
echo "Los primeros tres son los siguientes:"

# el primer parámetro es el programa en sí, así que empezamos por 1
for i in ${@:1:3}
do
	echo $i
done
