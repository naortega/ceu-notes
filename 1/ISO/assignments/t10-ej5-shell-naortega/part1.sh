#!/bin/bash

mkdir /home/user/
mkdir -p /home/user/{dir1/dir11, dir2/dir21, dir3/dir31, dir4}
touch /home/user/dir2/dir21/notas.txt
mv /home/user/dir2/dir21/notas.txt /home/user/dir4/
cp /home/user/dir4/notas.txt /home/user/dir3/dir31/
# asumo que es crear el archivo ligadefamosos.doc en dir31
touch /home/user/dir3/dir31/ligadefamosos.doc
chmod 700 /home/user/dir3/dir31/ligadefamosos.doc
chmod u+rwx,go-rwx /home/user/dir3/dir31/ligadefamosos.doc
chmod 777 /home/user/dir3/dir31/ligadefamosos.doc
chmod a+rwx /home/user/dir3/dir31/ligadefamosos.doc
chmod 500 /home/user/dir3/dir31/ligadefamosos.doc
chmod u+rx-w go-rwx /home/user/dir3/dir31/ligadefamosos.doc
