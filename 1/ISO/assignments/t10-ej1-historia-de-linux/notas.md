# Historia de GNU/Linux

## Inicios de Software Libre

Para hablar de GNU/Linux es necesario hablar de Richard M. Stallman. Richard
empezó su carrera en MIT en los EE.UU. en la informática. En este ambiente era
muy normal compartir código de los programas que se usaban, y así facilitar el
desarrollo. Un día Stallman tuvo un problema con una de las impresoras Xerox con
las que trabajaba, y cuando quiso obtener el acceso al código para poder
solucionarlo, se le denegó el acceso a ello. Esto sería el comienzo de la larga
campaña de Richard Stallman en contra del código privativo, y el movimiento
fundado por él de __software libre__.

El __software libre__ es todo aquel software que, en su enteridad, provea al
usuario las siguientes cuatro libertades:

0. **Uso:** que no haya restricciones en el uso del software.
1. **Estudio:** que se pueda estudiar libremente el código del programa y
   modificarlo.
2. **Distribución:** poder distribuir copias exactas del software.
3. **Ayudar:** poder distribuir copias modificadas del software.

Stallman fundó dos organizaciones distintas: el proyecto GNU y la FSF. El
proyecto GNU es un proyecto cuyo objetivo es la creación de un sistema operativo
completamente libre en todos sus componentes. Sus siglas son recursivas, en el
sentido de que la primera letra representa las siglas en sí: GNU. En su
totalidad representan GNU is Not UNIX, ya que GNU quiso crear un SO parecido al
de UNIX. Hay muchos programas conocidos de GNU como sería GIMP, GNOME, y Ring.

También fundó, en paralelo, el __Free Software Foundation__ (FSF), que se
encarga de promover el __software libre__. Actualmente organiza muchos eventos
en todo el mundo para promover el __software libre__, y ayuda organizaciones y
gobiernos para migrar a __software libre__.

## Introducción de Linux

Ya para los años 90, GNU había creado bastantes programas para un SO completo,
mas le faltaba el elemento más fundamental: el __kernel__, que se encarga de
abstraer el __hardware__ para el uso del __software__ de usuario.

Mientras tanto, un finlandés llamado Linus Torvalds estuvo desarrollando su
propio __kernel__ para uso propio. Era simplemente un proyecto personal de
__hobby__, que luego lo publicó en un foro de programadores, pero rápidamente
atraía la atención de muchos que estaban buscando un __kernel__ para compaginar
con el sistema de GNU. Durante los primeros años, muchos creaban lo que hoy
sería su propia distribución, combinando GNU y Linux por separado, pero no había
ninguna imagen que viniera de forma completa, como pasa hoy. Entonces Ian
Murdock sacó la primera distribución oficial de GNU y Linux: Debian GNU/Linux.
En este momento, nació de verdad la leyenda que es GNU/Linux y sus muchísimos
(demasiados) derivados.
