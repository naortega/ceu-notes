\babel@toc {spanish}{}\relax 
\contentsline {section}{\numberline {1}Introducción}{3}{section.1}%
\contentsline {section}{\numberline {2}Requisitos}{3}{section.2}%
\contentsline {section}{\numberline {3}Configuración}{4}{section.3}%
\contentsline {subsection}{\numberline {3.1}Navegación Y Selección}{4}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Localización}{5}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Red}{6}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Usuarios}{7}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Zona Horaria}{8}{subsection.3.5}%
\contentsline {subsection}{\numberline {3.6}Particionado Del Disco}{8}{subsection.3.6}%
\contentsline {subsection}{\numberline {3.7}Instalación De Software}{10}{subsection.3.7}%
\contentsline {subsection}{\numberline {3.8}GRUB}{11}{subsection.3.8}%
\contentsline {subsection}{\numberline {3.9}Finalización}{12}{subsection.3.9}%
\contentsline {section}{\numberline {4}Conclusión}{12}{section.4}%
\contentsline {section}{\numberline {5}Derechos de Autor y Licencia}{13}{section.5}%
