\documentclass[12pt,a4paper]{article}
\usepackage[spanish]{babel}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}

\title{Tema XI Ejercicio I: Instalación De Un S.O. De Servidor}
\author{Nicolás A. Ortega Froysa}

\begin{document}
\maketitle
\pagebreak
\tableofcontents
\pagebreak

\section{Introducción}

El objetivo de esta práctica es la instalación de un sistema operativo para el
propósito de ser usado como servidor (i.e.\ sin entorno gráfico), que
preferiblemente venga ya instalado con el servicio SSH que no servirá para
interactuar con la máquina de manera remota.

Este tipo de sistema nos es útil para aquellos casos donde la máquina no tiene
necesidad de mostrar ningún entorno gráfico, ya que su usuario no lo usa de
forma local, sino a través de unos servicios de red que se proveen (e.g.\
servidor web). En estos casos no es necesario ni que el ordenador esté conectado
a una pantalla para cumplir su función.

\section{Requisitos}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.75\linewidth]{imgs/00-download.png}
	\caption{Download.}
	\label{fig:download}
\end{figure}

Hay muchas distribuciones GNU/Linux que se pueden usar como S.O. para un
servidor. En el caso de esta práctica, se documentará la instalación de Debian
11 (Bullseye). Para ello, se descarga la ISO de su página web, \url{debian.org},
que nos descargará la versión estable más reciente (actualmente Bullseye) de la
ISO de instalación por red que se denominan {\tt netinst} (figura
\ref{fig:download}).

Cuando ya esté completa la descarga, se tiene que instalar la ISO en un
dispositivo de instalación (e.g.\ CD, {\em pendrive}, tarjeta SD). En GNU/Linux
esto se puede hacer con el comando siguiente, cambiando {\tt \<dispositivo\>}
con el nombre del archivo de dispositivo donde queremos instalar la ISO:

\begin{verbatim}
$ dd if=debian-11.3.0-amd64-netinst.iso \
    of=<dispositivo> bs=1M status=progress
\end{verbatim}

Una vez instalada la ISO en el dispositivo, iniciamos la máquina que nos servirá
de servidor y la arrancamos desde el dispositivo insertado.

\section{Configuración}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.75\linewidth]{imgs/01-install-menu.png}
	\caption{Install menu.}
	\label{fig:install-menu}
\end{figure}

Una vez iniciada la máquina, nos abrirá un menú para seleccionar qué tipo de
instalación (o mejor dicho: instalador) queremos usar (figura
\ref{fig:install-menu}). Como un servidor no tiene por qué tener tarjeta
gráfica, usamos la instalación normal (no gráfica).

\subsection{Navegación Y Selección}

Como no tenemos acceso a un ratón para el proceso de esta instalación, tendremos
que usar el teclado para todo. Generalmente es bastante intuitivo usarse. Se
navega entre las distintas opciones con las flechas del teclado, y se selecciona
con el espacio o {\em Enter}. Luego, para salir de la selección principal y
entrar en la selección de {\em Continuar} o {\em Atrás}, se usa la tecla {\em
Tab}.

\subsection{Localización}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/02-language-select.png}
		\caption{Language select.}
		\label{fig:language-select}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/03-location-select.png}
		\caption{Location select.}
		\label{fig:location-select}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/04-encoding-select.png}
		\caption{Locale select.}
		\label{fig:encoding-select}
	\end{subfigure}
	\caption{Locale configuration.}
\end{figure}

Lo primero que se nos pedirá es configurar la localización de nuestra máquina
(e.g.\ lengua, teclado, etc.). En primer lugar, definimos la lengua que queremos
usar para nuestro servidor (figura \ref{fig:language-select}). Usaremos el
inglés ya que tiene más soporte es confunde mucho cuando en la informática se
mezcla el inglés y el español. Notamos que aunque la lengua usada por el
servidor sea, por ejemplo, inglés, no quiere decir que no pueda proveer
servicios en otra, como el español. Sólo se trata de que en cuanto a la
administración del servidor en sí todos los mensajes nos van a salir en inglés.

En segundo lugar definimos la localización física de la máquina. En nuestro
caso: {\em Spain} (España). Como hemos puesto el inglés como lengua de nuestro
servidor, nos mostrará el principio sólo aquellos países donde se habla el
inglés como primera lengua. Como no queremos ninguno de éstos seleccionamos {\em
others} $\rightarrow$ {\em Europe} $\rightarrow$ {\em Spain} (figura
\ref{fig:location-select}).

Finalmente, definimos el {\em locale}. Como las lenguas no sólo dependen de
idioma, sino también de dialecto, y los caractéres que se necesitan pueden ser
diferentes dependiendo de esto, definimos el locale por el dialecto específico
que queremos usar. Como lo más cómodo (para mí) es el inglés estadounidense,
especificamos {\tt en\_US.UTF-8} (figura \ref{fig:encoding-select}).

\subsection{Red}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/06-set-hostname.png}
		\caption{Set hostname.}
		\label{fig:set-hostname}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\includegraphics[width=0.95\linewidth]{imgs/07-set-domain-name.png}
		\caption{Set domain name.}
		\label{fig:set-domain-name}
	\end{subfigure}
	\caption{Network configuration.}
\end{figure}

A continuación nos pedirá configurar la red. Lo primero es definir el {\em
hostname}, que sería el nombre de red que tiene la máquina (figura
\ref{fig:set-hostname}). Esto es útil para la red interna para reconocer y
encontrar ordenadores por un nombre en vez de su dirección IP. También nos
permite distinguir entre máquinas dentro de la red en cuanto a archivos de {\em
logs}, correo electrónico, y demás.

En el caso de un servidor, es especialmente importante definir el {\em domain
name} (figura \ref{fig:set-domain-name}). Si es un servidor con acceso a la red
externa, esto debe de tener el nombre que usaremos en la red externa (e.g.\ {\tt
mi-servidor.es}). Como en nuestro caso estamos creando un servidor de pruebas de
uso interno, usaremos el dominio {\tt home-server.local}.

\subsection{Usuarios}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/08-set-root-password.png}
		\caption{Set root password.}
		\label{fig:set-root-password}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/09-set-user-full-name.png}
		\caption{Set user full name.}
		\label{fig:set-user-full-name}
	\end{subfigure}
	\caption{User configuration.}
\end{figure}

A continuación configuramos los usuarios de nuestro sistema. En primer lugar,
configuramos la contraseña del usuario {\em Root} (figura
\ref{fig:set-root-password}), que es el usuario administrador del sistema. Este
usuario conviene que tenga una contraseña muy segura, ya que tiene todos los
permisos. Nos pedirá también verificar la contraseña, introduciendo ésta de
nuevo.

Después nos pide crear un usuario normal (figura \ref{fig:set-user-full-name}).
Para esto, primero nos pide el nombre completo, que se usa simplemente por
motivos estéticos y burocráticos. Posteriormente nos pedirá el nombre de
usuario, que es lo que usaremos para entrar en nuestro usuario en la máquina.
Para este nombre, hemos de evitar espacios (e.g.\ {\tt nicolas}). Finalmente
definimos la contraseña para este usuario.

\pagebreak

\subsection{Zona Horaria}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.75\linewidth]{imgs/10-set-timezone.png}
	\caption{Set timezone.}
	\label{fig:set-timezone}
\end{figure}

Para tener la hora correcta, y poder sincronizar la hora con servidores
exteriores, es necesario definir nuestra zona horaria. Como ya habíamos
seleccionado el país antes como España, ahora nos mostrará tan sólo las zonas
horarias disponibles para nuestro país (figura \ref{fig:set-timezone}).

\subsection{Particionado Del Disco}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/11-disk-setup-menu.png}
		\caption{Disk setup menu.}
		\label{fig:disk-setup-menu}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/12-select-partition-scheme.png}
		\caption{Select partition scheme.}
		\label{fig:select-partition-scheme}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/13-partitioning-overview.png}
		\caption{Partitioning overview.}
		\label{fig:partitioning-overview}
	\end{subfigure}
	\caption{Disk partitioning.}
\end{figure}

Para poder instalar debidamente el sistema operativo, hemos de particionar el
formatear el disco. Del formateo se encargará directamente el instalador, que
usará por defecto el formato EXT4.

La primera pantalla nos mostrará varias opciones para el particionado del disco
(figura \ref{fig:disk-setup-menu}). Como queremos usar el disco entero, y no
tenemos otros sistemas instalados, seleccionamos la primera opción: <<Guided --
use entire disk>>.

Para el particionado del disco, podemos dividir el sistema de archivos de
nuestro sistema en varias particiones. Lo más usual es que el directorio {\tt
/home/} tenga su propia partición, para poder separar los datos de usuario y los
archivos de sistema. En nuestro caso, no nos interesa, ya que como servidor los
usuarios no tendrán ficheros propios (por lo general). Seleccionamos la primera
opción: <<All files in one partition>> (figura
\ref{fig:select-partition-scheme}).

Finalizado esto, nos mostrará por pantalla un resumen de los cambios que se van
a realizar, y nos pedirá la confirmación (figura
\ref{fig:partitioning-overview}). Como esta acción es permanente y podemos
perder información que pueda haber en el disco anteriormente, la opción por
defecto es {\em no} hacer los cambios, luego entonces es necesario cambiar a
decir que sí.

\subsection{Instalación De Software}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/15-scan-for-extra-installation-media.png}
		\caption{Scan for extra installation media.}
		\label{fig:scan-for-extra-installation-media}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/16-mirror-selection.png}
		\caption{Mirror selection.}
		\label{fig:mirror-selection}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/17-package-usage-survey.png}
		\caption{Package usage survey.}
		\label{fig:package-usage-survey}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/18-extra-packages.png}
		\caption{Extra packages.}
		\label{fig:extra-packages}
	\end{subfigure}
	\caption{Repository and package configuration/installation.}
\end{figure}

Para la instalación de {\em software} adicional, lo primero que nos va a
preguntar es por si queremos instalar algo de un dispositivo adicional (figura
\ref{fig:scan-for-extra-installation-media}). Se hace de los dispositivos ya que
es posible que la máquina no tenga acceso a {\em internet} por falta de los {\em
drivers} necesarios para los interfaces de red -- esto suele pasar con tarjetas
de Wi-Fi. Como en nuestro caso no nos hace falta, elegimos <<No>>.

A continuación pide que seleccionemos el espejo que se va a usar para descargar
paquetes software (figura \ref{fig:mirror-selection}). Son espejos porque todos
contienen los mismos paquetes (y las mismas versiones de cada paquete). Lo que
diferencia uno de otro es su localización. Se podrá descargar paquetes a mayor
velocidad de los servidores más cercanos físicamente que de los servidores más
lejanos. Como en nuestro caso estamos en España, elegimos cualquier espejo de
España.

También nos preguntará acerca de si queremos participar en el llamado <<Package
Usage Survey>> o <<Popularity Contest>> (figura \ref{fig:package-usage-survey}).
Se trata de que tu máquina enviará estadísticas anónimas a los servidores de
Debian que contendrán los paquetes que usas en tu máquina con el propósito de
saber qué paquetes son más populares y precisan de más atención de los
administradores.

Finalmente nos da algunas opciones de programas que instalar de forma adicional
(figura \ref{fig:extra-packages}). Las primeras opciones son de entornos
gráficos, que no nos sirven ya que vamos a montar un servidor. Sí que interesa
la instalación de un servidor web y servidor SSH. Lo primero nos permitirá
montar un sitio web en nuestra máquina, y lo segundo nos sirve para acceder a la
máquina de manera remota.

\subsection{GRUB}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/19-grub-installation.png}
		\caption{GRUB installation.}
		\label{fig:grub-installation}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/20-select-disk-for-grub-installation.png}
		\caption{Select disk for GRUB installation.}
		\label{fig:select-disk-for-grub-installation}
	\end{subfigure}
	\caption{GRUB configuration.}
\end{figure}

El último paso de la instalación es de instalar y configurar GRUB, que nos sirve
para elegir el sistema que queremos inicializar, o la versión de kernel que
queremos usar. Como este es el único sistema que vamos a tener instalado,
instalamos GRUB (figura \ref{fig:grub-installation}). Es necesario especificar
el disco en el que queremos instalarlo (figura
\ref{fig:select-disk-for-grub-installation}), que como nuestra máquina tan sólo
tiene un disco, lo instalamos en aquel.

\subsection{Finalización}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/21-installation-complete.png}
		\caption{Installation complete.}
		\label{fig:installation-complete}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/22-first-boot.png}
		\caption{First boot.}
		\label{fig:first-boot}
	\end{subfigure}
	\caption{Install completion.}
\end{figure}

Una vez que todo esté instalado y configurado nos pedirá reiniciar la máquina y
quitar el dispositivo de instalación (figura \ref{fig:installation-complete}).
En cuanto reiniciamos, mostrará por pantalla un {\em login} donde podemos meter
el usuario (igualmente podemos meter {\tt root}). Cuando nos pide la contraseña,
aunque no aparezca visiblemente ningún carácter en la pantalla, sí está grabando
las teclas para comparar (figura \ref{fig:first-boot}).

\section{Conclusión}

Es muchísimo más útil e intuitivo usar Debian para servidores que Windows Server
2008. Para empezar, no hay entorno gráfico, lo cual ahorramos recursos que
serían necesarios para servir a los clientes. El instalador de {\tt curses}
(i.e.\ el instalador no-gráfico) es también más bonito y me da nostalgia de mi
juventud.

\pagebreak

\section{Derechos de Autor y Licencia}

\noindent
Copyright \copyright\ \the\year\ Nicolás A. Ortega Froysa
<nicolas@ortegas.org>  \\
\\
Este documento se distribuye bajo los términos y condiciones de la licencia
Creative Commons Attribution No Derivatives 4.0 International.

\end{document}
