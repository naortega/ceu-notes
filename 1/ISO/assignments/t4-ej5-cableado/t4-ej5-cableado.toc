\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Hoja De Control Del Documento}{3}{}%
\contentsline {section}{\numberline {2}Introducción}{4}{}%
\contentsline {section}{\numberline {3}Material}{4}{}%
\contentsline {section}{\numberline {4}Fabricación del Cable}{4}{}%
\contentsline {subsection}{\numberline {4.1}Orden de los Cables}{4}{}%
\contentsline {subsection}{\numberline {4.2}Procedimiento}{5}{}%
\contentsline {section}{\numberline {5}Caso de Uso}{5}{}%
\contentsline {subsection}{\numberline {5.1}PC a PC}{5}{}%
\contentsline {subsection}{\numberline {5.2}PC-switch-PC}{6}{}%
\contentsline {subsection}{\numberline {5.3}PC-switch-switch-PC}{6}{}%
\contentsline {section}{\numberline {6}Conclusión}{6}{}%
\contentsline {section}{\numberline {7}Derechos de Autor y Licencia}{7}{}%
