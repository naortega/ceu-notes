\documentclass[12pt,a4paper]{article}
\usepackage[spanish]{babel}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}

\title{Tema X Ejercicio II: Instalación de Ubuntu en una Máquina Virtual}
\author{Nicolás A. Ortega Froysa}

\begin{document}
\maketitle
\pagebreak
\tableofcontents
\pagebreak

\section{Introducción}

El objetivo de esta práctica es la creación de una máquina virtual, usando QEMU,
para virtualizar un sistema Ubuntu. Se usará QEMU ya que es un programa de
virtualización completamente {\em (software) libre}, y facilita el uso de KVM
para optimización de la virtualización.

\section{Adquisición de Recursos}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.55\linewidth]{imgs/00-download-ubuntu.png}
	\caption{Download ubuntu.}
	\label{fig:download-ubuntu}
\end{figure}

Antes que nada, empezamos la descarga de la ISO de Ubuntu (figura
\ref{fig:download-ubuntu}), ya que esto puede tardar varios minutos, mientras
vamos instalando lo demás. Esto se puede hacer yendo a la página principal de
Ubuntu en \url{ubuntu.com} y descargando la versión {\em Desktop} de Ubuntu.

Mientras descargamos la ISO, podemos también instalar QEMU, que lo usaremos para
la virtualización. Antes que nada, queremos asegurarnos de que nuestro sistema
soporta KVM para mejorar la virtualización. Corremos el comando siguiente, y si
da algún resultado entonces tenemos activado el módulo de KVM, de lo contrario
no lo podremos usar:

\begin{verbatim}
$ lsmod | grep kvm
\end{verbatim}

Para instalar QEMU en sí, en los sistemas basadas en Debian, podemos encontrarlo
en nuestros repositorios, instalándolo con los comandos siguientes (el paquete
{\tt qemu-kvm} se puede omitir si no tienes el módulo KVM como vimos
anteriormente):

\begin{verbatim}
$ sudo apt update
$ sudo apt install qemu qemu-utils qemu-system qemu-kvm
\end{verbatim}

Esto instalará el sistema QEMU y todas sus utilidades. Al tener esto instalado,
ya podemos crear el {\em disco virtual} para nuestra máquina virtual. Esto se
hace corriendo el comando {\tt qemu-img} de forma siguiente:

\begin{verbatim}
$ qemu-img create ubuntu.img 16G
\end{verbatim}

Este comando nos creará una imagen de 16GB denominada {\tt ubuntu.img}. Es un
archivo como cualquiera, pero QEMU lo usará para almacenar datos dentro como si
fuera un disco duro.

Cuando por fin hayamos creado el disco virtual y descargado la ISO de Ubuntu,
podemos empezar con el arranque y la instalación.

\section{Configuración e Instalación}

\subsection{Arranque de la Máquina Virtual}

Para arrancar nuestra máquina virtual, hemos de correr QEMU desde la línea de
comandos usando el comando siguiente (omitiendo {\tt -enable-kvm} si no tenemos
disponible ese módulo):

\begin{verbatim}
$ qemu-system-x86_64 -hda ubuntu.img \
    -cdrom ubuntu-{version}-desktop-amd64.iso \
    -boot d -m 4G -enable-kvm
\end{verbatim}

\noindent
Explicamos cada elemento de este comando:
\begin{itemize}
	\item {\tt qemu-system-x86\_64}: correr una virtualización QEMU de un
		sistema de arquitectura x86\_64.
	\item {\tt -hda ubuntu.img}: especificamos la imagen que usaremos como disco
		virtual (HDA).
	\item {\tt -cdrom ubuntu-\{version\}-desktop-amd64.iso}: asignamos a la ISO
		como disco periférico (CDROM).
	\item {\tt -boot d}: le decimos a QEMU que inicialice desde el CDROM
		virtual en primer lugar.
	\item {\tt -m 4G}: asignamos 4GB de memoria principal.
	\item {\tt -enable-kvm}: habilitamos KVM en el caso de que tengamos el
		módulo.
\end{itemize}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.65\linewidth]{imgs/01-boot-ubuntu.png}
	\caption{Boot ubuntu.}
	\label{fig:boot-ubuntu}
\end{figure}

Cuando corremos este comando, debería de abrir una ventana nueva con nuestra
máquina virtual Ubuntu (figura \ref{fig:boot-ubuntu}).

\subsection{Configuración}

En la primera pantalla que nos muestra Ubuntu, nos permite eligir idioma, y
también seleccionar el uso que vamos a dar a este ISO ahora mismo. Si
seleccionamos a <<Try Ubuntu>> nos traerá a un escritorio Ubuntu para que lo
probemos; si seleccionamos a <<Install Ubuntu>> empezará el proceso de
instalación. En nuestro caso, queremos lo segundo.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/02-keyboard-layout.png}
		\caption{Keyboard layout.}
		\label{fig:keyboard-layout}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/03-installation-type.png}
		\caption{Installation type.}
		\label{fig:installation-type}
	\end{subfigure}
	\caption{Configuración teclado y tipo instalación.}
\end{figure}

Esto nos llevará a la primera pantalla de configuración, que es para seleccionar
el teclado que vamos a usar (figura \ref{fig:keyboard-layout}). Se distinguen
por lengua, en primer lugar, y variante en segundo lugar. En mi caso, uso un
teclado estadounidense, así que escojo dos veces <<English (US)>>. También
dispone de una zona de entrada abajo para comprobar que el teclado funciona como
debería. Para continuar le damos a <<Continue>>.

A continuación nos preguntará por el tipo de instalación que queremos hacer
(figura \ref{fig:installation-type}). La instalación <<Minimal>> nos instalará
tan sólo aquellas cosas básicas que hacen falta para utilizar un sistema Ubuntu
con facilidad, mientras que la instalación <<Normal>> supone también la
instalación de herramientas que se usan de forma común en un sistema
informático, como sería un programa de {\em Office}. También nos permite la
instalación de actualizaciones que puedan haber desde la creación de la ISO, e
instalación de software de terceras que pueden ser útiles para ciertos
dispositivos. En nuestro caso, como tan sólo queremos especificar el proceso de
instalación, vamos a usar la instalación más básica y mínima sin actualizaciones
ni {\em software} de terceras.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/04-disk-usage.png}
		\caption{Disk usage.}
		\label{fig:disk-usage}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.47\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/05-confirm-disk-usage.png}
		\caption{Confirm disk usage.}
		\label{fig:confirm-disk-usage}
	\end{subfigure}
	\caption{Configuración de disco.}
\end{figure}

En la pantalla siguiente, nos preguntará acerca de cómo lo queremos instalar en
nuestro disco (virtual) (figura \ref{fig:disk-usage}). En nuestro caso, como no
queremos hacer nada especial, simplemente usaremos el disco entero para Ubuntu
con la opción <<Erase disk and install Ubuntu>>. Al darle a <<Install Now>> nos
preguntará si estamos de acuerdo con los cambios que se van a efectuar (figura
\ref{fig:confirm-disk-usage}) ya que borrará cualquier dato que haya en ese
disco. Le damos a <<Continue>>.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/06-timezone.png}
		\caption{Timezone.}
		\label{fig:timezone}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/07-user-config.png}
		\caption{User config.}
		\label{fig:user-config}
	\end{subfigure}
	\caption{Configuración final.}
\end{figure}

Después de este paso, Ubuntu ya está trabajando sobre la instalación en segundo
plano, pero seguirá pidiendo que configuremos algunas cosas más antes de que
veamos el progreso de esa instalación.

Lo primero que hemos de configurar es nuestra zona horaria (figura
\ref{fig:timezone}). Esto lo podemos hacer seleccionando la zona en el mapa de
manera gráfica, o escribiendo el nombre dentro de la zona de entrada que aparece
por debajo. Ambas cosas se actualizarán cuando interactuamos con la otra.

En segundo lugar nos pedirá crear un usuario y nombrar nuestro dispositivo,
dándole un {\em hostname} (figura \ref{fig:user-config}). Al acabar con este
paso, ya hemos finalizado con todo el proceso de configuración.

\subsection{Instalación}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/08-installation.png}
		\caption{Installation.}
		\label{fig:installation}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/09-install-details.png}
		\caption{Install details.}
		\label{fig:install-details}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/10-install-complete.png}
		\caption{Install complete.}
		\label{fig:install-complete}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/11-ubuntu-login.png}
		\caption{Ubuntu login.}
		\label{fig:ubuntu-login}
	\end{subfigure}
	\caption{Finalización de la instalación.}
\end{figure}

Al finalizar toda la configuración, nos mostrará el progreso actual de la
instalación (figura \ref{fig:installation}). Vemos que justo encima de la barra
de progreso hay una flecha y un texto que nos dice qué está haciendo actualmente
en la instalación. Si seleccionamos esta flecha/texto, nos mostrará los detalles
de lo que está haciendo el instalador por detrás (figura
\ref{fig:install-details}), ya que realmente el instalador simplemente está
corriendo comandos de fondo.

Una vez completada la instalación (que puede tardar bastante) nos mostrará un
diálogo para reiniciar nuestro ordenador (virtual) ya que se ha completado la
instalación (figura \ref{fig:install-complete}). Para esto, hemos de retraer el
dispositivo de instalación, mas esto lo tenemos de manera virtual, así que hemos
de apagar nuestra máquina virtual completamente. Para correrla sin el uso de la
ISO, usaremos el comando como la última vez, mas sin los argumentos de uso de
CDROM:

\begin{verbatim}
$ qemu-system-x86_64 -hda ubuntu.img -m 4G -enable-kvm
\end{verbatim}

Esto nos inicializará nuestro sistema virtual de nuevo, arrancando del disco
duro virtual en vez del CDROM virtual. Cuando termine de arrancar, veremos que
tenemos acceso a la pantalla de entrada de Ubuntu (figura
\ref{fig:ubuntu-login}).

\section{Conclusión}

Ubuntu siempre ha sido pionero en hacer que su sistema sea fácil y sencillo de
instalar y usar, sin complicaciones para que lo pueda hacer cualquiera. Esto lo
demuestra muy bien empezando nada más y nada menos que con su instalador (aunque
hay sistemas como Linux Mint que usan el mismo).

QEMU es también un virtualizador bastante potente, que nos provee más opciones
que VirtualBox, de forma completamente libre, y sin molestarnos con tantos menús
gráficos que nos pueden confundir con información que no nos interesa.

\pagebreak

\section{Derechos de Autor y Licencia}

\noindent
Copyright \copyright\ \the\year\ Nicolás A. Ortega Froysa
<nicolas@ortegas.org>  \\
\\
Este documento se distribuye bajo los términos y condiciones de la licencia
Creative Commons Attribution No Derivatives 4.0 International.

\end{document}
