La directiva de auditoría tiene como finalidad auditar el acceso a objetos, el
acceso al servicio de directorios, el cambio de directivas, el seguimiento de
procesos, el uso de privilegios, los eventos de inicio de sesión, los eventos
del sistema, y la administración de cuentas.

secpol.msc en CMD -> Directivas Locales -> Directiva de auditoría

Esto nos provee las siguientes opciones:
 - Auditar el acceso a objetos: determina si debe auditarse el evento de un
   usuario que obtiene acceso a un objeto con su propia lista de control de
   acceso del sistema (SACL) especificada.
 - Auditar el acceso del servicio de directorio: determina si debe auditarse el
   evento de un usuario que obtiene acceso a un objeto de Active Directory con
   su propia lista de control de acceso del sistema (SACL) especificada.
 - Auditar el cambio de directivas: determina si debe auditarse cada incidente
   de cambio en las directivas de asignación de derechos de usuario, directivas
   de auditoría o directivas de confianza.
 - Auditar el seguimiento de procesos: determina si debe auditarse la
   información de seguimiento detallada para eventos como activación de
   programas, salida de procesos, duplicación de identificadores y acceso a
   objetos indirectos.
 - Auditar el uso de privilegios: determina si debe auditarse cada instancia de
   un usuario que ejerce un derecho de usuario.
 - Auditar eventos de inicio de sesión: determina si debe auditarse cada
   instancia de inicio o cierre de sesión de un usuario en un equipo.

 - Auditar eventos de inicio de sesión de cuenta: determina si debe auditarse
   cada instancia de inicio o cierre de sesión de un usuario en un equipo en el
   que este equipo se usa para validar la cuenta.
 - Auditar eventos del sistema: determina si debe realizarse una auditoría
   cuando un usuario reinicia o apaga el equipo o cuando se produce un evento
   que afecta a la seguridad del sistema o al registro de seguridad.
 - Auditar la administración de cuentas: determina si debe auditarse cada evento
   de administración de cuentas en un equipo.

En Windows también tenemos acceso a un asistente para la configuración de
seguridad: el Security Configuration Wizard (SCW). Consta de una interfaz
gráfica para configurar la seguridad del sistema *en servidores*.
