\babel@toc {spanish}{}\relax 
\contentsline {section}{\numberline {1}Introducción}{3}{section.1}%
\contentsline {section}{\numberline {2}¿Cómo Funciona DHCP?}{3}{section.2}%
\contentsline {section}{\numberline {3}Configuración de Red}{4}{section.3}%
\contentsline {section}{\numberline {4}Instalación y Configuración de DHCPD}{5}{section.4}%
\contentsline {subsection}{\numberline {4.1}Instalación}{5}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Configuración}{5}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Pruebas}{8}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Habilitación e Inicio del Servicio}{8}{subsection.4.4}%
\contentsline {section}{\numberline {5}Conclusión}{9}{section.5}%
\contentsline {section}{\numberline {6}Derechos de Autor y Licencia}{10}{section.6}%
