\babel@toc {spanish}{}\relax 
\contentsline {section}{\numberline {1}Instalación de Windows Server}{3}{section.1}%
\contentsline {section}{\numberline {2}Configuración Red}{6}{section.2}%
\contentsline {section}{\numberline {3}Carpetas Compartidas}{7}{section.3}%
\contentsline {subsection}{\numberline {3.1}Configuración por Navegador de Archivos}{7}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Configuración por el Administrador de Recursos Compartidos}{9}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Creación de un Nuevo Recurso}{9}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Edición de un Recurso Existente}{11}{subsubsection.3.2.2}%
\contentsline {section}{\numberline {4}Conclusión}{13}{section.4}%
\contentsline {section}{\numberline {5}Derechos de Autor y Licencia}{14}{section.5}%
