# Introducción a SSOO
Un SO es un conjunto de programas que permite manejar el hardware del equipo.
Gestiona todos los recursos de hardware y provee servicios a los programas de
aplicación de software, ejecutándose en modo privilegiado respecto de los
restantes. Tiene entre sus objetivos la **seguridad y abstracción**.

## Seguridad
Para evitar que un programa acceda direcciones de memoria indebidas, hace uso de
un modo privilegiado para correr estas instrucciones y/o acceder a estas
direcciones.

## Abstracción
Esto ayuda a enmascarar los recursos físicos, permitiendo su manejo con
funciones más generales.

## Funciones de SSOO

 - Gestión de memoria
 - Gestión de ficheros
 - Gestión de dispositivos (E/S)
 - Gestión de procesos
 - Gestión de la red
 - Protección y seguridad

## Generaciones
 - Generación I:
  - Tubos de vacío y tarjetas perforadas
 - Generación II:
  - Sustitución de válvulas de vacío con transistores
  - Comunicación con lenguajes más avanzados
  - Surge Fortran
 - Generación III:
  - Circuitos integrados
  - Multiprogramación
  - Microchip
 - Generación IV:
  - Los primeros PC
  - Invención del microprocesador
 - Generación V:
  - Cambio a IA
 - Generación VI:
  - Computadoras basadas en redes neuronales artificiales

## Ley de Moore
Cada 2 años se duplica el número de transistores en un integrado.

## Tipos de SSOO

 - Por código:
  - Monolítico
   - Único programa dividido en subrutinas
  - Jerárquico
   - Varias capas de seguridad y privilegios que se denominan 'rings'
 - Por usuario:
  - Monousuario
  - Multiusuario
 - Por tareas:
  - Monotarea
  - Multitarea
 - Por proceso
  - Uniproceso: el SO sólo puede manejar un solo procesador
  - Multiproceso: puede manejar más de un procesador
   - Asimétrica: un procesador maestro que accede recursos de sistemas y
	 distribuye la carga
   - Simétrica: los procesos son enviados por el SO a cualquier procesador
	 disponible.

## Gestión de Procesos
Cuando se ejecuta un programa se realizan tareas de cálculo de la CPU y labores
de E/S. Cuando hay varios procesos, ambos pueden usar a la vez la E/S, pero
**sólo uno** puede usar la CPU a la vez. Esta **multiprogramación** da una
(falsa) sensación de procesos en paralelo. Distinguimos entre dos tipos de
procesos:
 - Los limitados por proceso: alto consumo CPU, bajo en E/S
 - Los limitados por E/S: bajo consumo CPU, alto en E/S

Un proceso puede tener los siguientes estados:
 - Nuevo
 - Listo, En Espera/Preparación
 - Terminado
 - Transición
 - Ejecución/Activo
 - Bloqueado

### Apropiación
 - **Planificación no apropiativa:** deja ejecutar al proceso en CPU hasta que
   finalice, se bloquee, espere por otro proceso o termine de forma voluntaria.
   Es decir, una vez que la CPU se le asigna ésta ya no se le puede quitar.
  - **First Come First Serve (FCFS)**
  - **Shortest Job First (SJF)**
 - **Planificación apropiativa:** El planificador puede desalojar al proceso en
   CPU durante su ejecución y cambiarlo por otro.
  - **Preemptive Shortest Process Next (PSPN)**
  - **Prioridades**
  - **Turno Rotatorio (Round-Robin)**
  - **Multi-Level Queue**

## Gestión de Memoria
Cuando un sistema es monoprograma, entonces el único proceso tiene acceso pleno
a toda la memoria de la máquina. Mas cuando hay varios programas, tiene que
haber un sistema para asignar la memoria a los diferentes procesos.

El sistema operativo también se tiene que ocupar de no dejar que un proceso
acceda a la memoria de otro - y por supuesto no del sistema operativo. Esto se
soluciona sobre todo con la memoria virtual, tal que el proceso cree que tiene
toda la memoria (incluso más grande que el tamaño físico de la memoria), y la
CPU se ocupa de traducir las direcciones. Cuando se intenta usar más memoria de
lo que hay en memoria física, se hace uso de un almacenamiento secundario que se
llama __swap__.

Para gestionar la memoria se suele usar una estructura lógica de __páginas__. Si
es demasiado pequeña entonces sobrecarga el sistema al tener que gestionar
tantas. Mas si es demasiado grande se desaprovecha espacio.

**Fichero:** Unidad lógica de almacenamiento de información.

 - Asignación Contigua (continuous blocks)
 - Asignación Enlazada (linked list)
 - Asignación Indexada (bitmap)
