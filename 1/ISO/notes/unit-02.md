# Unidad 2: Proceso de Arranque de un SO

Todo hardware necesita de un software que le indique las acciones a realizar. Al
arrancar un equipo necesitamos medios especiales para que se cargue un primer
software. Al arrancar, se hace un **Power-On Self-Test** (POST) que verifica e
inicializa los componentes de E/S, esto configura y diagnostica el estado del
hardware.

El Basic Input Output System (BIOS) es el sistema encargado de dirigir y
gestionar el arranque. En los sistemas más viejos, suenan tonos para hacer notar
qué error hay, y el número de tonos y su longitud denota qué fallo ha ocurrido.

Secuencia de arranque:
 1. Establecimiento de una tensión estabilizada:
  i. __Power Good:__ Se estabiliza la fuente y manda una señal a la placa base
  de que la tensión es correcta para continuar.
  ii. __Reset:__ Cuando el chipset de la placa base recibe la señal anterior,
  manda una señal de reinicio (reset) al procesador.
 2. Arranque BIOS/UEFI
 3. POST
 4. Búsqueda del sector de arranque (o MBR)
 5. Cargar el bootmgr y ceder el control al S.O.
 6. Carga del S.O.
