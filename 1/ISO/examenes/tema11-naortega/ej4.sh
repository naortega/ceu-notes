#!/bin/bash

function palindromo {
	WORD=$1
	REV=$(echo "$WORD" | rev)
	if [ $WORD = $REV ]
	then
		echo "Es capicúa."
	else
		echo "No es capicúa."
	fi
}

NUMERO=$1

if [ $NUMERO -lt 1000 ] || [ ${#NUMERO} -ne 5 ]
then
	echo "Se necesita un número mayor que 1000 y con 5 cifras"
	exit 1
fi

HALF=$(echo "scale=3; $NUMERO / 2" | bc)
echo $HALF

palindromo $NUMERO
