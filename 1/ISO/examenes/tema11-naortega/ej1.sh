#!/bin/bash

if [ $# -lt 3 ]
then
	echo "Son necesarias al menos 3 argumentos numéricos."
	exit 1
fi

# a
echo "Se han introducido $# argumentos."

# b
SUM=0

for i in ${@:1:3}
do
	SUM=$[$SUM+$i]
done

AVG=$(echo "scale=3; $SUM / 3" | bc)
echo $AVG

# c
echo $@

# d
echo $[$2 ** $3]
