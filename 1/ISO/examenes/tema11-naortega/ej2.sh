#!/bin/bash

# crear estructura:
mkdir -p /home/usuario/prueba/dir1/dir11
mkdir -p /home/usuario/prueba/dir2/dir41
mkdir -p /home/usuario/prueba/dir3/dir31
mkdir -p /home/usuario/prueba/dir4

# número de usuarios conectados
who | wc -l

# cuándo arrancó
uptime --pretty
