#!/bin/bash

function impar {
	LIM=$1
	seq 1 2 $LIM
}

if [ ${#1} -ne 3 ]
then
	echo "Se requiere un número de 3 cifras."
	exit 1
fi

impar $1
