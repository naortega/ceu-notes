#!/bin/bash

if [ $# -lt 3 ]
then
	echo "Se precisan al menos 3 argumentos."
	exit 1
fi

for i in $@; do echo "$i"; done

