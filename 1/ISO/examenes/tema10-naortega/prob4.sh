#!/bin/bash

# a)
ls /dev/tty??
# b)
ls /etc/[^t]*
# c)
ps | sort
# d)
wc -l /etc/passwd
# e)
ln -s /usr/dir1 /home/usuario/enlacedir1
# f)
cp -r /bin/u* /home/usuario/enlacedir1/
# g)
chmod a-w /home/usuario
# h)
cat /etc/motd

