# Los Sindicatos

## Funciones de un Sindicato

- Comunicación constante entre trabajadores y empresarios
- Estudio de las condiciones de trabajo dentro de la empresa
- Propuestas de mejora laboral
- Defensa de los intereses de los trabajadores representados
- Realización y firma de contratos colectivos

## Organización

Los sindicatos se pueden organizar en torno a dos criterios: __sector__ y
__ámbito territorial__.

El sector se refiere al sector económico en el que el sindicato representa a los
trabajadores.

El ámbito territorial se refiere al nivel en que el sindcato tiene
representación. Éste se puede clasificar en uno de los siguientes:

- Estatal
- Autonómico
- Provincial
- Inter-Provincial
- Regional
- Local
- De Empresa
- De Centro de Trabajo

## Representación

Se pueden clasificar los sindicatos según su representación en dos categorías:
más representativos y de representación ordinaria.

Ley Orgánica de Libertad Sindical artículo 6.2 sobre sindicatos más
representativos a nivel nacional:

> \2. Tendrán la consideración de sindicatos más representativos a nivel
> estatal:
>
> a) Los que acrediten una especial audiencia, expresada en la obtención, en
> dicho ámbito del 10 por 100 o más del total de delegados de personal de los
> miembros de los comités de empresa y de los correspondientes órganos de las
> Administraciones públicas.
>
> b) Los sindicatos o entes sindicales, afiliados, federados o confederados a
> una organización sindical de ámbito estatal que tenga la consideración de
> más representativa de acuerdo con lo previsto en la letra a).

Ley Orgánica de Libertad Sindical artículo 7 sobre sindicatos más
representativos a nivel autonómico:

> a) Los sindicatos de dicho ámbito que acrediten en el mismo una especial
> audiencia expresada en la obtención de, al menos, el 15 por 100 de los
> delegados de personal y de los representantes de los trabajadores en los
> comités de empresa, y en los órganos correspondientes de las
> Administraciones públicas, siempre que cuenten con un mínimo de 1.500
> representantes y no estén federados o confederados con organizaciones
> sindicales de ámbito estatal; b) los sindicatos o entes sindicales
> afiliados, federados o confederados a una organización sindical de ámbito de
> Comunidad Autónoma que tenga la consideración de más representativa de
> acuerdo con lo previsto en la letra a).

Aquellos sindicatos que, en un sector específico, hayan conseguido al menos el
10% de la representación podrán participar de manera colectiva en dicho sector
en funciones específicas. Éstos los podemos denominar de representación
ordinaria.
