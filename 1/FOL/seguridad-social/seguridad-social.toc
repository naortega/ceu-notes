\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Introducción}{3}{section.1}%
\contentsline {subsection}{\numberline {1.1}Estructura}{3}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}La Gestión del Sistema de la Seguridad Social}{4}{subsection.1.2}%
\contentsline {section}{\numberline {2}Obligaciones con la Seguridad Social del Empresario y los Trabajadores}{4}{section.2}%
\contentsline {section}{\numberline {3}Prestaciones de la Seguridad Social}{5}{section.3}%
\contentsline {subsection}{\numberline {3.1}Asistencia Sanitaria y Farmacéutica}{5}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Prestación por Nacimiento y Cuidado del Menor}{6}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Riesgo Durante el Embarazo o la Lactancia}{6}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Incapacidad Temporal}{6}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Incapacidad Permanente}{7}{subsection.3.5}%
\contentsline {subsection}{\numberline {3.6}Jubilación}{8}{subsection.3.6}%
\contentsline {subsection}{\numberline {3.7}Muerte o Supervivencia}{8}{subsection.3.7}%
\contentsline {subsubsection}{\numberline {3.7.1}Pensión de Viudedad}{8}{subsubsection.3.7.1}%
\contentsline {subsubsection}{\numberline {3.7.2}Pensión de Orfandad}{9}{subsubsection.3.7.2}%
\contentsline {section}{\numberline {4}Derechos de Autor y Licencia}{10}{section.4}%
