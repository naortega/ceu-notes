# Seguridad Social

Niveles de protección de la Seguridad Social:

- Contributivo: Financiado con aportaciones de empresarios y trabajadores.
- No Contributivo: Financiado a cargo de los Presupuestos Generales del Estado.

Regímenes que integran la seguridad social:

- Régimen General: Todos los trabajadores por cuenta ajena, que trabajen en
  territorio nacional, y no están incluidos en régimen especial.
- Régimenes Especiales: Autónomos, trabajadores de alta mar, mineros, y
  estudiantes.

Obligaciones de la empresa con la Seguridad Social:

- Afiliación: Sólo se realiza una vez en la vida; se debe proceder a ella en el
  primer contrato si el trabajador no estaba afiliado con anterioridad
- Altas: Las presentará el empresario con anterioridad a la incorporación del
  trabajador en la empresa.
- Bajas: El trabajador debe ser dado de baja en el plazo de tres días naturales
  desde que cesa el trabajo.
- Cotización: El empresario debe cotizar por las cuotas que a él le corresponden
  y por la cuota obrera, ingresando mensualmente ambas en la TGSS.

Presetaciones de la Seguridad Social:

- Prestaciones en especie: asistencia sanitaria, asistencia farmacéutica,
  prótesis quirúrgicas ortopédicas y vehículos para inválidos.
- Prestaciones económicas:
  - Subsidios: pago periódico y temporal
  - Pensiones: pago periódico y duración vitalicia
  - Indemnizaciones: abonan una sola vez
  - Otras: por desempleo, protección de carácter no económico
  - No contributivas
