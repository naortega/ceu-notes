# Tema 11: Búsqueda Activa de Empleo
## Planificación de la Carrera Laboral
 - Estudio del mercado laboral
 - Autoanálisis
 - Diseño de un plan de acción
 - Mejora de conocimientos
 - Adquisición de competencias
 - Revisión del plan

## Toma de Decisiones
 - Trabajo por cuenta ajena
 - Autónomo por cuenta propia
 - Oposición a funcionario público
 - Continuar con la formación
 - Combo de los anteriores

## Búsqueda de Empleo por Cuenta Ajena
 1. Planificación de la carrera profesional
  i. Autoanálisis
   a. Autoconocimiento
   b. Inventario personal y profesional
   c. Puntos fuertes y débiles
   d. Mejora de capacidades
  ii. Definición del objetivo profesional
   a. Tipo de empresa deseada
   b. Tipo de trabajo deseado
   c. Formación competencias y experiencia propias
   d. Objetivos laborales a medio plazo
  iii. Estudio del mercado laboral
   a. Vigilancia competitiva
   b. Benchmarking
  iv. Diseño de un plan de acción
  v. Revisión permanente del plan
 2. Búsqueda de ofertas de empleo
  i. Portales de empleo
  ii. Sistema nacional de empleo
  iii. ETTs (Trabajo Temporal)
  iv. Agencias de colocación
  v. Consultoras de selección
  vi. Networking
  vii. FCT (prácticas en empresa)
 3. Elaboración del CV y la carta de presentación
 4. Pruebas psicométricas
 5. Primeras entrevistas
 6. Segunda entrevista
 7. Fase final

## Oportunidades de Empleo Exterior
 - Europass
  - CV internacional
  - Pasaporte de lenguas
  - suplemento al título
 - Ploteus
  - Oportunidades de formación
  - Descripción de sistemas educativos
  - Información de intercambios y becas
 - Eures
  - Vacantes de empleo
  - Registro como demandante
  - Información sobre condiciones laborales
  - Asesoramiento

## Curriculum Vitae
 - CV Cronológico
 - CV Inverso (más profesional)
 - CV Funcional o temático
 - CV Mixto

Secciones:
 - Datos personales
 - Formación académica
 - Experiencia profesional
 - Formación complementaria:
  - Cursos extras
  - Conocimientos informáticos
  - Idiomas

## Carta de Presentación
Existen dos tipos de cartas de presentación:
 - Carta que responde a una oferta de empleo concreta
 - Carta como candidatura espontánea

Composición:
 1. Presentación
 2. Adecuación al puesto de trabajo
 3. Entrevista
 4. Despedida

## Pruebas Psicométricas
 - Test Psicotécnico:
  - Inteligencia
  - Aptitudes (verbal, numérica, administrativa)
  - Razonamiento (abstracto, espacial)
 - Test de personalidad:
  - Examinan rasgos de personalidad y conductas
 - Pruebas Profesionales:
  - Relacionadas con el puesto de trabajo
 - Test de Cultura General
  - Evalúan conocimientos básicos de historia, matemáticas, arte, etc.

## Dinámicas de Grupo
Consejos:
 - Evitar asumir un rol (e.g. líder)
 - Evita comportamientos que creas esperados por los evaluadores
 - No monopolices la conversación
 - Administra tu tiempo
 - Busca el terminar con una conclusión final
