SET SERVEROUTPUT ON;

CREATE OR REPLACE PROCEDURE insertarDepartamento(
    deptName dept.dname%type, deptLocale dept.loc%type, message OUT VARCHAR)
IS
    items INT;
    nextId dept.deptno%type;
BEGIN
    SELECT COUNT(*) INTO items FROM dept WHERE dname=deptName;
    IF items > 0 THEN
        message := 'El departamento no se ha creado, ya existe.';
    ELSE
        SELECT MAX(deptno) INTO nextId FROM dept;
        nextId := nextId + 10;
        INSERT INTO dept(deptno, dname, loc) VALUES(nextId, deptName, deptLocale);
        message := 'Se ha insertado el nuevo departamento.';
    END IF;
END;
/

DECLARE
    msg VARCHAR(128);
    items INT;
BEGIN
    insertarDepartamento('MARKETING', 'DETROIT', msg);
    dbms_output.put_line(msg);
    insertarDepartamento('QUALITY', 'LOS ANGELES', msg);
    dbms_output.put_line(msg);
END;
/
