SET SERVEROUTPUT ON;

CREATE OR REPLACE FUNCTION obtenerTrabajadoresJob(empJob emp.job%type) RETURN INT
IS
    CURSOR jobEmps IS SELECT empno, ename FROM emp WHERE job=empJob;
    counter INT := 0;
    empId emp.empno%type;
    empName emp.ename%type;
BEGIN
    OPEN jobEmps;
    LOOP
        FETCH jobEmps INTO empId, empName;
        EXIT WHEN jobEmps%notfound;
        dbms_output.put_line(empId || ' ' || empName);
        counter := counter + 1;
    END LOOP;
    CLOSE jobEmps;
    IF counter = 0 THEN
        RAISE NO_DATA_FOUND;
    END IF;
    RETURN counter;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        dbms_output.put_line('No existe ese valor para puesto en la tabla EMP.');
        RETURN 0;
END;
/

DECLARE
    cnt INT;
BEGIN
    cnt := obtenerTrabajadoresJob('MANAGER');
    dbms_output.put_line(cnt);
   
    cnt := obtenerTrabajadoresJob('SYSTEM');
    dbms_output.put_line(cnt);
END;
/
