SET SERVEROUTPUT ON;

CREATE OR REPLACE FUNCTION calculoAreas(shape VARCHAR, nums VARCHAR) RETURN INT
IS
    figuraNoValida EXCEPTION;
    width INT := 1;
    height INT := 1;
    res INT := -1;
BEGIN
    IF shape = 'triángulo' THEN
        width := TO_NUMBER(SUBSTR(nums, 1, INSTR(nums, ',') - 1));
        height := TO_NUMBER(SUBSTR(nums, INSTR(nums, ',') + 1));
        res := width * height / 2;
    ELSIF shape = 'cuadrado' THEN
        width := TO_NUMBER(nums);
        res := nums * nums;
    ELSIF shape = 'círculo' THEN
        width := TO_NUMBER(nums);
        res := 3.1415 * width * width;
    ELSE
        RAISE figuraNoValida;
    END IF;
   
    RETURN res;
EXCEPTION
    WHEN figuraNoValida THEN
        dbms_output.put_line('Nombre de figura no válida, debes usar sólo triángulo, cuadrado, o círculo.');
        RETURN -1;
END;
/

DECLARE
    res INT;
BEGIN
    res := calculoAreas('triángulo', '4,2');
    dbms_output.put_line('Triángulo de b=4 y a=2: ' || res);
    res := calculoAreas('círculo', '3');
    dbms_output.put_line('Círculo de r=3: ' || res);
END;
/
