-- Autor: Nicolás Andrés Ortega Froysa
-- GBD

-- 1
SELECT UPPER(REPLACE(nombre, ' ', '')), localidad FROM clientes;

-- 2
SELECT ROUND(AVG(precio), 2) FROM platos;

-- 3
/*
 * Si hay varios clientes y sólo quieres uno, puedes también
 * añadir al final `LIMIT 1`
 */
SELECT nombre, telefono FROM clientes WHERE localidad="GINES";

-- 4
SELECT i.nombre, ip.cantidad FROM platos p JOIN ingredientes_platos ip ON p.id=ip.id_plato JOIN ingredientes i ON ip.id_ingrediente=i.id WHERE p.nombre='Entrante bacalao en aceite';

-- 5
SELECT i.nombre FROM platos p JOIN ingredientes_platos ip ON p.id=ip.id_plato JOIN ingredientes i ON ip.id_ingrediente=i.id WHERE p.nombre='Tarta de queso';

-- 6
SELECT nombre, categoria FROM ingredientes WHERE nombre LIKE 'Pi%';

-- 7
SELECT COUNT(*) FROM ingredientes WHERE categoria='LACTEO';

-- 8
SELECT ROUND(p.precio*COUNT(*), 2) FROM comandas c LEFT JOIN platos p ON c.id_plato=p.id WHERE p.id=5;

-- 9
SELECT AVG(cantidad) FROM ingredientes_platos;

-- 10
SELECT p.nombre FROM platos p JOIN ingredientes_platos ip ON p.id=ip.id_plato JOIN ingredientes i ON ip.id_ingrediente=i.id WHERE i.nombre='Aceite de oliva virgen extra';

-- 11
SELECT COUNT(*) FROM comandas WHERE estado='SERVIDO' AND fecha='02-03-2021';

-- 12
SELECT p.nombre FROM comandas com JOIN platos p ON com.id_plato=p.id WHERE estado='DEVUELTO';

-- 13
SELECT SYSDATE - fecha FROM comandas ORDER BY fecha DESC LIMIT 1;
