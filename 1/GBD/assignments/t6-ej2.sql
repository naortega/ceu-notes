SET SERVEROUTPUT ON;

-- ej1
DECLARE
    empl EMP%ROWTYPE;
    TYPE IntList IS TABLE OF INT INDEX BY BINARY_INTEGER;
    arr IntList;
    i INT;
BEGIN
    i := 1;
    arr(1) := 7839;
    arr(2) := 7698;
    arr(3) := 7782;
    WHILE i <= arr.count()
    LOOP
        SELECT * INTO empl FROM emp WHERE empno = arr(i);
        dbms_output.put_line(empl.ename);
        i := i + 1;
    END LOOP;
END;
/

-- ej2
DECLARE
    TYPE EMPL IS RECORD(
        codigo NUMBER(4),
        nombre VARCHAR2(10),
        trabajo VARCHAR2(9));
    TYPE EMPLLIST IS TABLE OF EMPL INDEX BY BINARY_INTEGER;
    empls EMPLLIST;
BEGIN
    SELECT empno, ename, job INTO empls(1) FROM emp WHERE empno=7839;
    dbms_output.put_line('Código: ' || empls(1).codigo);
    dbms_output.put_line('Nombre: ' || empls(1).nombre);
    dbms_output.put_line('Trabajo: ' || empls(1).trabajo);
END;
/