-- Nicolás A. Ortega Froysa
-- ASIR 1
-- GBD

-- Actividad 1
CREATE TABLE Mascotas(
	id INT PRIMARY KEY,
	nombre VARCHAR(32),
	especie VARCHAR(32),
	raza VARCHAR(32),
	edad INT,
	-- [M]acho, [H]embra, u [O]tro (en el caso de especies asexuales)
	sexo CHAR(1) CHECK(sexo IN('M', 'H', 'O'))
);

INSERT INTO Mascotas VALUES(1, "Fofita", "Gato", "Siames", 5, 'H');
INSERT INTO Mascotas VALUES(2, "Domingo", "Perro", "Perro de Agua", 7, 'M');
INSERT INTO Mascotas VALUES(3, "Estufito", "Dragon", "Europeo", 564, 'M');
INSERT INTO Mascotas VALUES(4, "MiguelB", "Pez", "Pez Dorada", 1, 'O');
INSERT INTO Mascotas VALUES(5, "Dani", "Perro", "Pastor Aleman", 11, 'M');
INSERT INTO Mascotas VALUES(6, "Agustin", "Cerdo", "Blanco", 9, 'M');

SELECT nombre, especie FROM Mascotas;
SELECT nombre "NAME", sexo "GENDER" FROM Mascotas;
SELECT nombre, DATEADD(year, -edad, SYSDATE) "ANHO NACIMIENTO" FROM MASCOTAS;

-- Actividad 2
ALTER TABLE Mascotas ADD pedo INT;
UPDATE Mascotas SET peso=30 WHERE id=1;
UPDATE Mascotas SET peso=50 WHERE id=2;
UPDATE Mascotas SET peso=1200 WHERE id=3;
UPDATE Mascotas SET peso=1 WHERE id=4;
UPDATE Mascotas SET peso=60 WHERE id=5;
UPDATE Mascotas SET peso=110 WHERE id=6;

SELECT nombre FROM Mascotas WHERE peso BETWEEN 1 AND 3;

-- Activdad 3
CREATE TABLE Proveedores(
	id_proveedor INT PRIMARY KEY,
	nombre_comercial VARCHAR(32) NOT NULL,
	direccion VARCHAR(64),
	email_contacto VARCHAR(64),
	tfno_contacto VARCHAR(32),
);
CREATE TABLE Productos(
	id INT PRIMARY KEY,
	nombre VARCHAR(32) NOT NULL,
	composicion VARCHAR(32) NOT NULL,
	id_proveedor INT FOREIGN KEY id_proveedor REFERENCES Proveedores(id_proveedor),
	precio number(5,2)
);
CREATE TABLE Clientes(
	id_cliente INT PRIMARY KEY,
	nombre_cliente VARCHAR(64) NOT NULL,
	forma_pago VARCHAR(32) CHECK(forma_pago IN ("METALICO", "TARJETA"))
);
CREATE TABLE Compras(
	ref_compra INT PRIMARY KEY,
	id_cliente INT FOREIGN KEY id_cliente REFERENCES Clientes(id_cliente),
	id_producto INT FOREIGN KEY id_producto REFERENCES Productos(id),
	estado VARCHAR(16) CHECK(estado IN ("SERVIDO", "RESERVADO", "PEDIDO A ALMACEN")),
	fecha DATE
);

INSERT INTO Proveedores VALUES(1, "Prov A", "C. AAA, 1", "contacto@a.com", "+34 111111111");
INSERT INTO Proveedores VALUES(2, "Prov B", "C. BBB, 2", "contacto@b.com", "+34 222222222");

INSERT INTO Productos VALUES(1, "Prod A1", "Muchos A", 1, 6.74);
INSERT INTO Productos VALUES(2, "Prod A2", "Muchisimos A", 1, 100.99);
INSERT INTO Productos VALUES(3, "Prod B1", "Muchos B", 2, 6.74);
INSERT INTO Productos VALUES(4, "Prod B2", "Muchisimos B", 2, 100.99);

INSERT INTO Clientes VALUES(1, "A", "METALICO");
INSERT INTO Clientes VALUES(2, "B", "TARJETA");

INSERT INTO Compras VALUES(1, 1, 1, "PEDIDO A ALMACEN", SYSDATE-4);
INSERT INTO Compras VALUES(2, 2, 3, "SERVIDO", SYSDATE-1);
UPDATE Compras SET estado="SERVIDO" WHERE ref_compra=1;
INSERT INTO Compras VALUES(3, 1, 4, "RESERVADO", SYSDATE);
UPDATE Clientes SET forma_pago="TARJETA" WHERE id_cliente=1;
DELETE FROM Compras WHERE id_cliente=2;

SELECT prod.id, prod.nombre, prod.composicion, prov.nombre_comercial, TRUNC(precio, 0)
	FROM Productos prod
	JOIN Proveedores prov ON prod.id_proveedor=prov.id_proveedor;
SELECT nombre, CEIL(precio) "Producto redondeado superior" FROM Productos;
SELECT * FROM Proveedores WHERE id=1;
SELECT nombre FROM Clientes WHERE forma_pago="TARJETA";
SELECT COUNT(*) FROM Compras WHERE estado="SERVIDO";
