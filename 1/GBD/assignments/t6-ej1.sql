SET SERVEROUTPUT ON;

-- ej1
DECLARE
	a INT := &primer_numero;
	b INT := &segundo_numero;
BEGIN
	IF a < b THEN
		dbms_output.put_line('ERROR: a > b');
	ELSIF a MOD b = 0 THEN
		dbms_output.put_line('Son divisibles.');
	ELSE
		dbms_output.put_line('No son divisibles.');
	END IF;
END;
/

-- ej2
BEGIN
	dbms_output.put_line(TO_CHAR(SYSDATE, 'Day'));
END;
/

-- ej3
DECLARE
	a INT := &primer_numero;
	b INT := &segundo_numero;
	c INT := &tercer_numero;
	suma INT;
BEGIN
	IF a < 0 OR b < 0 OR c < 0 THEN
		dbms_output.put_line('Número negativo no aceptable.');
	ELSE
		suma := a + b + c;
		dbms_output.put_line(suma);
	END IF;
END;
/

-- ej4
DECLARE
	salario INT := &tu_salario;
BEGIN
	IF salario <= 0 THEN
		dbms_output.put_line('Salario no válido.');
	ELSIF salario < 20000 THEN
		dbms_output.put_line('Tu salario es bajo.');
	ELSIF salario < 40000 THEN
		dbms_output.put_line('Tu salario está bien.');
	ELSIF salario >= 40000 THEN
		dbms_output.put_line('Tu salario está muy bien!');
	END IF;
END;
/
