DROP TABLE Barcos;
DROP TABLE Especies;
DROP TABLE Lotes;

CREATE TABLE Barcos(
	matricula CHAR(7) PRIMARY KEY,
	nombre VARCHAR(20),
	clase VARCHAR(20),
	capacidad NUMBER(8,2),
	nacionalidad VARCHAR(20)
);

CREATE TABLE Lotes(
	codigo INT PRIMARY KEY,
	matricula CHAR(7), /* references Barcos(matricula) */
	num_kg NUMBER(8,2) CHECK(num_kg > 0),
	precio_kg_salida NUMBER(8,2) CHECK(precio_kg_salida > 0),
	precio_kg_vendido NUMBER(8,2) CHECK(precio_kg_vendido > 0),
	fecha_venta DATE NOT NULL,
	cod_especie INT /* references Especies(codigo) */
);

CREATE TABLE Especies(
	codigo INT PRIMARY KEY,
	nombre VARCHAR(20),
	tipo VARCHAR(20),
	cupo_barco NUMBER(8,2),
	caladero_ppal INT /* references Caladeros(codigo) */
);

CREATE TABLE Caladeros(
	codigo INT PRIMARY KEY,
	nombre VARCHAR(20) CHECK(nombre = UPPER(nombre)),
	ubicacion VARCHAR(20) CHECK(ubicacion = UPPER(ubicacion)),
	especie_ppal INT /* references Especies(codigo) */
);

CREATE TABLE Fechas_Capturas(
	cod_especie INT, /* references Especies(codigo) */
	cod_caladero INT, /* references Caladeros(codigo) */
	fecha_inicio DATE,
	fecha_fin DATE,
	PRIMARY KEY(cod_especie, cod_caladero)
);

ALTER TABLE Lotes
	ADD CONSTRAINT FK_Lotes_matricula
	FOREIGN KEY (matricula) REFERENCES Barcos(matricula)
	ON DELETE CASCADE;

ALTER TABLE Lotes
	ADD CONSTRAINT FK_Lotes_cod_especie
	FOREIGN KEY (cod_especie) REFERENCES Especies(codigo)
	ON DELETE CASCADE;

ALTER TABLE Especies
	ADD CONSTRAINT FK_Especies_caladero_ppal
	FOREIGN KEY (caladero_ppal) REFERENCES Caladeros(codigo)
	ON DELETE RESTRICT;

ALTER TABLE Caladeros
	ADD CONSTRAINT FK_Caladeros_especie_ppal
	FOREIGN KEY (especie_ppal) REFERENCES Especies(codigo)
	ON DELETE SET NULL;

ALTER TABLE Fechas_Capturas
	ADD CONSTRAINT FK_Fechas_Capturas_cod_especie
	FOREIGN KEY (cod_especie) REFERENCES Especies(codigo);

ALTER TABLE Fechas_Capturas
	ADD CONSTRAINT FK_Fechas_Capturas_cod_caladero
	FOREIGN KEY (cod_caladero) REFERENCES Caladeros(codigo);

ALTER TABLE Barcos
	ADD CHECK(REGEXP_LIKE(matricula, '[A-Z]{2}-[0-9]{4}'));

ALTER TABLE Lotes
	ADD CHECK(precio_kg_vendido > precio_kg_salida);

ALTER TABLE Fechas_Capturas
	ADD CHECK(fecha_inicio BETWEEN '02-02-2022' AND '28-03-2022');
