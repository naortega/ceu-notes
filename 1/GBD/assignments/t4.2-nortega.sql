-- Nicolás A. Ortega Froysa
-- ASIR 1
-- GBD

CREATE TABLE fabricantes(
	codigo INT PRIMARY KEY,
	nombre VARCHAR(100)
);

CREATE TABLE articulos(
	codigo INT PRIMARY KEY,
	nombre VARCHAR(100),
	precio INT,
	fabricante INT,
	FOREIGN KEY (fabricante) REFERENCES fabricantes(codigo)
);

INSERT INTO fabricantes VALUES(1, 'LG');
INSERT INTO fabricantes VALUES(2, 'SAMSUNG');
INSERT INTO fabricantes VALUES(3, 'SIEMENS');
INSERT INTO fabricantes VALUES(4, 'SONY');

INSERT INTO articulos VALUES(1, "RADIO", 180, 1);
INSERT INTO articulos VALUES(2, "RADIO", 100, 2);
INSERT INTO articulos VALUES(3, "RADIO", 70, 3);
INSERT INTO articulos VALUES(4, "RADIO", 50, 4);
INSERT INTO articulos VALUES(5, "TELEVISION", 320, 1);
INSERT INTO articulos VALUES(6, "TELEVISION", 850, 2);
INSERT INTO articulos VALUES(7, "TELEVISION", 600, 3);
INSERT INTO articulos VALUES(8, "TELEVISION", 170, 4);

-- 1.1
SELECT DISTINCT nombre FROM articulos;

-- 1.2
SELECT nombre, precio FROM articulos;

-- 1.3
SELECT nombre FROM articulos WHERE precio <= 200;

-- 1.4
SELECT * FROM articulos WHERE precio >= 60 AND precio <= 120;
SELECT * FROM articulos WHERE precio BETWEEN 60 AND 120;

-- 1.5
SELECT nombre, precio * 166386 "Precio Pesetas" FROM articulos;

-- 1.6
SELECT AVG(precio) FROM articulos;

-- 1.7
SELECT AVG(precio) FROM articulos WHERE fabricante=2;

-- 1.8
SELECT COUNT(*) FROM articulos WHERE precio >= 180;

-- 1.9
SELECT nombre, precio FROM articulos WHERE precio >= 180 ORDER BY precio DESC, nombre ASC;

-- 1.10
SELECT * FROM articulos a JOIN fabricantes f ON a.fabricante=f.codigo;

-- 1.11
SELECT a.nombre, a.precio, f.nombre FROM articulos a JOIN fabricantes f ON a.fabricante=f.codigo;

-- 1.12
SELECT nombre, precio FROM articulos ORDER BY precio ASC LIMIT 1;

-- 1.13
INSERT INTO articulos VALUES(9, 'ALTAVOCES', 70, 2);

-- 1.14
UPDATE articulos SET nombre='IMPRESORA LASER' WHERE codigo=8;

-- 1.15
UPDATE articulos SET precio=precio*0.9;

-- 1.16
UPDATE articulos SET precio=precio-10 WHERE precio >= 120;
