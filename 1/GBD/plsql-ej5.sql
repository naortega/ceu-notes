DECLARE
	nombre VARCHAR(50) := &MI_NOMBRE;
	apellido VARCHAR(100) := &MIS_APELLIDOS;
BEGIN
	dbms_output.put_line('Hola' || nombre || ' ' || apellido);
END;
/

DECLARE
	num1 INT := 8;
	num2 INT := 4;
BEGIN
	suma INT := num1 + num2;
	dbms_output.put_line(suma);
	resta INT := num1 - num2;
	dbms_output.put_line(resta);
	mult INT := num1 * num2;
	dbms_output.put_line(mult);
	div INT := num1 / num2;
	dbms_output.put_line(div);
END;
/
