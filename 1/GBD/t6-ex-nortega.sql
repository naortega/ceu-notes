SET SERVEROUTPUT ON;

-- ej1

/* Declara una variable departamento que tenga como tipo de dato una fila
 * completa de la tabla dept.
 *
 * Después, mediante un cursor implícito, guarda en esa variable departamento la
 * fila de la tabla dept cuyo campo loc vale CHICAGO. Para terminar, utiliza un
 * bucle FOR que recorra todas las letras del nombre (dname) de la fila guardada
 * en la variable departamento y vaya mostrando en cada iteración, cada letra
 * de dname.
 */

DECLARE
    departamento dept%ROWTYPE;
BEGIN
    SELECT * INTO departamento FROM dept WHERE loc='CHICAGO';
    FOR i IN 1..LENGTH(departamento.dname)
    LOOP
        dbms_output.put_line(SUBSTR(departamento.dname,i,1));
    END LOOP;
END;
/

-- ej2

/*
 * Crea un tipo de dato registro con los campos id (tipo de dato igual a deptno de
 * la tabla dept) y nombre (tipo de dato igual a dname de la tabla dept) y declara
 * una variable departamento del tipo de ese registro. Ahora usa un cursor
 * implícito para guardar en esa variable departamento el deptno y el dname de la
 * fila de la tabla dept cuyo deptno vale 10. Para terminar, muestra por pantalla
 * el id y el nombre de la variable departamento obtenido.
 */

DECLARE
    TYPE DEPT_DATA IS RECORD(
        id dept.deptno%type,
        nombre dept.dname%type);
    departamento DEPT_DATA;
BEGIN
    SELECT deptno, dname INTO departamento
        FROM dept WHERE deptno=10;
    dbms_output.put_line(departamento.id || ' ' ||
        departamento.nombre);
END;
/

-- ej3

/*
 * Se necesita un registro para los empleados y otro para las localidades.
 *
 * El registro de empleados debe tener los siguientes campos: id (tipo de dato
 * igual que el campo empno de la tabla emp), nombre (tipo de dato igual que el
 * campo ename de la tabla emp), sueldo (tipo de dato igual que el campo sal de la
 * tabla emp), localidad (tipo de dato igual que el registro de localidades).
 *
 * El registro de localidades debe tener los siguientes campos: ref (varchar
 * tamaño 3 con valor MAI), localidad varchar tamaño 50 con valor MAIRENA,
 * provincia (varchar tamaño 50 con valor SEVILLA) y cp (int tamaño 5 con valor
 * 41927).
 *
 * Se quiere declarar una variable del tipo registro empleado y un cursor
 * explícito que se traiga los campos empno, ename y sal de la tabla emp.
 *
 * Recorre todas las filas del cursor declarado con un bucle LOOP, guardando los
 * datos extraídos en la variable del tipo registro empleado.
 *
 * En cada iteración del bucle LOOP muestra por pantalla id, nombre, sueldo, ref
 * de localidad, localidad de localidad, provincia de localidad y cp de
 * localidad.
 */

DECLARE
    TYPE LOC_DATA IS RECORD(
        ref VARCHAR(3) := 'MAI',
        localidad VARCHAR(50) := 'MAIRENA',
        provincia VARCHAR(50) := 'SEVILLA',
        cp INT(5) := 41927);
    TYPE EMP_DATA IS RECORD(
        id emp.empno%type,
        nombre emp.ename%type,
        sueldo emp.sal%type,
        loc LOC_DATA);
    empl EMP_DATA;
    CURSOR empls IS SELECT empno, ename, sal FROM emp;
BEGIN
    OPEN empls;
    LOOP
        FETCH empls INTO empl.id, empl.nombre, empl.sueldo;
        EXIT WHEN empls%notfound;
        dbms_output.put_line('-----');
        dbms_output.put_line('Empleado número ' || empl.id);
        dbms_output.put_line('Nombre: ' || empl.nombre);
        dbms_output.put_line('Sueldo: ' || empl.sueldo);
        dbms_output.put_line('Referencia de localidad: ' || empl.loc.ref);
        dbms_output.put_line('Nombre de localidad: ' || empl.loc.localidad);
        dbms_output.put_line('Provincia de localidad: ' || empl.loc.provincia);
        dbms_output.put_line('Código postal de localidad: ' || empl.loc.cp);
    END LOOP;
    CLOSE empls;
END;
/
