# Tema 2: Ficheros
Estructuras de información para guardar datos de diferente naturaleza. La
estructura del nombre suele ser `<nombre fichero>.<ext>`. Estos ficheros suelen
ser de tipo:
 - Plano (Plain): puro texto
 - Binario (Binary): se requiere un programa para interpretarlo

Un archivo se puede clasificar según:
 - Contenido
  - Plano: caracteres y números
  - Contenido complejo: vídeos, imágenes, audios, etc.
 - Organización
  - Secuencial: datos unos detrás de otros
  - Directa: acceso a un dato concreto
  - Indexada: consulta de índice (combinación de las anteriores)
 - Utilidad
  - Importantes
  - Modificaciones
  - Antiguos/archivables
  - Etc.

Los ficheros planos usan un código ASCII (American Standard Code for Information
Interchange). La separación entre mayúscula y minúscula es de 32 en la tabla
ASCII. **Viene mayúscula primero**.
