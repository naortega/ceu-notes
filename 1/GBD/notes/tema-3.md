# Tema 3: Base de Datos
Una base de datos se compone de lo siguiente:
 - Tablas: información de un objeto o suceso
  - Filas (entrada)
  - Columnas (atributos)
 - Relaciones: vínculos entre las tablas

Entre los atributos, tiene que haber uno o varios que forman una clave, que es
un campo (o varios) que identifica a una entrada de forma única. Se recomienda
usar una columna `ID`.

Una consulta, o __query__, es cualquier tipo de petición, sea crear, leer,
modificar, o borrar.

Una **vista** es una transformación sobre una o varias tablas para obtener una
nueva __tabla virtual__. En este caso **se almacena su definición**.

Un **informe** es una lista ordenada con todos los campos y registros
seleccionados de forma amigable.

Un **procedimiento** es un tipo especial de script que se encuentra almacenado
en la base de datos y que forma parte de su esquema. I.e. una __función__.

Estructura = Metadatos = Esquema = Schema

## Uso de las Bases de Datos
 - Administrador: se encarga de la implementación física. Desarrolla la política
   de seguridad y acceso a la base de datos.
 - Diseñador: Identifican los datos, las relaciones entre ellos, las
   restricciones, etc. Deben conocer los procesos y los datos.
 - Programador de Aplicaciones: Desarrollan la aplicación que realiza CRUD sobre
   la base de datos para que la utilicen los usuarios finales.
 - Usuarios Finales: Emplean las aplicaciones que explotan la base de datos.
   Ellos fijan con el administrador, diseñador, y programador los requisitos
   para la gestión de la información.

## Almacenamiento de Información
 - Discos SATA
 - Discos SCSI: Interfaces para discos duros de gran capacidad.
 - RAID: Conjunto de varios discos formando un bloque
 - NAS: Sistema de almacenamiento masivo en red.
 - SAN: Red de área de almacenamiento. Conecta servidores, matrices de discos, y
   librerías de soporte.

## Reglas de Codd (BB.DD. Relacionales)
 0. Regla de Fundación: Todo es relacional
 1. Regla de Información: no hay información que no esté en tablas
 2. Regla de Acceso Garantizado: todos los datos deben ser accesibles sin ambigüedad
 3. Regla de Tratamiento de Valores Nulos: debe permitir valores nulos
 4. Catálogo Basado en el Modelo Relacional: los usuarios con permiso deben
	poder acceder a un base de datos.
 ...

## SQL
Se divide en cuatro sublenguajes:
 - Data Manipulation Language
 - Data Definition Language
 - DCL
 - TCL

## Proceso de Diseño de una Base de Datos
 1. Se realiza el modelo conceptual (Diagramas E/R)
 2. Se transforma el modelo conceptual en lógico (Modelo Relacional)
 3. Se pasa del modelo lógico al físico (DDL SQL)

## Generalizaciones
Superclases y subclases como en POO. Hay 4 tipos:
 - Exclusiva: sólo se puede ser de un tipo
 - Inclusiva: se puede ser de cualquier tipo
 - Parcial: se puede ser o no de uno de los subclases (e.g. interface)
 - Total: se tiene que ser de una de los subclases (superclass)
