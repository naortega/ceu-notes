# Guía de las Formas Normales
Una serie de condiciones para cumplir para evitar la redundancia en las bases de
datos y mejorar la integridad de éstas.

## Forma Normal (FN) 1
Cada columna sólo puede tener un valor por entrada. En caso de que no, se puede
crear una nueva tabla.

## Forma Normal (FN) 2
Los atributos de cada entrada han de depender de la llave completa, no sólo una
parte. Si existe un caso donde un atributo tan sólo depende de una de las
claves, se puede crear una tabla separada.

## Forma Normal (FN) 3
Los atributos que dependen de la clave, pero tan sólo de forma transitiva (i.d.
indirecta), queremos ponerlas en una tabla aparte para reducir redundancias.
