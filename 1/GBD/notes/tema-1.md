# Introducción
Una base de datos es un sitio donde se guardan los datos (e.g. pen drive,
archivo, lista de la compra).

## SQL
Lenguaje de __query__ que se usa con bases de datos como el de Oracle.

## CRUD
Define un sistema de información. Todos los datos se pueden:
 - Create
 - Read
 - Update
 - Delete
