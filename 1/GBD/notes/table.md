Students:
 - ID: INT (PRIMARY KEY)
 - Name: VARCHAR
 - Surname: VARCHAR
 - Birthday: DATE
 - Course: INT (FOREIGN KEY)

| ID | Name           | Surname        | Birthday    | Course |
---------------------------------------------------------------
| 1  | Nicolás        | Ortega         | 1996-08-03  | 1      |
| 2  | Jose Antonio   | NULL           | XXXX-XX-XX  | 1      |
| 3  | Miguel Ángel   | NULL           | XXXX-XX-XX  | 1      |
| 4  | Daniel         | NULL           | XXXX-XX-XX  | 1      |
| 5  | Agustín        | NULL           | XXXX-XX-XX  | 1      |
| 6  | Domingo        | NULL           | XXXX-XX-XX  | 1      |
| 7  | Miguel         | NULL           | XXXX-XX-XX  | 1      |


Courses:
 - ID: INT (PRIMARY KEY)
 - Name: VARCHAR
 - Years: INT

| ID | Name                                           | Years |
---------------------------------------------------------------
| 1  | Administración de Sistemas Informáticos en Red | 2     |
