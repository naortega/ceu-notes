<html>
	<body>
		<table>
			<tr>
				<th colspan="4" >Temporada: 
{
doc("futbol1.xml")/marcador/competicion[id="1"]/temporada
}
-
Jornada:
{
doc("futbol1.xml")/marcador/competicion[id="1"]/jornada/id
}
				</th>
			</tr>
{
for $event in doc("futbol1.xml")/marcador/competicion[id="1"]/grupos/grupo[id="1"]/eventos/evento
return
	<tr>
		<td>{ string($event/id) }</td>
		<td>{ string($event/equipolocal) } - { string($event/equipovisitante) }</td>
		<td>{ string($event/resultadolocal) }</td>
		<td>{ string($event/resultadovisitante) }</td>
	</tr>
}
			</tr>
		</table>
	</body>
</html>
