xquery version="1.0" encoding="UTF-8";

<h1>Ejercicio 0</h1>
<p>Listado HTML de la matrícula de los coches que usan diésel.</p>
<ul>
{
for $x in doc("concesionario.xml")/concesionario/coche[combustible="Diesel"]/@matricula
order by $x
return <li>Matrícula: <b>$x</b></li>
}
</ul>

<h1>Ejercicio 1</h1>
<p>Muestra el precio de los coches más del 21% de IVA</p>
{
for $x in doc("concesionario.xml")/
}

<h1>Ejercicio 4</h1>
<p>Muestra los vehículos cuyo precio es mayor a 24000</p>
{
for $x in doc("concesionario.xml")/concesionario/coche
where $x/price > 24000
return $x
}

<h1>Ejercicio 9</h1>
<p>Muestra todos los datos de cada coche del concesionario dentro de una etiqueta coche.</p>
{
for $x in doc("concesionario.xml")/concesionario/coche
return $x
}
