\documentclass[12pt,a4paper]{article}
\usepackage[spanish]{babel}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}

\title{Tema VIII Ejercicio VII}
\author{Nicolás A. Ortega Froysa}

\begin{document}
\maketitle
\pagebreak
\tableofcontents
\pagebreak

\section{Resumen Ejecutivo}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.75\linewidth]{imgs/00-mapa-de-la-red.png}
	\caption{Mapa de la red.}
	\label{fig:mapa-de-la-red}
\end{figure}

El objetivo de esta práctica es crear una red dinámica en la que hay tres
servidores (DNS, DHCP, y HTTP), y cuatro redes (virtuales) distintas que tienen
al menos cinco terminales. Para montar esta red, se hará uso de cinco {\em
switches} y un {\em router}, de tal modo que quede como se puede ver en la
figura \ref{fig:mapa-de-la-red}.

\section{Configuración}

\subsection{Switches}

En todos los {\em switches} hemos de configurar algunas cosas básicas, como
sería las VLAN, la contraseña de entrada ({\tt ASIR1}) y el MOTD. Para esto, en
cada {\em switch} corremos los siguientes comandos:

\begin{verbatim}
enable
conf t
enable secret ASIR1
banner motd "Estas accediendo al switch, enhorabuena!"
vlan 10
name INF
vlan 20
name ASIR
vlan 30
name DAW
vlan 40
name DAM
\end{verbatim}

En aquellos {\em switches} que estén más cerca de los terminales y servidores
(i.e.\ S-INF, S-ASIR, S-DAW, y S-DAM) tenemos que configurar sus interfaces.
Asumimos que la interfaz FastEthernet0/1 siempre queda reservada para conectarse
al {\em switch} central (i.e.\ S-CENTRAL), y las demás las podemos usar para
terminales y servidores. Para esto corremos los comandos siguientes,
intercambiando {\tt \textless{}n\textgreater{}} por la VLAN que corresponde a la
red virtual asignada a este {\em switch}:

\begin{verbatim}
int fa0/1
switchport mode trunk
int range fa0/2-24
switchport mode access
switchport access vlan <n>
\end{verbatim}

Luego, en el {\em switch} central (S-CENTRAL) configuramos tal que todas las
interfaces que estamos usando (i.e. FastEthernet0/1-5) sean de tipo troncal:

\begin{verbatim}
int range fa0/1-5
switchport mode trunk
\end{verbatim}

\subsection{Servidores}

Todos los servidores de nuestra red se encuentran dentro de la VLAN 10 (con
nombre <<INF>>). Físicamente se encuentran conectados al {\em switch} S-INF.

\subsubsection{Servidor DHCP}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.75\linewidth]{imgs/01-configuración-DHCP.png}
	\caption{Configuración DHCP.}
	\label{fig:configuracion-dhcp}
\end{figure}

El servidor DHCP lo configuramos manualmente con las direcciones IP siguientes:

\begin{itemize}
	\item IP: 192.168.1.2
	\item Máscara: 255.255.255.0
	\item Gateway: 192.168.1.1
	\item DNS: 192.168.1.3
\end{itemize}

\noindent
Luego configuramos cuatro {\em pools} de DHCP distintas para cada VLAN tal como
aparecen en la figura \ref{fig:configuracion-dhcp}. Empezamos a partir de la
dirección 192.168.x.10 de cada red para reservar las primeras direcciones por si
quisiéramos añadir más servidores que proveen servicios en cada red. Antes de
continuar nos aseguramos de que el servicio esté encendido.

\subsubsection{Servidor DNS}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.75\linewidth]{imgs/02-configuración-DNS.png}
	\caption{Configuración DNS.}
	\label{fig:configuracion-dns}
\end{figure}

El servidor DNS lo configuramos manualmente con las direcciones IP siguientes:

\begin{itemize}
	\item IP: 192.168.1.3
	\item Máscara: 255.255.255.0
	\item Gateway: 192.168.1.1
	\item DNS: 192.168.1.3
\end{itemize}

Es aquí en este servidor donde tenemos que configurar el nombre de dominio de
nuestro servidor HTTP. Para esto añadimos una entrada apuntando el nombre de
dominio <<www.universidadasir.es>> a la dirección IP 192.168.1.4. Nos aseguramos
de que el servicio está encendido, de tal modo que el debería parecerse a la
figura \ref{fig:configuracion-dns}.

\subsubsection{Servidor HTTP}

El servidor HTTP lo configuramos manualmente con las direcciones IP siguientes:

\begin{itemize}
	\item IP: 192.168.1.4
	\item Máscara: 255.255.255.0
	\item Gateway: 192.168.1.1
	\item DNS: 192.168.1.3
\end{itemize}

Para este servidor tan sólo hemos de entrar en la configuración de HTTP y
asegurarnos de que esté encendido, y (opcionalmente) modificar el archivo {\tt
index.html} para que aparezca algún resultado más reconocible.

\subsection{Router}

Para la configuración del {\em router} tenemos que configurar primero las VLAN,
que se puede hacer con los comandos siguientes:

\begin{verbatim}
enable
vlan database
vlan 10 name INF
vlan 20 name ASIR
vlan 30 name DAW
vlan 40 name DAM
\end{verbatim}

A continuación configuramos la interfaz FastEthernet0/0 para que se divida en
varias sub-interfaces: una nativa (0/0.1), y una por cada VLAN de nuestra red.
También hemos de asignar un {\em helper address} que servirá para redireccionar
todas las peticiones de DHCP a nuestro servidor DHCP que se encuentra en la red
VLAN 10.

\begin{verbatim}
int fa0/0
no ip address
int fa0/0.1
encapsulation dot1Q 1 native
ip address 192.168.0.1
int fa0/0.10
encapsulation dot1Q 10
ip address 192.168.1.1
int fa0/0.20
encapsulation dot1Q 10
ip address 192.168.2.1
int fa0/0.20
encapsulation dot1Q 20
ip address 192.168.2.1
int fa0/0.30
encapsulation dot1Q 30
ip address 192.168.3.1
int fa0/0.40
encapsulation dot1Q 40
ip address 192.168.4.1
\end{verbatim}

\subsection{Terminales}

Con todo esto configurado, la configuración de los terminales es bien fácil. Tan
sólo hemos de entrar en la configuración de IP de cada terminal y cambiarlo de
modo estático al uso de DHCP. Entonces debería recibir la configuración dada por
el servidor DHCP al ordenador con una dirección IP nueva que corresponde a su
VLAN.

\section{Prueba}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.75\linewidth]{imgs/03-prueba-conexión-HTTP.png}
	\caption{Prueba conexión HTTP.}
	\label{fig:prueba-conexion-http}
\end{figure}

Una vez que tengamos todo configurado, podemos verificar si nuestra red funciona
accediendo mediante navegador web desde un terminal en cualquier VLAN que no sea
la 10 (de INF) a nuestro servidor HTTP con el nombre de dominio que hemos
asignado anteriormente. Si todo funciona correctamente, ha de aparecer la página
{\tt index.html} que hemos modificado anteriormente (figura
\ref{fig:prueba-conexion-http}).

\pagebreak

\section{Derechos de Autor y Licencia}

\noindent
Copyright \copyright\ \the\year\ Nicolás A. Ortega Froysa
<nicolas@ortegas.org>  \\
\\
Este documento se distribuye bajo los términos y condiciones de la licencia
Creative Commons Attribution No Derivatives 4.0 International.

\end{document}
