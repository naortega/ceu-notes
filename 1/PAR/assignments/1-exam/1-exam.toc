\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Hoja De Control Del Documento}{3}{}%
\contentsline {section}{\numberline {2}Resumen Ejecutivo}{4}{}%
\contentsline {section}{\numberline {3}Configuración Redes Inalámbricas}{4}{}%
\contentsline {section}{\numberline {4}IP Configuration}{5}{}%
\contentsline {subsection}{\numberline {4.1}Terminales de Usuario}{5}{}%
\contentsline {subsection}{\numberline {4.2}Enrutadores}{7}{}%
\contentsline {section}{\numberline {5}Servicios en la Red}{9}{}%
\contentsline {subsection}{\numberline {5.1}DNS}{9}{}%
\contentsline {subsection}{\numberline {5.2}Web (HTTP)}{11}{}%
\contentsline {section}{\numberline {6}Enrutamiento}{11}{}%
\contentsline {subsection}{\numberline {6.1}Dinámico}{13}{}%
\contentsline {subsection}{\numberline {6.2}Estático}{14}{}%
\contentsline {subsection}{\numberline {6.3}Seguridad del Enrutamiento}{15}{}%
\contentsline {section}{\numberline {7}Derechos de Autor y Licencia}{16}{}%
