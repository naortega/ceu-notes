# Initial Conf
enable
conf t
enable secret cisco
username cisco privilege 1 password 0 cisco
vlan 100
name 100_SOPORTE
vlan 102
name 102_MERCADEO
vlan 104
name 104_JURIDICA
vlan 106
name 106_I+D
vlan 108
name 108_VENTAS

# Conf Left
int f0/1
switchport access vlan 102
int f0/2
switchport access vlan 104
int f0/3
switchport access vlan 100
int f0/24
switchport mode trunk

# Conf right
int f0/1
switchport access vlan 106
int f0/2
switchport access vlan 108
int range f0/23-24
switchport mode trunk

# IP
int vlan1
ip address 10.30.3.1 255.255.255.0