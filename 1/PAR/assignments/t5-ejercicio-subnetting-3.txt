1)
a. 10 bits de máscara, 8 son del primer 113, lo cual 10 - 8 = 2 bits => 2^2 = 4 subredes.
b. 2^(32-10) - 2 = 4 194 302 hosts
c. 2^7 + 2^6 = 192 => 113.192.0.0
d. 113.191.255.255
e.
Direcciones de subred 1: 113.0.0.1 - 113.63.255.254
Dirección de subred 1: 113.0.0.0
Dirección de broadcast de subred 1: 113.63.255.255
f. Sí, pertenece a la subred 1.

2)
a. 16 primeros usados por 176.46, lo cual 20 - 16 = 4 bits => 2^4 = 16 subredes.
b. 2^(32-20) - 2 = 4094 hosts
c. 2^6 + 2^5 = 96 => 176.46.96.0
d. No existe.
e.
Direcciones de subred 5: 176.46.80.1 - 176.46.95.254
Dirección de subred 5: 176.46.80.0
Dirección de broadcast de subred 5: 176.46.95.255
f. Sí, pertenece a la subred 15

3)
a. 255.255.255.248
b. 29-24 = 5 => 2^5 = 32 subredes
c. 2^(32-29) - 2 = 8 hosts
d. 2^3 * (21 - 1) = 235.171.39.160
e. 2^3 * (23 - 1) - 1 = 235.171.39.176
f.
Direcciones de subred 23 = 235.171.39.177-182
Dirección de subred 23 = 235.171.39.176
Dirección de broadcast de subred 23 = 235.171.39.183
g. No lo son. 235.171.39.248 es la dirección de red de la subred 32, y la 235.171.39.247 es el broadcast de la subred 31.
h. 2^5 = 32 => sería mejor una máscara de (32 - 5) = 27 y sobrarían (30 - 17) = 13 direcciones.
