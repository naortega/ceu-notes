\documentclass[12pt,a4paper]{article}
\usepackage[spanish]{babel}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}

\title{Tema VIII Examen}
\author{Nicolás A. Ortega Froysa}

\begin{document}
\maketitle
\pagebreak
\tableofcontents
\pagebreak

\section{Introducción}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.75\linewidth]{imgs/00-mapa-de-la-red.png}
	\caption{Mapa de la red.}
	\label{fig:mapa-de-la-red}
\end{figure}

El objetivo es crear una red como aparece en la figura \ref{fig:mapa-de-la-red}.
Para esto hemos de configurar tres redes, además de una adicional (pequeña) para
un servidor DHCP general que se usará para las redes de ASIR y DAW -- ya que la
red INF tiene su propio servidor DHCP.

\section{Configuración}
\subsection{El Router}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.75\linewidth]{imgs/01-modulo-NM-2FE2W.png}
	\caption{Uso del módulo NM-2FE2W en un {\em router} 2621XM.}
	\label{fig:modulo-NM-2FE2W}
\end{figure}

Lo primero, que vamos a configurar es el {\em router}. Vamos a usar el modelo
2621XM y añadirle el módulo NM-2FE2W que añade dos puertos de {\em FastEthernet}
más a nuestro {\em router} (figura \ref{fig:modulo-NM-2FE2W}), de tal modo que
tenemos 4 puertos en total.

En estos cuatro interfaces, las tenemos que configurar con la dirección IP
correspondiente de {\em gateway} (enlace de red) de cada subred:

\begin{itemize}
	\item Red 0 (sólo con servidor DHCP general): 192.168.0.1/30
	\item Red 1: 192.168.1.0/24
	\item Red 2: 192.168.2.0/24
	\item Red 3: 192.168.3.0/24
\end{itemize}

Para la red 0 usamos la máscara de subred 30 (i.e.\ 255.255.255.252) ya que sólo
contendrá el {\em router} y el servidor DHCP general. Una vez configuradas estas
direcciones, también hemos de configurar a las redes 2 y 3 al servidor DHCP
general -- a la red 1 no se le configura porque tiene su propio servidor DHCP.
Para esto, tenemos que correr los siguientes comandos (cambiando {\tt
\textless{}if\textgreater{}} con la interfaz de la subred en el {\em router}):

\begin{verbatim}
int <if>
ip helper-address 192.168.0.2
\end{verbatim}

\subsection{Los Switches}

En los {\em switches} hemos de configurar las redes virtuales (VLAN) y también
lo que se pedía cambio de contraseña, configuración de reloj, y definición del
mensaje del día (MotD).

Para empezar, configuramos la contraseña, la hora, y el mensaje del día. Para
esto, entramos en la consola y ponemos los comandos siguientes:

\begin{verbatim}
enable
clock set 16:29:00 May 13 2022
conf t
banner motd "Vamos a aprobar!"
enable secret asir2021
\end{verbatim}

Para configurar los VLAN simplemente hemos de añadir cada VLAN al {\em switch} y
asignamos a todas las interfaces de ese {\em switch} su VLAN correspondiente
(e.g.\ en la red INF se le configura para la VLAN 10). Simplemente cambiamos
{\tt \textless{}id\textgreater{}} por la ID de la VLAN correspondiente.

\begin{verbatim}
vlan 10
name INF
vlan 20
name ASIR
vlan 30
name DAW
int range fa0/1-24
switchport mode access
switchport access vlan <id>
\end{verbatim}

\subsection{Red INF}

La primera red que vamos a configurar es la de INF, ya que contiene más
elementos que requieren de configuración manual. Sobre todo se trata de
configurar los tres servidores que tiene.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/02-DNS.png}
		\caption{Configuración del servidor DNS.}
		\label{fig:DNS}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/03-DHCP-INF.png}
		\caption{Configuración del servidor DHCP de la red INF.}
		\label{fig:DHCP-INF}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/04-configuración-del-servidor-HTTP.png}
		\caption{Configuración del servidor HTTP.}
		\label{fig:HTTP-INF}
	\end{subfigure}
	\caption{Configuración de los servidores de la red INF.}
\end{figure}

El primer servidor es el DNS, que tendrá dirección IP de 192.168.1.2/24, y se ha
de configurar como se ve en la figura \ref{fig:DNS}, con una entrada para el
nombre de dominio www.aprobamosasir.es. El segundo sería el servidor DHCP de la
red INF con dirección IP de 192.168.1.3. Como sólo responde a las peticiones de
esta red, sólo ha de tener una entrada (figura \ref{fig:DHCP-INF}):

\begin{itemize}
	\item Default Gateway: 192.168.1.1
	\item DNS Server: 192.168.1.2
	\item Start IP Address: 192.168.1.5
	\item Subnet Mask: 255.255.255.0
	\item Maximum Number of Users: 251
\end{itemize}

Finalmente, el servidor HTTP simplemente ha de estar configurado con su
dirección IP de 192.168.1.4, y que su servicio de HTTP esté habilitado. En
nuestro caso, también hemos cambiado el contenido del archivo {\tt index.html}
(figura \ref{fig:HTTP-INF}).

Ya con esto configurado, podemos ir a todos los PCs de la red INF y
configurarlos tal que pidan su dirección IP por DHCP.

\subsection{Servidor DHCP General}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.75\linewidth]{imgs/08-configuración-del-servidor-DHCP-general.png}
	\caption{Configuración del servidor DHCP general.}
	\label{fig:DHCP-general}
\end{figure}

El último paso es la configuración del servidor DHCP general, conectado
directamente al {\em router}. Ponemos la dirección que habíamos especificado
antes, 192.168.0.2/30. Luego configuramos dos entradas de DHCP de la forma
siguiente (figura \ref{fig:DHCP-general}):

\begin{itemize}
	\item Red ASIR:
		\begin{itemize}
			\item Pool Name: serverPool
			\item Default Gateway: 192.168.2.1
			\item DNS Server: 192.168.1.2
			\item Start IP Address: 192.168.2.5
			\item Subnet Mask: 255.255.255.0
			\item Maximum Number of Users: 251
		\end{itemize}
	\item Red DAW:
		\begin{itemize}
			\item Pool Name: RED DAW
			\item Default Gateway: 192.168.3.1
			\item DNS Server: 192.168.1.2
			\item Start IP Address: 192.168.3.5
			\item Subnet Mask: 255.255.255.0
			\item Maximum Number of Users: 251
		\end{itemize}
\end{itemize}

Con todo esto configurado, ya podemos configurar los PCs de los demás redes
(ASIR y DAW) para pedir su dirección IP mediante DHCP.

\section{Pruebas}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/07-prueba-intranet-del-servidor-HTTP.png}
		\caption{Prueba en intranet del servidor HTTP.}
		\label{fig:prueba-intranet-HTTP}
	\end{subfigure}
	\hfill
	\begin{subfigure}[tb]{0.45\textwidth}
		\centering
		\includegraphics[width=0.95\linewidth]{imgs/09-prueba-interred-del-servidor-HTTP.png}
		\caption{Prueba en interred del servidor HTTP.}
		\label{fig:prueba-interred-HTTP}
	\end{subfigure}
	\caption{Pruebas de conectividad.}
\end{figure}

Si la red funciona correctamente, todos los PCs han de tener dirección IP por
DHCP configurado, además de su {\em gateway} y servidor DNS. De este modo,
podemos también comprobar la conectividad simplemente con probar si pueden
llegar a conectarse al servidor HTTP de la red INF por medio de su nombre de
dominio, tanto dentro de la misma red (figura \ref{fig:prueba-intranet-HTTP})
como desde las otras redes (figura \ref{fig:prueba-interred-HTTP}). Esto nos
demuestra el funcionamiento del DHCP, DNS, el router, y el servidor HTTP.

\pagebreak

\section{Derechos de Autor y Licencia}

\noindent
Copyright \copyright\ \the\year\ Nicolás A. Ortega Froysa
<nicolas@ortegas.org> \\
\\
Este documento se distribuye bajo los términos y condiciones de la licencia
Creative Commons Attribution No Derivatives 4.0 International.

\end{document}
