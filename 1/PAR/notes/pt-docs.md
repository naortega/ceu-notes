# PacketTracer Docs (Documentación PT)

## Configuración Propio Switch/Router

### Hostname

```
hostname <nombre>
```

### Contraseña

```
enable secret <password>
username <usuario> privilege 1 password 0 <password>
```

### MOTD

```
banner motd "<motd>"
```

## Sub-Interfaces

```
int <if>.<sub-if> # e.g. fa0/1.1
encapsulation dot1Q <net> [native]
```

## Enrutamiento

### Dinámico

```
router rip
version 2
no auto-summary
network <ip-net>
```

## VLANs

```
vlan <id>
name <name>
```

### Enlaces de Acceso

```
# para una sola interfaz
int <if>
# para un rango de interfaces
int range <if>-<if>
switchport access vlan <id>
```

### Enlaces Troncales

```
int <if>
switchport mode trunk
```

### Asignar IP a VLAN

```
int vlan <id>
ip address <ip> <mask>
```

## DHCP

```
ip dhcp pool <name>
network <ip-net> <mask-net>
default-router <ip>
dns-server <ip>
ip dhcp excluded-address <low-ip> <high-ip>
```

### Asignar Dirección Servidor DHCP

Hay veces que queremos asignar desde un __router__ o un __switch__ el servidor
DHCP con un cierto IP que está dentro de otra red. Para esto hacemos uso del
__helper address__. Simplemente entramos en la (sub-)interfaz a la que queremos
asignar este servidor, y añadimos el __helper__ como de forma siguiente:

```
int <if>[.<sub-if>]
ip helper-address <ip-dhcp>
```
